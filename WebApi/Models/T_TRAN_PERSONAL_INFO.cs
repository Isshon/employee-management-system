//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_TRAN_PERSONAL_INFO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_TRAN_PERSONAL_INFO()
        {
            this.T_TRAN_CERTIFICATION_INFO = new HashSet<T_TRAN_CERTIFICATION_INFO>();
            this.T_TRAN_CHILD_INFO = new HashSet<T_TRAN_CHILD_INFO>();
            this.T_TRAN_EDUCATION_INFO = new HashSet<T_TRAN_EDUCATION_INFO>();
            this.T_TRAN_EMPLOYMENT_INFO = new HashSet<T_TRAN_EMPLOYMENT_INFO>();
            this.T_TRAN_EXIT_INTERVIEW = new HashSet<T_TRAN_EXIT_INTERVIEW>();
            this.T_TRANS_MEMERSHIP_HISTORY = new HashSet<T_TRANS_MEMERSHIP_HISTORY>();
            this.T_TRAN_PHOTO_AND_OTHERS = new HashSet<T_TRAN_PHOTO_AND_OTHERS>();
            this.T_TRAN_PROJECT_EXPERIENCE_INFO = new HashSet<T_TRAN_PROJECT_EXPERIENCE_INFO>();
            this.T_TRAN_SIBLING_INFO = new HashSet<T_TRAN_SIBLING_INFO>();
            this.T_TRAN_TRAINING_INFO = new HashSet<T_TRAN_TRAINING_INFO>();
            this.T_TRANS_ADDRESS_INFO = new HashSet<T_TRANS_ADDRESS_INFO>();
        }
    
        public int EMP_ID { get; set; }
        public string PERSONAL_ID { get; set; }
        public string DOMEN_ID { get; set; }
        public Nullable<int> SALUTATION_ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string FATHER_NAME { get; set; }
        public int FATHER_OCCUPATION_ID { get; set; }
        public string MOTHER_NAME { get; set; }
        public int MOTHER_OCCUPATION_ID { get; set; }
        public System.DateTime DATE_OF_BIRTH { get; set; }
        public int RELIGION_ID { get; set; }
        public Nullable<int> BLOOD_GROUP_ID { get; set; }
        public int GENDER_ID { get; set; }
        public Nullable<int> MARTIAL_STATUS_ID { get; set; }
        public string SPOUSE_NAME { get; set; }
        public Nullable<int> SPOUSE_OCCUPATION_ID { get; set; }
        public Nullable<int> NATIONALITY_ID { get; set; }
        public string NATIONAL_ID { get; set; }
        public string BIRTH_ID { get; set; }
        public string PASSPORT_NO { get; set; }
        public Nullable<System.DateTime> PASSPORT_EXPIRY { get; set; }
        public Nullable<int> DESIGNATION_ID { get; set; }
        public Nullable<int> DEPARTMENT_ID { get; set; }
        public Nullable<int> BRANCH_ID { get; set; }
        public Nullable<int> EMP_TYPE_ID { get; set; }
        public System.DateTime JOINING_DATE { get; set; }
        public Nullable<System.DateTime> CONFIRMATION_DATE { get; set; }
        public string BANK_ACCOUNT { get; set; }
        public Nullable<int> BANK_ID { get; set; }
        public string OFFICE_MOBILE { get; set; }
        public Nullable<int> OFFICE_EXT { get; set; }
        public string PERSONAL_MOBILE { get; set; }
        public string OFFICE_EMAIL { get; set; }
        public string PERSONAL_EMAIL { get; set; }
        public string EMER_PERSON { get; set; }
        public string EMER_PERSON_MOBILE { get; set; }
        public string RELATION { get; set; }
        public string EMER_PERSON_ADDR { get; set; }
        public Nullable<int> ADDEDBY { get; set; }
        public Nullable<System.DateTime> TS { get; set; }
        public string LOGIN_ID { get; set; }
    
        public virtual T_SYS_BANK_INFO T_SYS_BANK_INFO { get; set; }
        public virtual T_SYS_BLOOD_GROUP_INFO T_SYS_BLOOD_GROUP_INFO { get; set; }
        public virtual T_SYS_BRANCH_OFFICE_INFO T_SYS_BRANCH_OFFICE_INFO { get; set; }
        public virtual T_SYS_DEPARTMET_INFO T_SYS_DEPARTMET_INFO { get; set; }
        public virtual T_SYS_DESIGNATION_MASTER T_SYS_DESIGNATION_MASTER { get; set; }
        public virtual T_SYS_EMPLOYEE_TYPE_INFO T_SYS_EMPLOYEE_TYPE_INFO { get; set; }
        public virtual T_SYS_GENDER_INFO T_SYS_GENDER_INFO { get; set; }
        public virtual T_SYS_MARITAL_STATUS_INFO T_SYS_MARITAL_STATUS_INFO { get; set; }
        public virtual T_SYS_RELIGION_INFO T_SYS_RELIGION_INFO { get; set; }
        public virtual T_SYS_SALUTATION_INFO T_SYS_SALUTATION_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_CERTIFICATION_INFO> T_TRAN_CERTIFICATION_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_CHILD_INFO> T_TRAN_CHILD_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_EDUCATION_INFO> T_TRAN_EDUCATION_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_EMPLOYMENT_INFO> T_TRAN_EMPLOYMENT_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_EXIT_INTERVIEW> T_TRAN_EXIT_INTERVIEW { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRANS_MEMERSHIP_HISTORY> T_TRANS_MEMERSHIP_HISTORY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_PHOTO_AND_OTHERS> T_TRAN_PHOTO_AND_OTHERS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_PROJECT_EXPERIENCE_INFO> T_TRAN_PROJECT_EXPERIENCE_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_SIBLING_INFO> T_TRAN_SIBLING_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_TRAINING_INFO> T_TRAN_TRAINING_INFO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRANS_ADDRESS_INFO> T_TRANS_ADDRESS_INFO { get; set; }
    }
}
