//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_TRAN_EMPLOYMENT_INFO
    {
        public int ID { get; set; }
        public string COMPANY_NAME { get; set; }
        public string COMPANY_LOCATION { get; set; }
        public string DESIGNATION { get; set; }
        public string DEPARTMENT_NAME { get; set; }
        public System.DateTime START_DATE { get; set; }
        public Nullable<System.DateTime> END_DATE { get; set; }
        public string ROLES_RESPONSIBILITY { get; set; }
        public string AREA_OF_EXPERIENCE { get; set; }
        public int EMP_ID { get; set; }
        public System.DateTime TS { get; set; }
        public int ADDEDBY { get; set; }
    
        public virtual T_TRAN_PERSONAL_INFO T_TRAN_PERSONAL_INFO { get; set; }
    }
}
