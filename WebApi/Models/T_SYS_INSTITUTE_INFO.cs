//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SYS_INSTITUTE_INFO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_SYS_INSTITUTE_INFO()
        {
            this.T_TRAN_EDUCATION_INFO = new HashSet<T_TRAN_EDUCATION_INFO>();
        }
    
        public int INSTITUTE_ID { get; set; }
        public string INSTITUTE_NAME { get; set; }
        public Nullable<int> INSTITUTE_TYPE { get; set; }
        public Nullable<int> ADDEDBY { get; set; }
        public Nullable<System.DateTime> TS { get; set; }
        public Nullable<bool> ACTIVE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TRAN_EDUCATION_INFO> T_TRAN_EDUCATION_INFO { get; set; }
    }
}
