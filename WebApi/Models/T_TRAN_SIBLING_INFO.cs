//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_TRAN_SIBLING_INFO
    {
        public int ID { get; set; }
        public string FULL_NAME { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string MOBILE_NO { get; set; }
        public Nullable<int> OCCUPATION_ID { get; set; }
        public int NO_OF_SIBLINGS { get; set; }
        public Nullable<int> SIBLINGS_GENDER { get; set; }
        public int EMP_ID { get; set; }
        public System.DateTime TS { get; set; }
        public int ADDEDBY { get; set; }
        public string LOGIN_ID { get; set; }
    
        public virtual T_SYS_OCCUPATION_INO T_SYS_OCCUPATION_INO { get; set; }
        public virtual T_TRAN_PERSONAL_INFO T_TRAN_PERSONAL_INFO { get; set; }
    }
}
