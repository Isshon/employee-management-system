﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.Controllers.Personal
{
    public class PersonalRepository : IPersonalRepository
    {
        public ComonModel.Personal GetAllById(int id)
        {
            ComonModel.Personal allinfo = new ComonModel.Personal();
            List<ComonModel.ClsSiblingInfo> AllSiblings = null;
            List<ComonModel.ClsChildInfo> AllChilds = null;
            List<ComonModel.ClsEducationInfo> AllEducationInfo = null;
            List<ComonModel.ClsTrainingInfo> AllTrainingInfo = null;
            List<ComonModel.ClsCertificationInfo> AllCertificationInfo = null;
            List<ComonModel.ClsEmploymentInfo> AllEmploymentInfo = null;
            List<ComonModel.ClsProjectExperienceInfo> AllProjectExperienceInfo = null;
            List<ComonModel.ClsMemershipHistory> AllMembershipInfo = null;
            //List<ComonModel.ClsPhotoAndOthers> AllPhotoAndOthers = null;


            using (var db = new EMPLOYEE_DBEntities())
            {
                //var SiblingList = db.T_TRAN_SIBLING_INFO.Where(s => s.ID == id).ToList();
                //AllSiblings = (IList<ComonModel.ClsSiblingInfo>)db.T_TRAN_SIBLING_INFO.Where(s => s.EMP_ID == id).ToList();
                //AllChilds = (IList<ComonModel.ClsChildInfo>)db.T_TRAN_CHILD_INFO.Where(s => s.EMP_ID == id).ToList();
                //AllEducationInfo = (IList<ComonModel.ClsEducationInfo>)db.T_TRAN_EDUCATION_INFO.Where(s => s.EMP_ID == id).ToList();
                //AllTrainingInfo = (IList<ComonModel.ClsTrainingInfo>)db.T_TRAN_TRAINING_INFO.Where(s => s.EMP_ID == id).ToList();
                //AllCertificationInfo = (IList<ComonModel.ClsCertificationInfo>)db.T_TRAN_CERTIFICATION_INFO.Where(s => s.EMP_ID == id).ToList();
                //AllEmploymentInfo = (IList<ComonModel.ClsEmploymentInfo>)db.T_TRAN_EMPLOYMENT_INFO.Where(s => s.EMP_ID == id).ToList();
                //AllProjectExperienceInfo = (IList<ComonModel.ClsProjectExperienceInfo>)db.T_TRAN_PROJECT_EXPERIENCE_INFO.Where(s => s.EMP_ID == id).ToList();
                //AllPhotoAndOthers = (IList<ComonModel.ClsPhotoAndOthers>)db.T_TRAN_PHOTO_AND_OTHERS.Where(s => s.EMP_ID == id).ToList();
                var CurrAddr = db.T_TRANS_ADDRESS_INFO.Where(s => s.EMP_ID == id).FirstOrDefault();
                if (CurrAddr != null)
                {
                    allinfo.PermanentPoliceId = CurrAddr.PERMANENT_POLICE_ID;
                    allinfo.PermanentPostId = CurrAddr.PERMANENT_POST_ID;
                    allinfo.PermanentDistrictId = CurrAddr.PERMANENT_DISTRICT_ID;
                    allinfo.PermanentVillage = CurrAddr.PERMANENT_VILLAGE;
                    allinfo.PermanentRoad = CurrAddr.PERMANENT_ROAD;
                    allinfo.PresentDistrictId = CurrAddr.PRESENT_DISTRICT_ID;
                    allinfo.PresentPoliceId = CurrAddr.PRESENT_POLICE_ID;
                    allinfo.PresentPostId = CurrAddr.PRESENT_POST_ID;
                    allinfo.PresentRoad = CurrAddr.PRESENT_ROAD;
                    allinfo.PresentVillage = CurrAddr.PRESENT_VILLAGE;
                    allinfo.SameAsPresentAddr = CurrAddr.SAME_AS_PRESENT_ADDR;
                }
                var ExtraCurricullar=db.T_TRAN_PHOTO_AND_OTHERS.Where(s => s.EMP_ID == id).FirstOrDefault();
                if(ExtraCurricullar!=null)
                {
                    allinfo.ExtraId = ExtraCurricullar.EXTRA_ID;
                    allinfo.ExtraActivities = ExtraCurricullar.EXTRA_ACTIVITIES;
                    allinfo.Publication = ExtraCurricullar.PUBLICATION;
                    allinfo.Photo = ExtraCurricullar.PHOTO;
                    allinfo.Signature = ExtraCurricullar.SIGNATURE;
                }
                var Personalinfo = db.T_TRAN_PERSONAL_INFO.Where(s => s.EMP_ID == id).FirstOrDefault();
                if(Personalinfo!=null)
                {
                    allinfo.EmpId = Personalinfo.EMP_ID;
                    allinfo.PersonalId = Personalinfo.PERSONAL_ID;
                    allinfo.DomenId = Personalinfo.DOMEN_ID;
                    allinfo.SalutationId = Personalinfo.SALUTATION_ID;
                    allinfo.FirstName = Personalinfo.FIRST_NAME;
                    allinfo.LastName = Personalinfo.LAST_NAME;
                    allinfo.FatherName = Personalinfo.FATHER_NAME;
                    allinfo.FatherOccupationId = Personalinfo.FATHER_OCCUPATION_ID;
                    allinfo.MotherName = Personalinfo.MOTHER_NAME;
                    allinfo.MotherOccupationId = Personalinfo.MOTHER_OCCUPATION_ID;
                    allinfo.DateOfBirth = Personalinfo.DATE_OF_BIRTH;
                    allinfo.ReligionId = Personalinfo.RELIGION_ID;
                    allinfo.BloodGroupId = Personalinfo.BLOOD_GROUP_ID;
                    allinfo.GenderId = Personalinfo.GENDER_ID;
                    allinfo.MartialStatusId = Personalinfo.MARTIAL_STATUS_ID;
                    allinfo.SpouseName = Personalinfo.SPOUSE_NAME;
                    allinfo.SpouseOccupationId = Personalinfo.SPOUSE_OCCUPATION_ID;
                    allinfo.NationalityId = Personalinfo.NATIONALITY_ID;
                    allinfo.NationalId = Personalinfo.NATIONAL_ID;
                    allinfo.BirthId = Personalinfo.BIRTH_ID;
                    allinfo.PassportNo = Personalinfo.PASSPORT_NO;
                    allinfo.PassportExpiry = Personalinfo.PASSPORT_EXPIRY;
                    allinfo.DesignationId = Personalinfo.DESIGNATION_ID;
                    allinfo.DepartmentId = Personalinfo.DEPARTMENT_ID;
                    allinfo.BranchId = Personalinfo.BRANCH_ID;
                    allinfo.EmpTypeId = Personalinfo.EMP_TYPE_ID;
                    allinfo.JoiningDate = Personalinfo.JOINING_DATE;
                    allinfo.ConfirmationDate = Personalinfo.CONFIRMATION_DATE;
                    allinfo.BankAccount = Personalinfo.BANK_ACCOUNT;
                    allinfo.BankId = Personalinfo.BANK_ID;
                    allinfo.OfficeMobile = Personalinfo.OFFICE_MOBILE;
                    allinfo.OfficeExt = Personalinfo.OFFICE_EXT;
                    allinfo.PersonalMobile = Personalinfo.PERSONAL_MOBILE;
                    allinfo.OfficeEmail = Personalinfo.OFFICE_EMAIL;
                    allinfo.PersonalEmail = Personalinfo.PERSONAL_EMAIL;
                    allinfo.EmerPerson = Personalinfo.EMER_PERSON;
                    allinfo.EmerPersonMobile = Personalinfo.EMER_PERSON_MOBILE;
                    allinfo.Relation = Personalinfo.RELATION;
                    allinfo.EmerPersonAddr = Personalinfo.EMER_PERSON_ADDR;
                    allinfo.AddedBy = Personalinfo.ADDEDBY;
                }
                AllChilds = db.T_TRAN_CHILD_INFO.Select(s => new ComonModel.ClsChildInfo()
                {
                    ChildId = s.CHILD_ID,
                    FullName = s.FULL_NAME,
                    MobileNo = s.MOBILE_NO,
                    Occupation = s.OCCUPATION,
                    NoOfChild = s.NO_OF_CHILD,
                    ChildGender = s.CHILD_GENDER,
                    EmpId = s.EMP_ID,
                    Ts = s.TS,
                    AddedBy = s.ADDEDBY,
                    LoginId = s.LOGIN_ID,
                }).Where(a=>a.EmpId == id).ToList();
                AllSiblings = db.T_TRAN_SIBLING_INFO.Select(s => new ComonModel.ClsSiblingInfo()
                {
                    Id = s.ID,
                    FullName = s.FULL_NAME,
                    MobileNo = s.MOBILE_NO,
                    OccupationId = s.OCCUPATION_ID,
                    NoOfSiblings = s.NO_OF_SIBLINGS,
                    SiblingsGender = s.SIBLINGS_GENDER,
                    EmpId = s.EMP_ID,
                    Ts = s.TS,
                    AddedBy = s.ADDEDBY,
                    LoginId = s.LOGIN_ID,
                }).Where(a => a.EmpId == id).ToList();
                AllEducationInfo = db.T_TRAN_EDUCATION_INFO.Select(s => new ComonModel.ClsEducationInfo()
                {
                    Id = s.ID,
                    DegreeId = s.DEGREE_ID,
                    //DegreeName = s.MOBILE_NO,
                    SubjectId = s.SUBJECT_ID,
                    //SubjectName = s.NO_OF_SIBLINGS,
                    InstitutionName = s.INSTITUTION_NAME,
                    BoardUniversityId = s.BOARD_UNIVERSITY_ID,
                    //BoardUniversityName = s.TS,
                    PassingYear = s.PASSING_YEAR,
                    ResultTypeId = s.RESULT_TYPE_ID,
                    Cgpa = s.CGPA,
                    OutOf = s.OUT_OF,
                    ClassDivision = s.CLASS_DIVISION,
                    Session=s.SESSION,
                    EmpId = s.EMP_ID,
                    AddedBy=s.ADDEDBY,
                    Ts= s.TS,
                }).Where(a => a.EmpId == id).ToList();

                AllTrainingInfo = db.T_TRAN_TRAINING_INFO.Select(s => new ComonModel.ClsTrainingInfo()
                {
                    Id = s.ID,
                    TrainingType = s.TRAINING_TYPE,
                    //DegreeName = s.MOBILE_NO,
                    TrainingTitle = s.TRAINING_TITLE,
                    //SubjectName = s.NO_OF_SIBLINGS,
                    TrainingDetails = s.TRAINING_DETAILS,
                    InstitutionName = s.INSTITUTION_NAME,
                    //BoardUniversityName = s.TS,
                    CountryId = s.COUNTRY_ID,
                    TrainingStartDate = s.TRAINING_START_DATE,
                    TrainingEndDate = s.TRAINING_END_DATE,
                    Duration = s.DURATION,
                    Location = s.LOCATION,
                    EmpId = s.EMP_ID,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS,
                    BondFor = s.BOND_FOR,
                }).Where(a => a.EmpId == id).ToList();
                AllCertificationInfo = db.T_TRAN_CERTIFICATION_INFO.Select(s => new ComonModel.ClsCertificationInfo()
                {
                    Id = s.ID,
                    CertName = s.CERT_NAME,
                    //DegreeName = s.MOBILE_NO,
                    ExamDetails = s.EXAM_DETAILS,
                    //SubjectName = s.NO_OF_SIBLINGS,
                    CertAuthority = s.CERT_AUTHORITY,
                    CertType = s.CERT_TYPE,
                    //BoardUniversityName = s.TS,
                    Location = s.LOCATION,
                    ObtainedDate = s.OBTAINED_DATE,
                    ExpiryDate = s.EXPIRY_DATE,
                    EmpId = s.EMP_ID,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS,
                }).Where(a => a.EmpId == id).ToList();
                AllEmploymentInfo = db.T_TRAN_EMPLOYMENT_INFO.Select(s => new ComonModel.ClsEmploymentInfo()
                {
                    Id = s.ID,
                    CompanyName = s.COMPANY_NAME,
                    //DegreeName = s.MOBILE_NO,
                    CompanyLocation = s.COMPANY_LOCATION,
                    //SubjectName = s.NO_OF_SIBLINGS,
                    Designation = s.DESIGNATION,
                    DepartmentName = s.DEPARTMENT_NAME,
                    //BoardUniversityName = s.TS,
                    StartDate = s.START_DATE,
                    EndDate = s.END_DATE,
                    RolesResponsibility = s.ROLES_RESPONSIBILITY,
                    EmpId = s.EMP_ID,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS,
                    AreaOfExperience = s.AREA_OF_EXPERIENCE,
                }).Where(a => a.EmpId == id).ToList();
                AllProjectExperienceInfo = db.T_TRAN_PROJECT_EXPERIENCE_INFO.Select(s => new ComonModel.ClsProjectExperienceInfo()
                {
                    Id = s.ID,
                    ProjectName = s.PROJECT_NAME,
                    //DegreeName = s.MOBILE_NO,
                    CompanyName = s.COMPANY_NAME,
                    //SubjectName = s.NO_OF_SIBLINGS,
                    CustomerName = s.CUSTOMER_NAME,
                    RolesResponsibility = s.ROLES_RESPONSIBILITY,
                    //BoardUniversityName = s.TS,
                    StartDate = s.START_DATE,
                    EndDate = s.END_DATE,
                    TechnologyUsed = s.ROLES_RESPONSIBILITY,
                    EmpId = s.EMP_ID,
                    addedBy = s.ADDEDBY,
                    ts = s.TS,                    
                }).Where(a => a.EmpId == id).ToList();
                AllMembershipInfo = db.T_TRANS_MEMERSHIP_HISTORY.Select(s => new ComonModel.ClsMemershipHistory()
                {
                    id = s.ID,
                    MemberId = s.MEMBER_ID,
                    //DegreeName = s.MOBILE_NO,
                    SocietyName = s.SOCIETY_NAME,
                    //SubjectName = s.NO_OF_SIBLINGS,
                    Address = s.ADDRESS,
                    EmpId = s.EMP_ID,
                    AddedBy = s.ADDEDBY,
                }).Where(a => a.EmpId == id).ToList();
                //AllPhotoAndOthers = db.T_TRAN_PHOTO_AND_OTHERS.Select(s => new ComonModel.ClsPhotoAndOthers()
                //{
                //    ExtraId = s.EXTRA_ID,
                //    ExtraActivities = s.EXTRA_ACTIVITIES,
                //    //DegreeName = s.MOBILE_NO,
                //    Publication = s.PUBLICATION,
                //    //SubjectName = s.NO_OF_SIBLINGS,
                //    Signature = s.SIGNATURE,
                //    Photo = s.PHOTO,
                //    //BoardUniversityName = s.TS,                    
                //    EmpId = s.EMP_ID,
                //    AddedBy = s.ADDEDBY,
                //    Ts = s.TS,
                //}).Where(a => a.EmpId == id).ToList();

            }
            allinfo.ChildInfoList = AllChilds;
            allinfo.SiblingsInfoList = AllSiblings;
            allinfo.EducationInfoList = AllEducationInfo;
            allinfo.TrainingInfoList = AllTrainingInfo;
            allinfo.CertificationInfoList = AllCertificationInfo;
            allinfo.EmploymentInfoList = AllEmploymentInfo;
            allinfo.ProjectExperienceInfoList = AllProjectExperienceInfo;
            allinfo.MembershipInfoList = AllMembershipInfo;
            


                return allinfo;
        }
       
        #region Education Info
        public string SaveEducationInfo(ComonModel.ClsEducationInfo EducationInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_EDUCATION_INFO objT_TRAN_EDUCATION_INFO = db.T_TRAN_EDUCATION_INFO.Where(s => s.ID == EducationInfo.Id).FirstOrDefault();

                if (objT_TRAN_EDUCATION_INFO == null)
                {
                    objT_TRAN_EDUCATION_INFO = new T_TRAN_EDUCATION_INFO();
                    db.T_TRAN_EDUCATION_INFO.Add(objT_TRAN_EDUCATION_INFO);
                }
                objT_TRAN_EDUCATION_INFO.DEGREE_ID = EducationInfo.DegreeId;
                objT_TRAN_EDUCATION_INFO.SUBJECT_ID = EducationInfo.SubjectId;
                objT_TRAN_EDUCATION_INFO.INSTITUTION_NAME = EducationInfo.InstitutionName;
                objT_TRAN_EDUCATION_INFO.BOARD_UNIVERSITY_ID = EducationInfo.BoardUniversityId;
                objT_TRAN_EDUCATION_INFO.PASSING_YEAR = EducationInfo.PassingYear;
                objT_TRAN_EDUCATION_INFO.RESULT_TYPE_ID = EducationInfo.ResultTypeId;
                objT_TRAN_EDUCATION_INFO.CGPA = EducationInfo.Cgpa;
                objT_TRAN_EDUCATION_INFO.OUT_OF = EducationInfo.OutOf;
                objT_TRAN_EDUCATION_INFO.CLASS_DIVISION = EducationInfo.ClassDivision;
                objT_TRAN_EDUCATION_INFO.SESSION = EducationInfo.Session;
                objT_TRAN_EDUCATION_INFO.EMP_ID = EducationInfo.EmpId;
                objT_TRAN_EDUCATION_INFO.TS = DateTime.Now;
                objT_TRAN_EDUCATION_INFO.ADDEDBY = EducationInfo.AddedBy;
                db.SaveChanges();

            }




            if (EducationInfo.EmpId > 0)
            {

                return EducationInfo.EmpId.ToString();
            }

            else return "Error while saving Education info !";

        }
        public IEnumerable<ComonModel.ClsEducationInfo> GetAllEducationInfo()
        {
            IList<ComonModel.ClsEducationInfo> AllEducation = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllEducation = db.T_TRAN_EDUCATION_INFO.Select(s => new ComonModel.ClsEducationInfo()
                {
                   BoardUniversityId=s.BOARD_UNIVERSITY_ID,
                   Cgpa=s.CGPA,
                   ClassDivision=s.CLASS_DIVISION,
                   DegreeId=s.DEGREE_ID,
                   DegreeName=db.T_SYS_DEGREE_INFO.Where(d=>d.DEGREE_ID==s.BOARD_UNIVERSITY_ID).FirstOrDefault().DEGREE_NAME,
                   BoardUniversityName = db.T_SYS_INSTITUTE_INFO.Where(d => d.INSTITUTE_ID == s.BOARD_UNIVERSITY_ID).FirstOrDefault().INSTITUTE_NAME,
                   EmpId=s.EMP_ID,
                   Id=s.ID,
                   InstitutionName=s.INSTITUTION_NAME,
                   OutOf=s.OUT_OF,
                   PassingYear=s.PASSING_YEAR,
                   ResultTypeId=s.RESULT_TYPE_ID,
                   Session=s.SESSION,
                   SubjectId=s.SUBJECT_ID,
                   SubjectName=db.T_SYS_COURSE_INFO.Where(d=>d.COURSE_ID==s.SUBJECT_ID).FirstOrDefault().COURSE_NAME,                  
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllEducation;
        }
        public ComonModel.ClsEducationInfo GetEducationById(int id)
        {
            ComonModel.ClsEducationInfo EducationInfo = new ComonModel.ClsEducationInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldEducation = db.T_TRAN_EDUCATION_INFO.Where(s => s.ID == id).FirstOrDefault();

                if (OldEducation != null)
                {


                    EducationInfo.Id = OldEducation.ID;
                    EducationInfo.SubjectId = OldEducation.SUBJECT_ID;
                    EducationInfo.InstitutionName = OldEducation.INSTITUTION_NAME;
                    EducationInfo.BoardUniversityId = OldEducation.BOARD_UNIVERSITY_ID;
                    EducationInfo.PassingYear = OldEducation.PASSING_YEAR;
                    EducationInfo.ResultTypeId = OldEducation.RESULT_TYPE_ID;
                    EducationInfo.Cgpa = OldEducation.CGPA;
                    EducationInfo.OutOf = OldEducation.OUT_OF;
                    EducationInfo.ClassDivision = OldEducation.CLASS_DIVISION;
                    EducationInfo.Session = OldEducation.SESSION;
                    EducationInfo.EmpId = OldEducation.EMP_ID;
                    EducationInfo.Ts = DateTime.Now;
                    EducationInfo.AddedBy = OldEducation.ADDEDBY;
                }

            }
            return EducationInfo;
        }
        public string UpdateEducationInfo(ComonModel.ClsEducationInfo EducationInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldEducation = db.T_TRAN_EDUCATION_INFO.Where(s => s.ID == EducationInfo.Id).FirstOrDefault();
                if (OldEducation != null)
                {
                    OldEducation.ID = EducationInfo.Id;
                    OldEducation.SUBJECT_ID = EducationInfo.SubjectId;
                    OldEducation.INSTITUTION_NAME = EducationInfo.InstitutionName;
                    OldEducation.BOARD_UNIVERSITY_ID = EducationInfo.BoardUniversityId;
                    OldEducation.PASSING_YEAR = EducationInfo.PassingYear;
                    OldEducation.RESULT_TYPE_ID = EducationInfo.ResultTypeId;
                    OldEducation.CGPA = EducationInfo.Cgpa;
                    OldEducation.OUT_OF = EducationInfo.OutOf;
                    OldEducation.CLASS_DIVISION = EducationInfo.ClassDivision;
                    OldEducation.SESSION = EducationInfo.Session;
                    OldEducation.EMP_ID = EducationInfo.EmpId;
                    OldEducation.TS = DateTime.Now;
                    OldEducation.ADDEDBY = EducationInfo.AddedBy;
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }
        public string DeleteEducationInfo(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldEducationInfo = db.T_TRAN_EDUCATION_INFO.Where(s => s.ID == id).FirstOrDefault();
                if (OldEducationInfo != null)
                {
                    db.T_TRAN_EDUCATION_INFO.Remove(OldEducationInfo);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }
        #endregion
        #region Employment Info
        public string SaveEmploymentInfo(ComonModel.ClsEmploymentInfo EmploymentInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_EMPLOYMENT_INFO objT_TRAN_EMPLOYMENT_INFO = db.T_TRAN_EMPLOYMENT_INFO.Where(s => s.ID == EmploymentInfo.Id).FirstOrDefault();

                if (objT_TRAN_EMPLOYMENT_INFO == null)
                {
                    objT_TRAN_EMPLOYMENT_INFO = new T_TRAN_EMPLOYMENT_INFO();
                    db.T_TRAN_EMPLOYMENT_INFO.Add(objT_TRAN_EMPLOYMENT_INFO);
                }
                objT_TRAN_EMPLOYMENT_INFO.COMPANY_NAME = EmploymentInfo.CompanyName;
                objT_TRAN_EMPLOYMENT_INFO.ADDEDBY = EmploymentInfo.AddedBy;
                objT_TRAN_EMPLOYMENT_INFO.AREA_OF_EXPERIENCE = EmploymentInfo.AreaOfExperience;
                objT_TRAN_EMPLOYMENT_INFO.COMPANY_LOCATION = EmploymentInfo.CompanyLocation;
                objT_TRAN_EMPLOYMENT_INFO.DEPARTMENT_NAME = EmploymentInfo.DepartmentName;
                objT_TRAN_EMPLOYMENT_INFO.DESIGNATION = EmploymentInfo.Designation;
                objT_TRAN_EMPLOYMENT_INFO.EMP_ID = EmploymentInfo.EmpId;
                objT_TRAN_EMPLOYMENT_INFO.END_DATE = EmploymentInfo.EndDate;
                objT_TRAN_EMPLOYMENT_INFO.START_DATE = EmploymentInfo.StartDate;
                objT_TRAN_EMPLOYMENT_INFO.ROLES_RESPONSIBILITY = EmploymentInfo.RolesResponsibility;
                objT_TRAN_EMPLOYMENT_INFO.TS =DateTime.Now;
                db.SaveChanges();

            }

            if (EmploymentInfo.EmpId > 0)
                return EmploymentInfo.EmpId.ToString();
            else
                return "Error While Saving Employment Info !";
        }
        public IEnumerable<ComonModel.ClsEmploymentInfo> GetAllEmploymentInfo()
        {
            IList<ComonModel.ClsEmploymentInfo> AllEmployment = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllEmployment = db.T_TRAN_EMPLOYMENT_INFO.Select(s => new ComonModel.ClsEmploymentInfo()
                {
                   AreaOfExperience=s.AREA_OF_EXPERIENCE,
                   CompanyLocation=s.COMPANY_LOCATION,
                   CompanyName=s.COMPANY_NAME,
                   DepartmentName=s.DEPARTMENT_NAME,
                   Designation=s.DESIGNATION,
                   EmpId=s.EMP_ID,
                   EndDate=s.END_DATE,
                   Id=s.ID,
                   RolesResponsibility=s.ROLES_RESPONSIBILITY,
                   StartDate=s.START_DATE,                  
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllEmployment;
        }
        public string DeleteEmploymentInfo(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldEmploymentInfo = db.T_TRAN_EMPLOYMENT_INFO.Where(s => s.ID == id).FirstOrDefault();
                if (OldEmploymentInfo != null)
                {
                    db.T_TRAN_EMPLOYMENT_INFO.Remove(OldEmploymentInfo);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }

        #endregion
        #region Address Info
        public string SaveAddressInfo(ComonModel.ClsAddressInfo AddressInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRANS_ADDRESS_INFO objT_TRANS_ADDRESS_INFO = db.T_TRANS_ADDRESS_INFO.Where(s => s.EMP_ID == AddressInfo.EmpId).FirstOrDefault();

                if (objT_TRANS_ADDRESS_INFO == null)
                {
                    objT_TRANS_ADDRESS_INFO = new T_TRANS_ADDRESS_INFO();
                    db.T_TRANS_ADDRESS_INFO.Add(objT_TRANS_ADDRESS_INFO);
                }
                objT_TRANS_ADDRESS_INFO.EMP_ID = AddressInfo.EmpId;
                objT_TRANS_ADDRESS_INFO.PERMANENT_DISTRICT_ID = AddressInfo.PermanentDistrictId;
                objT_TRANS_ADDRESS_INFO.PERMANENT_POLICE_ID = AddressInfo.PermanentPoliceId;
                objT_TRANS_ADDRESS_INFO.PERMANENT_POST_ID = AddressInfo.PermanentPostId;
                objT_TRANS_ADDRESS_INFO.PERMANENT_ROAD = AddressInfo.PermanentRoad;
                objT_TRANS_ADDRESS_INFO.PERMANENT_VILLAGE = AddressInfo.PermanentVillage;
                objT_TRANS_ADDRESS_INFO.PRESENT_DISTRICT_ID = AddressInfo.PresentDistrictId;
                objT_TRANS_ADDRESS_INFO.PRESENT_POLICE_ID = AddressInfo.PresentPoliceId;
                objT_TRANS_ADDRESS_INFO.PRESENT_POST_ID = AddressInfo.PresentPostId;
                objT_TRANS_ADDRESS_INFO.PRESENT_ROAD = AddressInfo.PresentRoad;
                objT_TRANS_ADDRESS_INFO.PRESENT_VILLAGE = AddressInfo.PresentVillage;
                objT_TRANS_ADDRESS_INFO.TS = DateTime.Now;
                objT_TRANS_ADDRESS_INFO.SAME_AS_PRESENT_ADDR = AddressInfo.SameAsPresentAddr;
              
                db.SaveChanges();

            }




            if (AddressInfo.EmpId > 0)
            {

                return AddressInfo.EmpId.ToString();
            }

            else return "Error while saving Address info !";

        }
        public IEnumerable<ComonModel.ClsAddressInfo> GetAllAddressInfo()
        {
            IList<ComonModel.ClsAddressInfo> AllAddress = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllAddress = db.T_TRANS_ADDRESS_INFO.Select(s => new ComonModel.ClsAddressInfo()
                {
                    EmpId = s.EMP_ID,
                    Id = s.ID,
                    PermanentDistrictId = s.PERMANENT_DISTRICT_ID,
                    PermanentPoliceId = s.PERMANENT_POLICE_ID,
                    PermanentPostId = s.PERMANENT_POST_ID,
                    PermanentRoad = s.PERMANENT_ROAD,
                    PermanentVillage = s.PERMANENT_VILLAGE,
                    PresentDistrictId = s.PRESENT_DISTRICT_ID,
                    PresentPoliceId = s.PRESENT_POLICE_ID,
                    PresentPostId = s.PRESENT_POST_ID,
                    PresentRoad = s.PRESENT_ROAD,
                    PresentVillage = s.PRESENT_VILLAGE,
                    SameAsPresentAddr = s.SAME_AS_PRESENT_ADDR,
                    Ts = s.TS,
                    PermanentDistrictName = db.T_SYS_DISTRICT_MASTER.Where(d => d.DISTRICT_ID == s.PERMANENT_DISTRICT_ID).FirstOrDefault().DESCRIPTION,
                    PermanentPolliceStationName=db.T_SYS_POLICESTATION_MASTER.Where(d=>d.POLICESTATION_ID==s.PERMANENT_POLICE_ID).FirstOrDefault().DESCRIPTION,
                    PermanentPostOfficeName=db.T_SYS_POSTOFFICE_MASTER.Where(d=>d.POSTOFFICE_ID==s.PERMANENT_POST_ID).FirstOrDefault().DESCRIPTION,
                    PresentDistrictName=db.T_SYS_DISTRICT_MASTER.Where(d=>d.DISTRICT_ID==s.PRESENT_DISTRICT_ID).FirstOrDefault().DESCRIPTION,
                    PresentPolliceStationName = db.T_SYS_POLICESTATION_MASTER.Where(d => d.POLICESTATION_ID == s.PRESENT_POLICE_ID).FirstOrDefault().DESCRIPTION,
                    PresentPostOfficeName = db.T_SYS_POSTOFFICE_MASTER.Where(d => d.POSTOFFICE_ID == s.PRESENT_POST_ID).FirstOrDefault().DESCRIPTION,
                    //AddedBy = s.ADDEDBY,
                    //Ts = s.TS
                }).ToList();
            }
            return AllAddress;
        }
        public string DeleteAddressInfo(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldAddressInfo = db.T_TRANS_ADDRESS_INFO.Where(s => s.EMP_ID == id).FirstOrDefault();
                if (OldAddressInfo != null)
                {
                    db.T_TRANS_ADDRESS_INFO.Remove(OldAddressInfo);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }
        #endregion
        #region Certification
       
        public string SaveCertificationInfo(ComonModel.ClsCertificationInfo CertificationInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_CERTIFICATION_INFO objT_TRAN_CERTIFICATION_INFO = db.T_TRAN_CERTIFICATION_INFO.Where(s => s.ID == CertificationInfo.Id).FirstOrDefault();

                if (objT_TRAN_CERTIFICATION_INFO == null)
                {
                    objT_TRAN_CERTIFICATION_INFO = new T_TRAN_CERTIFICATION_INFO();
                    db.T_TRAN_CERTIFICATION_INFO.Add(objT_TRAN_CERTIFICATION_INFO);
                }
                objT_TRAN_CERTIFICATION_INFO.CERT_NAME = CertificationInfo.CertName;
                objT_TRAN_CERTIFICATION_INFO.EXAM_DETAILS = CertificationInfo.ExamDetails;
                objT_TRAN_CERTIFICATION_INFO.CERT_AUTHORITY = CertificationInfo.CertAuthority;
                objT_TRAN_CERTIFICATION_INFO.LOCATION = CertificationInfo.Location;
                objT_TRAN_CERTIFICATION_INFO.EXPIRY_DATE = CertificationInfo.ExpiryDate;
                objT_TRAN_CERTIFICATION_INFO.CERT_TYPE = CertificationInfo.CertType;
                objT_TRAN_CERTIFICATION_INFO.OBTAINED_DATE = CertificationInfo.ObtainedDate;
                objT_TRAN_CERTIFICATION_INFO.EMP_ID = CertificationInfo.EmpId;
                objT_TRAN_CERTIFICATION_INFO.ADDEDBY = CertificationInfo.AddedBy;
                objT_TRAN_CERTIFICATION_INFO.TS = DateTime.Now;
                db.SaveChanges();
            };



            if (CertificationInfo.EmpId > 0)
                return CertificationInfo.EmpId.ToString();
            else
                return "Error While saving Certification info !";
        }
        public IEnumerable<ComonModel.ClsCertificationInfo> GetAllCertificationInfo()
        {
            IList<ComonModel.ClsCertificationInfo> AllCertification = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllCertification = db.T_TRAN_CERTIFICATION_INFO.Select(s => new ComonModel.ClsCertificationInfo()
                {
                    Id = s.ID,   
                    AddedBy=s.ADDEDBY,
                    CertAuthority=s.CERT_AUTHORITY,
                    CertName=s.CERT_NAME,
                    CertType=s.CERT_TYPE,
                    EmpId=s.EMP_ID,
                    ExamDetails=s.EXAM_DETAILS,
                    ExpiryDate=s.EXPIRY_DATE,
                    Location=s.LOCATION,
                    ObtainedDate=s.OBTAINED_DATE,                
                    Ts = s.TS,


                }).ToList();
            }
            return AllCertification;
        }
        public string DeleteCertificationInfo(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldCertificationInfo = db.T_TRAN_CERTIFICATION_INFO.Where(s => s.ID == id).FirstOrDefault();
                if (OldCertificationInfo != null)
                {
                    db.T_TRAN_CERTIFICATION_INFO.Remove(OldCertificationInfo);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }
        #endregion
        #region Project Experience
        public string SaveProjectExperienceInfo(ComonModel.ClsProjectExperienceInfo ProjectExperienceInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_PROJECT_EXPERIENCE_INFO objT_TRAN_PROJECT_EXPERIENCE_INFO = db.T_TRAN_PROJECT_EXPERIENCE_INFO.Where(s => s.ID == ProjectExperienceInfo.Id).FirstOrDefault();

                if (objT_TRAN_PROJECT_EXPERIENCE_INFO == null)
                {
                    objT_TRAN_PROJECT_EXPERIENCE_INFO = new T_TRAN_PROJECT_EXPERIENCE_INFO();
                    db.T_TRAN_PROJECT_EXPERIENCE_INFO.Add(objT_TRAN_PROJECT_EXPERIENCE_INFO);
                }
                objT_TRAN_PROJECT_EXPERIENCE_INFO.ADDEDBY = ProjectExperienceInfo.addedBy;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.PROJECT_NAME = ProjectExperienceInfo.ProjectName;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.COMPANY_NAME = ProjectExperienceInfo.CompanyName;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.CUSTOMER_NAME = ProjectExperienceInfo.CustomerName;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.END_DATE = ProjectExperienceInfo.EndDate;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.PROJECT_NAME = ProjectExperienceInfo.ProjectName;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.START_DATE = ProjectExperienceInfo.StartDate;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.EMP_ID = ProjectExperienceInfo.EmpId;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.ROLES_RESPONSIBILITY = ProjectExperienceInfo.RolesResponsibility;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.TECHNOLOGY_USED = ProjectExperienceInfo.TechnologyUsed;
                objT_TRAN_PROJECT_EXPERIENCE_INFO.TS = DateTime.Now;
                db.SaveChanges();
            }

            if (ProjectExperienceInfo.EmpId > 0)
                return ProjectExperienceInfo.EmpId.ToString();
            else
                return "Error While Saving Project Experience Info !";
        }
        public IEnumerable<ComonModel.ClsProjectExperienceInfo> GetAllProjectExperienceInfo()
        {
            IList<ComonModel.ClsProjectExperienceInfo> AllProjectExperience = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllProjectExperience = db.T_TRAN_PROJECT_EXPERIENCE_INFO.Select(s => new ComonModel.ClsProjectExperienceInfo()
                {
                    addedBy=s.ADDEDBY,
                    CompanyName=s.COMPANY_NAME,
                    CustomerName=s.CUSTOMER_NAME,
                    EmpId=s.EMP_ID,
                    EndDate=s.END_DATE,
                    Id=s.ID,
                    ProjectName=s.PROJECT_NAME,
                    RolesResponsibility=s.ROLES_RESPONSIBILITY,
                    StartDate=s.START_DATE,
                    TechnologyUsed=s.TECHNOLOGY_USED,
                    ts=s.TS,
                   

                }).ToList();
            }
            return AllProjectExperience;
        }
        public string DeleteProjectExperienceInfo(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldProjectExperienceInfo = db.T_TRAN_PROJECT_EXPERIENCE_INFO.Where(s => s.ID == id).FirstOrDefault();
                if (OldProjectExperienceInfo != null)
                {
                    db.T_TRAN_PROJECT_EXPERIENCE_INFO.Remove(OldProjectExperienceInfo);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }
        #endregion
        #region Training Info
        public string SaveTrainingInfo(ComonModel.ClsTrainingInfo TrainingInfo)
        {
           
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_TRAINING_INFO objT_TRAN_TRAINING_INFO = db.T_TRAN_TRAINING_INFO.Where(s => s.ID == TrainingInfo.Id).FirstOrDefault();
                if (objT_TRAN_TRAINING_INFO == null)
                {
                    objT_TRAN_TRAINING_INFO = new T_TRAN_TRAINING_INFO();
                    db.T_TRAN_TRAINING_INFO.Add(objT_TRAN_TRAINING_INFO);
                }
                   objT_TRAN_TRAINING_INFO. TRAINING_TYPE = TrainingInfo.TrainingType;
                   objT_TRAN_TRAINING_INFO. TRAINING_TITLE = TrainingInfo.TrainingTitle;
                   objT_TRAN_TRAINING_INFO. TRAINING_DETAILS = TrainingInfo.TrainingDetails;
                   objT_TRAN_TRAINING_INFO. INSTITUTION_NAME = TrainingInfo.InstitutionName;
                   objT_TRAN_TRAINING_INFO. LOCATION = TrainingInfo.Location;
                   objT_TRAN_TRAINING_INFO. COUNTRY_ID = TrainingInfo.CountryId;
                   objT_TRAN_TRAINING_INFO. DURATION = TrainingInfo.Duration;
                   objT_TRAN_TRAINING_INFO. TRAINING_START_DATE = TrainingInfo.TrainingStartDate;
                   objT_TRAN_TRAINING_INFO. TRAINING_END_DATE = TrainingInfo.TrainingEndDate;
                   objT_TRAN_TRAINING_INFO. BOND_FOR = TrainingInfo.BondFor;
                   objT_TRAN_TRAINING_INFO. IS_LEGAL_BOND = TrainingInfo.IsLegalBond;
                   objT_TRAN_TRAINING_INFO. EFFECTIVE_DATE = TrainingInfo.EffectiveDate;
                   objT_TRAN_TRAINING_INFO. END_DATE = TrainingInfo.EndDate;
                   objT_TRAN_TRAINING_INFO. EMP_ID = TrainingInfo.EmpId;
                   objT_TRAN_TRAINING_INFO. TS = DateTime.Now;
                   objT_TRAN_TRAINING_INFO. ADDEDBY = TrainingInfo.AddedBy;
           
                db.SaveChanges();
            }
            if (TrainingInfo.EmpId > 0)
            {

                return TrainingInfo.EmpId.ToString();
            }

            else return "Error while saving Training info !";

        }
        public IEnumerable<ComonModel.ClsTrainingInfo> GetAllTrainingInfo()
        {
            IList<ComonModel.ClsTrainingInfo> AllTraining = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllTraining = db.T_TRAN_TRAINING_INFO.Select(s => new ComonModel.ClsTrainingInfo()
                {
                    Id = s.ID,
                    AddedBy = s.ADDEDBY,                    
                    EmpId = s.EMP_ID,
                    EndDate = s.END_DATE,
                    BondFor=s.BOND_FOR,
                    CountryId=s.COUNTRY_ID,
                    Duration=s.DURATION,
                    CountryName=db.T_SYS_COUNTRY_MASTER.Where(d=>d.COUNTRY_ID==s.COUNTRY_ID).FirstOrDefault().COUNTRY_NAME,
                    EffectiveDate=s.EFFECTIVE_DATE,
                    InstitutionName=s.INSTITUTION_NAME,
                    IsLegalBond=s.IS_LEGAL_BOND,
                    Location=s.LOCATION,
                    TrainingDetails=s.TRAINING_DETAILS,
                    TrainingEndDate=s.TRAINING_END_DATE,
                    TrainingStartDate=s.TRAINING_START_DATE,
                    TrainingTitle=s.TRAINING_TITLE,
                    TrainingType =s.TRAINING_TYPE,                                   
                    Ts = s.TS,


                }).ToList();
            }
            return AllTraining;
        }
        public string DeleteTrainingInfo(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldTrainingInfo = db.T_TRAN_TRAINING_INFO.Where(s => s.ID == id).FirstOrDefault();
                if (OldTrainingInfo != null)
                {
                    db.T_TRAN_TRAINING_INFO.Remove(OldTrainingInfo);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }
        #endregion
        #region Personal Info
        public string SavePersonalInfo(ComonModel.Personal PersonalInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_PERSONAL_INFO objT_TRAN_PERSONAL_INFO = db.T_TRAN_PERSONAL_INFO.Where(s => s.DOMEN_ID == PersonalInfo.DomenId).FirstOrDefault();

                if (objT_TRAN_PERSONAL_INFO == null)
                {
                    objT_TRAN_PERSONAL_INFO = new T_TRAN_PERSONAL_INFO();
                    db.T_TRAN_PERSONAL_INFO.Add(objT_TRAN_PERSONAL_INFO);
                }
                objT_TRAN_PERSONAL_INFO.PERSONAL_ID = PersonalInfo.PersonalId;
                objT_TRAN_PERSONAL_INFO.DOMEN_ID = PersonalInfo.DomenId;
                objT_TRAN_PERSONAL_INFO.SALUTATION_ID = PersonalInfo.SalutationId;
                objT_TRAN_PERSONAL_INFO.FIRST_NAME = PersonalInfo.FirstName;
                objT_TRAN_PERSONAL_INFO.LAST_NAME = PersonalInfo.LastName;
                objT_TRAN_PERSONAL_INFO.FATHER_NAME = PersonalInfo.FatherName;
                objT_TRAN_PERSONAL_INFO.FATHER_OCCUPATION_ID = PersonalInfo.FatherOccupationId;
                objT_TRAN_PERSONAL_INFO.MOTHER_NAME = PersonalInfo.PersonalId;
                objT_TRAN_PERSONAL_INFO.MOTHER_OCCUPATION_ID = PersonalInfo.MotherOccupationId;
                objT_TRAN_PERSONAL_INFO.DATE_OF_BIRTH = PersonalInfo.DateOfBirth;
                objT_TRAN_PERSONAL_INFO.RELIGION_ID = PersonalInfo.ReligionId;
                objT_TRAN_PERSONAL_INFO.BLOOD_GROUP_ID = PersonalInfo.BloodGroupId;
                objT_TRAN_PERSONAL_INFO.GENDER_ID = PersonalInfo.GenderId;
                objT_TRAN_PERSONAL_INFO.MARTIAL_STATUS_ID = PersonalInfo.MartialStatusId;
                objT_TRAN_PERSONAL_INFO.SPOUSE_NAME = PersonalInfo.SpouseName;
                objT_TRAN_PERSONAL_INFO.SPOUSE_OCCUPATION_ID = PersonalInfo.SpouseOccupationId;
                objT_TRAN_PERSONAL_INFO.NATIONALITY_ID = PersonalInfo.NationalityId;
                objT_TRAN_PERSONAL_INFO.NATIONAL_ID = PersonalInfo.NationalId;
                objT_TRAN_PERSONAL_INFO.BIRTH_ID = PersonalInfo.PersonalId;
                objT_TRAN_PERSONAL_INFO.PASSPORT_NO = PersonalInfo.PersonalId;
                objT_TRAN_PERSONAL_INFO.PASSPORT_EXPIRY = PersonalInfo.PassportExpiry;
                objT_TRAN_PERSONAL_INFO.DESIGNATION_ID = PersonalInfo.DesignationId;
                objT_TRAN_PERSONAL_INFO.DEPARTMENT_ID = PersonalInfo.DepartmentId;
                objT_TRAN_PERSONAL_INFO.BRANCH_ID = PersonalInfo.BranchId;
                objT_TRAN_PERSONAL_INFO.EMP_TYPE_ID = PersonalInfo.EmpTypeId;
                objT_TRAN_PERSONAL_INFO.JOINING_DATE = PersonalInfo.JoiningDate;
                objT_TRAN_PERSONAL_INFO.CONFIRMATION_DATE = PersonalInfo.ConfirmationDate;
                objT_TRAN_PERSONAL_INFO.BANK_ACCOUNT = PersonalInfo.BankAccount;
                objT_TRAN_PERSONAL_INFO.BANK_ID = PersonalInfo.BankId;
                objT_TRAN_PERSONAL_INFO.OFFICE_MOBILE = PersonalInfo.OfficeMobile;
                objT_TRAN_PERSONAL_INFO.OFFICE_EXT = PersonalInfo.OfficeExt;
                objT_TRAN_PERSONAL_INFO.PERSONAL_MOBILE = PersonalInfo.PersonalMobile;
                objT_TRAN_PERSONAL_INFO.OFFICE_EMAIL = PersonalInfo.OfficeEmail;
                objT_TRAN_PERSONAL_INFO.PERSONAL_EMAIL = PersonalInfo.PersonalEmail;
                objT_TRAN_PERSONAL_INFO.EMER_PERSON = PersonalInfo.EmerPerson;
                objT_TRAN_PERSONAL_INFO.EMER_PERSON_MOBILE = PersonalInfo.EmerPersonMobile;
                objT_TRAN_PERSONAL_INFO.RELATION = PersonalInfo.Relation;
                objT_TRAN_PERSONAL_INFO.EMER_PERSON_ADDR = PersonalInfo.EmerPersonAddr;
                objT_TRAN_PERSONAL_INFO.TS = PersonalInfo.Ts;
                objT_TRAN_PERSONAL_INFO.LOGIN_ID = PersonalInfo.LoginId;
                db.SaveChanges();

                if (objT_TRAN_PERSONAL_INFO.EMP_ID > 0)
                {
                    if (PersonalInfo.SiblingsInfoList.Count > 0)
                    {
                        foreach (ComonModel.ClsSiblingInfo objSiblings in PersonalInfo.SiblingsInfoList)
                        {
                            T_TRAN_SIBLING_INFO objT_TRAN_SIBLING_INFO = db.T_TRAN_SIBLING_INFO.Where(s => s.ID == objSiblings.Id).FirstOrDefault();

                            if (objT_TRAN_SIBLING_INFO == null)
                            {
                                objT_TRAN_SIBLING_INFO = new T_TRAN_SIBLING_INFO();
                                db.T_TRAN_SIBLING_INFO.Add(objT_TRAN_SIBLING_INFO);
                            }

                            objT_TRAN_SIBLING_INFO.FULL_NAME = objSiblings.FullName;
                            objT_TRAN_SIBLING_INFO.DOB = objSiblings.Dob;
                            objT_TRAN_SIBLING_INFO.MOBILE_NO = objSiblings.MobileNo;
                            objT_TRAN_SIBLING_INFO.OCCUPATION_ID = objSiblings.OccupationId;
                            objT_TRAN_SIBLING_INFO.NO_OF_SIBLINGS = objSiblings.NoOfSiblings;
                            objT_TRAN_SIBLING_INFO.SIBLINGS_GENDER = objSiblings.SiblingsGender;
                            objT_TRAN_SIBLING_INFO.ADDEDBY = objSiblings.AddedBy;
                            objT_TRAN_SIBLING_INFO.TS = DateTime.Now;
                            objT_TRAN_SIBLING_INFO.EMP_ID = objT_TRAN_PERSONAL_INFO.EMP_ID;
                            objT_TRAN_SIBLING_INFO.LOGIN_ID = PersonalInfo.LoginId;

                        }
                        db.SaveChanges();
                    }

                    if (PersonalInfo.ChildInfoList.Count > 0)
                    {
                        foreach (ComonModel.ClsChildInfo objChildInfo in PersonalInfo.ChildInfoList)
                        {
                            T_TRAN_CHILD_INFO objT_TRAN_CHILD_INFO = db.T_TRAN_CHILD_INFO.Where(s => s.CHILD_ID == objChildInfo.ChildId).FirstOrDefault();

                            if (objT_TRAN_CHILD_INFO == null)
                            {
                                objT_TRAN_CHILD_INFO = new T_TRAN_CHILD_INFO();
                                db.T_TRAN_CHILD_INFO.Add(objT_TRAN_CHILD_INFO);
                            }
                            objT_TRAN_CHILD_INFO.FULL_NAME = objChildInfo.FullName;
                            objT_TRAN_CHILD_INFO.DOB = objChildInfo.Dob;
                            objT_TRAN_CHILD_INFO.MOBILE_NO = objChildInfo.MobileNo;
                            objT_TRAN_CHILD_INFO.OCCUPATION = objChildInfo.Occupation;
                            objT_TRAN_CHILD_INFO.NO_OF_CHILD = objChildInfo.NoOfChild;
                            objT_TRAN_CHILD_INFO.CHILD_GENDER = objChildInfo.ChildGender;
                            objT_TRAN_CHILD_INFO.ADDEDBY = objChildInfo.AddedBy;
                            objT_TRAN_CHILD_INFO.TS = DateTime.Now;
                            objT_TRAN_CHILD_INFO.EMP_ID = objT_TRAN_PERSONAL_INFO.EMP_ID;
                            objT_TRAN_CHILD_INFO.LOGIN_ID = PersonalInfo.LoginId;

                        }
                        db.SaveChanges();
                    }

                    // return PersonalInfo.EmpId.ToString();
                }

            }
            if (PersonalInfo.EmpId > 0)
            {

                return PersonalInfo.EmpId.ToString();
            }

            else return "Error while saving personal info !";


        }
        #endregion
        #region Exit Interview
        public string ExitInterViewSaveInfo(ComonModel.ClsExitInterview ExitInterViewSaveInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_EXIT_INTERVIEW objT_TRAN_EXIT_INTERVIEW = db.T_TRAN_EXIT_INTERVIEW.Where(s => s.EMP_ID == ExitInterViewSaveInfo.EmpId).FirstOrDefault();

                if (objT_TRAN_EXIT_INTERVIEW == null)
                {
                    objT_TRAN_EXIT_INTERVIEW = new T_TRAN_EXIT_INTERVIEW();
                    db.T_TRAN_EXIT_INTERVIEW.Add(objT_TRAN_EXIT_INTERVIEW);
                }
                objT_TRAN_EXIT_INTERVIEW.EMP_ID = ExitInterViewSaveInfo.EmpId;
                objT_TRAN_EXIT_INTERVIEW.ADDEDBY = ExitInterViewSaveInfo.AddedBy;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO1 = ExitInterViewSaveInfo.QuestionNo1;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO2 = ExitInterViewSaveInfo.QuestionNo2;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO2_1 = ExitInterViewSaveInfo.QuestionNo21;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO2_2 = ExitInterViewSaveInfo.QuestionNo22;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO2_3 = ExitInterViewSaveInfo.QuestionNo23;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO2_4 = ExitInterViewSaveInfo.QuestionNo24;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO3 = ExitInterViewSaveInfo.QuestionNo3;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO4 = ExitInterViewSaveInfo.QuestionNo4;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO5 = ExitInterViewSaveInfo.QuestionNo5;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO6 = ExitInterViewSaveInfo.QuestionNo6;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO7 = ExitInterViewSaveInfo.QuestionNo7;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO8 = ExitInterViewSaveInfo.QuestionNo8;
                objT_TRAN_EXIT_INTERVIEW.QUESTION_NO9 = ExitInterViewSaveInfo.QuestionNo9;
                objT_TRAN_EXIT_INTERVIEW.RELEASE_DATE = ExitInterViewSaveInfo.ReleaseDate;
                objT_TRAN_EXIT_INTERVIEW.REQUEST_FOR_EXP = ExitInterViewSaveInfo.RequestForExp;
                objT_TRAN_EXIT_INTERVIEW.REQUEST_FOR_RELEASE = ExitInterViewSaveInfo.RequestForRelease;
                objT_TRAN_EXIT_INTERVIEW.RESIGNATION_DATE = ExitInterViewSaveInfo.ResignationDate;
                objT_TRAN_EXIT_INTERVIEW.TS = DateTime.Now;
                db.SaveChanges();
            }

            if (ExitInterViewSaveInfo.EmpId > 0)
                return ExitInterViewSaveInfo.EmpId.ToString();
            else
                return "Error While Saving Exit Interview Info !";
        }
        #endregion
        #region Photo And Others
        public string SavePhotoAndOtherInfo(ComonModel.ClsPhotoAndOthers PhotoAndOthersInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                T_TRAN_PHOTO_AND_OTHERS objT_TRAN_PHOTO_AND_OTHERS = db.T_TRAN_PHOTO_AND_OTHERS.Where(s => s.EMP_ID == PhotoAndOthersInfo.EmpId).FirstOrDefault();

                if (objT_TRAN_PHOTO_AND_OTHERS == null)
                {
                    objT_TRAN_PHOTO_AND_OTHERS = new T_TRAN_PHOTO_AND_OTHERS();
                    db.T_TRAN_PHOTO_AND_OTHERS.Add(objT_TRAN_PHOTO_AND_OTHERS);
                }

                objT_TRAN_PHOTO_AND_OTHERS.EXTRA_ACTIVITIES = PhotoAndOthersInfo.ExtraActivities;
                objT_TRAN_PHOTO_AND_OTHERS.EMP_ID = PhotoAndOthersInfo.EmpId;
                objT_TRAN_PHOTO_AND_OTHERS.ADDEDBY = PhotoAndOthersInfo.AddedBy;
                objT_TRAN_PHOTO_AND_OTHERS.PHOTO = PhotoAndOthersInfo.Photo;
                objT_TRAN_PHOTO_AND_OTHERS.PUBLICATION = PhotoAndOthersInfo.Publication;
                objT_TRAN_PHOTO_AND_OTHERS.SIGNATURE = PhotoAndOthersInfo.Signature;
                
                objT_TRAN_PHOTO_AND_OTHERS.TS = DateTime.Now;


                db.SaveChanges();

                if (objT_TRAN_PHOTO_AND_OTHERS.EMP_ID > 0)
                {
                    if (PhotoAndOthersInfo.MemberShipHistoryList.Count > 0)
                    {
                        foreach (ComonModel.ClsMemershipHistory objMembershipHistory in PhotoAndOthersInfo.MemberShipHistoryList)
                        {
                            T_TRANS_MEMERSHIP_HISTORY objT_TRANS_MEMERSHIP_HISTORY = db.T_TRANS_MEMERSHIP_HISTORY.Where(s => s.MEMBER_ID == objMembershipHistory.MemberId).FirstOrDefault();                          
                            if (objT_TRANS_MEMERSHIP_HISTORY == null)
                            {
                                objT_TRANS_MEMERSHIP_HISTORY = new T_TRANS_MEMERSHIP_HISTORY();
                                db.T_TRANS_MEMERSHIP_HISTORY.Add(objT_TRANS_MEMERSHIP_HISTORY);
                            }
                            objT_TRANS_MEMERSHIP_HISTORY.EMP_ID = objMembershipHistory.EmpId;
                            objT_TRANS_MEMERSHIP_HISTORY.MEMBER_ID = objMembershipHistory.MemberId;
                            objT_TRANS_MEMERSHIP_HISTORY.SOCIETY_NAME = objMembershipHistory.SocietyName;
                            objT_TRANS_MEMERSHIP_HISTORY.TS = DateTime.Now;
                            objT_TRANS_MEMERSHIP_HISTORY.ADDRESS = objMembershipHistory.Address;
                            objT_TRANS_MEMERSHIP_HISTORY.ADDEDBY = objMembershipHistory.AddedBy;

                        }
                        db.SaveChanges();
                    }

                }

            }
            if (PhotoAndOthersInfo.EmpId > 0)
            {

                return PhotoAndOthersInfo.EmpId.ToString();
            }

            else return "Error while saving Photo And Others Info !";


        }
        public IEnumerable<ComonModel.ClsMemershipHistory> GetAllMemberShipInfo()
        {
            IList<ComonModel.ClsMemershipHistory> AllMemberShipInfo = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllMemberShipInfo = db.T_TRANS_MEMERSHIP_HISTORY.Select(s => new ComonModel.ClsMemershipHistory()
                {
                   AddedBy=s.ADDEDBY,
                   Address=s.ADDRESS,
                   EmpId=s.EMP_ID,
                   id=s.ID,
                   MemberId=s.MEMBER_ID,
                   SocietyName=s.SOCIETY_NAME,
                    Ts = s.TS,
                }).ToList();
            }
            return AllMemberShipInfo;
        }
        public string DeleteMemberShipInfo(string id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldSocietyMemberInfo = db.T_TRANS_MEMERSHIP_HISTORY.Where(s => s.MEMBER_ID == id).FirstOrDefault();
                if (OldSocietyMemberInfo != null)
                {
                    db.T_TRANS_MEMERSHIP_HISTORY.Remove(OldSocietyMemberInfo);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }
        #endregion
    }
}