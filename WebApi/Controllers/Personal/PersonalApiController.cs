﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers.Personal
{
    public class PersonalApiController : ApiController
    {

        IPersonalRepository iPersonalRepository;
        public PersonalApiController()
        {
            iPersonalRepository = new PersonalRepository();
        }

        //  [Authorize]
        #region Personal Info
        public IHttpActionResult GetAllById(int id)
        {
            return Ok(iPersonalRepository.GetAllById(id));
        }
        public IHttpActionResult SavePersonalInfo(ComonModel.Personal PersonalInfo)
        {
            return Ok(iPersonalRepository.SavePersonalInfo(PersonalInfo));
        }
        #endregion

        #region Training Info
        public IHttpActionResult SaveTrainingInfo(ComonModel.ClsTrainingInfo TrainingInfo)
        {
            return Ok(iPersonalRepository.SaveTrainingInfo(TrainingInfo));
        }
       public IHttpActionResult GetAllTrainingInfo()
        {
            return Ok(iPersonalRepository.GetAllTrainingInfo());
        }
       public IHttpActionResult   DeleteTrainingInfo(int id)
        {
            return Ok(iPersonalRepository.DeleteTrainingInfo(id));
        }
        #endregion
        #region Certification Info
        public IHttpActionResult SaveCertificationInfo(ComonModel.ClsCertificationInfo CertificationInfo)
        {
            return Ok(iPersonalRepository.SaveCertificationInfo(CertificationInfo));
        }
        public IHttpActionResult GetAllCertificationInfo()
        {
            return Ok(iPersonalRepository.GetAllCertificationInfo());
        }
       public IHttpActionResult DeleteCertificationInfo(int id)
        {
            return Ok(iPersonalRepository.DeleteCertificationInfo(id));
        }


        #endregion
        #region Employment Info
        public IHttpActionResult SaveEmploymentInfo(ComonModel.ClsEmploymentInfo EmploymentInfo)
        {
            return Ok(iPersonalRepository.SaveEmploymentInfo(EmploymentInfo));
        }
        public IHttpActionResult DeleteEmploymentInfo(int id)
        {
            return Ok(iPersonalRepository.DeleteEmploymentInfo(id));
        }
        public IHttpActionResult GetAllEmploymentInfo()
        {
            return Ok(iPersonalRepository.GetAllEmploymentInfo());
        }
        #endregion
        #region Project Experience
        public IHttpActionResult SaveProjectExperienceInfo(ComonModel.ClsProjectExperienceInfo ProjectExperienceInfo)
        {
            return Ok(iPersonalRepository.SaveProjectExperienceInfo(ProjectExperienceInfo));
        }
        public IHttpActionResult GetAllProjectExperienceInfo()
        {
            return Ok(iPersonalRepository.GetAllProjectExperienceInfo());
        }
       public IHttpActionResult DeleteProjectExperienceInfo(int id)
        {
            return Ok(iPersonalRepository.DeleteProjectExperienceInfo(id));
        }
        #endregion
        #region Education Info
        public IHttpActionResult GetAllEducationInfo()
        {
            return Ok(iPersonalRepository.GetAllEducationInfo());
        }
        public IHttpActionResult DeleteEducationInfo(int id)
        {
            return Ok(iPersonalRepository.DeleteEducationInfo(id));
        }
        public IHttpActionResult GetEducationById(int id)
        {
            return Ok(iPersonalRepository.GetEducationById(id));
        }
        public IHttpActionResult  UpdateEducationInfo(ComonModel.ClsEducationInfo EducationInfo)
        {
            return Ok(iPersonalRepository.UpdateEducationInfo(EducationInfo));
        }
        public IHttpActionResult SaveEducationInfo(ComonModel.ClsEducationInfo EducationInfo)
        {
            return Ok(iPersonalRepository.SaveEducationInfo(EducationInfo));
        }
        #endregion
       
        #region Address Info
        public IHttpActionResult SaveAddressInfo(ComonModel.ClsAddressInfo AddressInfo)
        {
            return Ok(iPersonalRepository.SaveAddressInfo(AddressInfo));
        }
        public IHttpActionResult GetAllAddressInfo()
        {
            return Ok(iPersonalRepository.GetAllAddressInfo());
        }
        public IHttpActionResult  DeleteAddressInfo(int id)
        {
            return Ok(iPersonalRepository.DeleteAddressInfo(id));
        }
        #endregion
        #region Exit Interview Info
        public IHttpActionResult  ExitInterViewSaveInfo(ComonModel.ClsExitInterview ExitInterViewSaveInfo)
        {
            return Ok(iPersonalRepository.ExitInterViewSaveInfo(ExitInterViewSaveInfo));
        }
        #endregion
        #region Photo And Others
        public IHttpActionResult SavePhotoAndOtherInfo(ComonModel.ClsPhotoAndOthers PhotoAndOthersInfo)
        {
            return Ok(iPersonalRepository.SavePhotoAndOtherInfo(PhotoAndOthersInfo));
        }
        public IHttpActionResult GetAllMemberShipInfo()
        {
            return Ok(iPersonalRepository.GetAllMemberShipInfo());
        }
        public IHttpActionResult DeleteMemberShipInfo(string id)
        {
            return Ok(iPersonalRepository.DeleteMemberShipInfo(id));
        }
        #endregion
    }
}
