﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Controllers.Personal
{
    interface IPersonalRepository
    {
        #region Personal Info
        ComonModel.Personal GetAllById(int id);
        string SavePersonalInfo(ComonModel.Personal PersonalInfo);
        #endregion
        #region Training Info
        string SaveTrainingInfo(ComonModel.ClsTrainingInfo TrainingInfo);
        IEnumerable<ComonModel.ClsTrainingInfo> GetAllTrainingInfo();
        string DeleteTrainingInfo(int id);
        #endregion
        #region Certification Info
        string SaveCertificationInfo(ComonModel.ClsCertificationInfo CertificationInfo);
        IEnumerable<ComonModel.ClsCertificationInfo> GetAllCertificationInfo();
        string DeleteCertificationInfo(int id);
        #endregion
        #region Employment Info
        string SaveEmploymentInfo(ComonModel.ClsEmploymentInfo EmploymentInfo);
        IEnumerable<ComonModel.ClsEmploymentInfo> GetAllEmploymentInfo();
        string DeleteEmploymentInfo(int id);
        #endregion
        #region Project Experience Info
        string SaveProjectExperienceInfo(ComonModel.ClsProjectExperienceInfo ProjectExperienceInfo);
        IEnumerable<ComonModel.ClsProjectExperienceInfo> GetAllProjectExperienceInfo();
        string DeleteProjectExperienceInfo(int id);
        #endregion
        #region Education Info
        string SaveEducationInfo(ComonModel.ClsEducationInfo EducationInfo);
        IEnumerable<ComonModel.ClsEducationInfo> GetAllEducationInfo();
        ComonModel.ClsEducationInfo GetEducationById(int id);
        string UpdateEducationInfo(ComonModel.ClsEducationInfo EducationInfo);
        string DeleteEducationInfo(int id);
        #endregion      
        #region Address Info
        string SaveAddressInfo(ComonModel.ClsAddressInfo AddressInfo);
        IEnumerable<ComonModel.ClsAddressInfo> GetAllAddressInfo();
        string DeleteAddressInfo(int id);
        #endregion
        #region Exit Interview Info
        string ExitInterViewSaveInfo(ComonModel.ClsExitInterview ExitInterViewSaveInfo);
        #endregion
        #region Photo And Others
        string SavePhotoAndOtherInfo(ComonModel.ClsPhotoAndOthers PhotoAndOthersInfo);
        IEnumerable<ComonModel.ClsMemershipHistory> GetAllMemberShipInfo();
        string DeleteMemberShipInfo(string id);
        #endregion
    }
}
