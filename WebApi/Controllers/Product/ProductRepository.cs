﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.Controllers.Product
{
    public class ProductRepository : IProduct
    {
        public IList<ComonModel.ClsProducts> GetProduct()
        {

            //List<ClsProducts> listProduct = new List<ClsProducts>();
            //listProduct.Add(new ClsProducts { Id = 1, Name = "Apple", Category = "Fruits", price = 103 });
            //listProduct.Add(new ClsProducts { Id = 2, Name = "Bottle", Category = "Plastic", price = 50 });
            //return listProduct;

            IList<ComonModel.ClsProducts> products = null;

            using (var ctx = new EMPLOYEE_DBEntities())
            {
                products = ctx.T_PRODUCTS.Select(s => new ComonModel.ClsProducts()
                {
                    Id = s.ID,
                    Name = s.Name,
                    Category = s.Category,
                    price = s.Price
                }).ToList<ComonModel.ClsProducts>();
            }

            return products;


        }
        public ComonModel.ClsProducts FindProductById(int id)
        {

            //List<ClsProducts> listProduct = new List<ClsProducts>();
            //listProduct.Add(new ClsProducts { Id = 1, Name = "Apple", Category = "Fruits", price = 103 });
            //listProduct.Add(new ClsProducts { Id = 2, Name = "Bottle", Category = "Plastic", price = 50 });
            //return listProduct;

            ComonModel.ClsProducts product = new ComonModel.ClsProducts();

            using (var ctx = new EMPLOYEE_DBEntities())
            {
                var existingProduct = ctx.T_PRODUCTS.Where(s => s.ID == id).FirstOrDefault<T_PRODUCTS>();


                if (existingProduct != null)
                {
                    product.Id = existingProduct.ID;
                    product.Name = existingProduct.Name;
                    product.Category = existingProduct.Category;
                    product.price = existingProduct.Price;
                }
                product.listBankinfo= ctx.T_SYS_BANK_INFO.Select(s => new ComonModel.ClsBankInfo()
                {
                    BankId = s.BANK_ID,
                    BankName = s.BANK_NAME,
                    AddedBy=null,
                    Ts=null,
                    Active=true
                }).ToList();

                product.listCountry = ctx.T_SYS_COUNTRY_MASTER.Select(s => new ComonModel.ClsCountryMaster()
                {
                    CountryId = s.COUNTRY_ID,
                    CountryName = s.COUNTRY_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS,
                    Nationality = s.NATIONALITY
                }).ToList();
            }
            return product;


        }


        public string PostNewStudent(ComonModel.ClsProducts product)
        {


            using (var ctx = new EMPLOYEE_DBEntities())
            {
                ctx.T_PRODUCTS.Add(new T_PRODUCTS()
                {
                    ID = product.Id,
                    Name = product.Name,
                    Category = product.Category,
                    Price = product.price
                });

                ctx.SaveChanges();
            }

            return "Saved Successfully";
        }

        public string UpdateProduct(ComonModel.ClsProducts product)
        {

            using (var ctx = new EMPLOYEE_DBEntities())
            {
                var existingProduct = ctx.T_PRODUCTS.Where(s => s.ID == product.Id).FirstOrDefault<T_PRODUCTS>();

                if (existingProduct != null)
                {
                    existingProduct.Name = product.Name;
                    existingProduct.Category = product.Category;
                    existingProduct.Price = product.price;
                    ctx.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

    }
}