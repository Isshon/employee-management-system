﻿using ComonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Controllers.Product
{
    interface IProduct
    {
        IList<ClsProducts> GetProduct();
        ClsProducts FindProductById(int id);

        string PostNewStudent(ComonModel.ClsProducts product);
        string UpdateProduct(ComonModel.ClsProducts product);
    }
}
