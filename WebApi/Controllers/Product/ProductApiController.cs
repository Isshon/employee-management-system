﻿using ComonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers.Product
{
    public class ProductApiController : ApiController
    {
        IProduct iproduct;      
        public ProductApiController()
        {
            iproduct = new ProductRepository();
        }

        [Authorize]
        public IHttpActionResult Authorize()
        {
            return Ok("Authorize");
        }
        [Authorize]
        public IHttpActionResult GetAllProducts()
        {
            return Ok(iproduct.GetProduct().ToList());
        }

        public IHttpActionResult GetProduct(int id)
        {
            return Ok(iproduct.FindProductById(id));

        }

        public IHttpActionResult PostNewStudent(ClsProducts product)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            iproduct.PostNewStudent(product);
            return Ok();
        }
        public IHttpActionResult UpdateProduct(ClsProducts product)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            return Ok(iproduct.UpdateProduct(product));

        }
    }
}
