﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers.LoginApi
{
    public class LoginApiController : ApiController
    {
        ILogin iLogin;

        public LoginApiController()
        {
            iLogin = new LoginRepository();
        }


        [HttpPost]
        public bool Login(string loginId, string password)
        {
            return iLogin.Login(loginId, password);
        }
    }
}
