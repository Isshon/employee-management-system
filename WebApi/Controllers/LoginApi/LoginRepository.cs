﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Linq;
using System.Web;

namespace WebApi.Controllers.LoginApi
{
    public class LoginRepository : ILogin
    {

        public bool Login(string loginId,string password)
        {
            string serverName = ConfigurationManager.AppSettings["ADServer"];
            bool result = false;
            try
            {
                //Check user credentials

                DirectoryEntry Entry = new DirectoryEntry("LDAP://" + serverName, loginId, password);
                object nativeObject = Entry.NativeObject;
                result = true;
            }
            catch (Exception ex)
            {
                result = false;

            }

            return result;
        }

    }
}