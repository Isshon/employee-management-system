﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Controllers.Parameter
{
    interface IParameterRepository
    {
        ComonModel.ClsAlldropdownData GetAllDropdownData();
        IEnumerable<ComonModel.ClsPolicestationMaster> GetPoliceStationByDistrictId(int id);
        IEnumerable<ComonModel.ClsPostofficeMaster> GetOfficeByDistrictId(int id);

        IEnumerable<ComonModel.ClsEmployeeTypeInfo> GetAllEmployeeType();
        ComonModel.ClsEmployeeTypeInfo GetEmployeeTypeId(int id);
        string CreateEmployeeType(ComonModel.ClsEmployeeTypeInfo EmployeeTypeInfo);
        string UpdateEmployeeType(ComonModel.ClsEmployeeTypeInfo EmployeeTypeInfo);
        string DeleteEmployeeType(int id);


        IEnumerable<ComonModel.ClsBankInfo> GetAllBank();
        ComonModel.ClsBankInfo GetBankId(int id);
        string CreateBank(ComonModel.ClsBankInfo BankInfo);
        string UpdateBank(ComonModel.ClsBankInfo BankInfo);
        string DeleteBank(int id);


        IEnumerable<ComonModel.ClsBloodGroupInfo> GetAllBloodGroup();
        ComonModel.ClsBloodGroupInfo GetBloodGroupId(int id);
        string CreateBloodGroup(ComonModel.ClsBloodGroupInfo BloodGroupInfo);
        string UpdateBloodGroup(ComonModel.ClsBloodGroupInfo BloodGroupInfo);
        string DeleteBloodGroup(int id);


        IEnumerable<ComonModel.ClsBranchOfficeInfo> GetAllBranchOffice();
        ComonModel.ClsBranchOfficeInfo GetBranchOfficeId(int id);
        string CreateBranchOffice(ComonModel.ClsBranchOfficeInfo BranchOfficeInfo);
        string UpdateBranchOffice(ComonModel.ClsBranchOfficeInfo BranchOfficeInfo);
        string DeleteBranchOffice(int id);

        IEnumerable<ComonModel.ClsCountryMaster> GetAllCountry();
        ComonModel.ClsCountryMaster GetCountryId(int id);
        string CreateCountry(ComonModel.ClsCountryMaster CountryMaster);
        string UpdateCountry(ComonModel.ClsCountryMaster CountryMaster);
        string DeleteCountry(int id);


        IEnumerable<ComonModel.ClsCourseInfo> GetAllCourse();
        ComonModel.ClsCourseInfo GetCourseId(int id);
        string CreateCourse(ComonModel.ClsCourseInfo CourseInfo);
        string UpdateCourse(ComonModel.ClsCourseInfo CourseInfo);
        string DeleteCourse(int id);


        IEnumerable<ComonModel.ClsDegreeInfo> GetAllDegree();
        ComonModel.ClsDegreeInfo GetDegreeId(int id);
        string CreateDegree(ComonModel.ClsDegreeInfo DegreeInfo);
        string UpdateDegree(ComonModel.ClsDegreeInfo DegreeInfo);
        string DeleteDegree(int id);


        IEnumerable<ComonModel.ClsDepartmentInfo> GetAllDepartment();
        ComonModel.ClsDepartmentInfo GetDepartmentId(int id);
        string CreateDepartment(ComonModel.ClsDepartmentInfo DepartmentInfo);
        string UpdateDepartment(ComonModel.ClsDepartmentInfo DepartmentInfo);
        string DeleteDepartment(int id);


        IEnumerable<ComonModel.ClsDesignationMaster> GetAllDesignation();
        ComonModel.ClsDesignationMaster GetDesignationId(int id);
        string CreateDesignation(ComonModel.ClsDesignationMaster DesignationMaster);
        string UpdateDesignation(ComonModel.ClsDesignationMaster DesignationMaster);
        string DeleteDesignation(int id);


        IEnumerable<ComonModel.ClsDistrictMaster> GetAllDistrict();
        ComonModel.ClsDistrictMaster GetDistrictId(int id);
        string CreateDistrict(ComonModel.ClsDistrictMaster DistrictMaster);
        string UpdateDistrict(ComonModel.ClsDistrictMaster DistrictMaster);
        string DeleteDistrict(int id);


        IEnumerable<ComonModel.ClsGenderInfo> GetAllGender();
        ComonModel.ClsGenderInfo GetGenderId(int id);
        string CreateGender(ComonModel.ClsGenderInfo GenderInfo);
        string UpdateGender(ComonModel.ClsGenderInfo GenderInfo);
        string DeleteGender(int id);


        IEnumerable<ComonModel.ClsInstituteInfo> GetAllInstitute();
        ComonModel.ClsInstituteInfo GetInstituteId(int id);
        string CreateInstitute(ComonModel.ClsInstituteInfo InstituteInfo);
        string UpdateInstitute(ComonModel.ClsInstituteInfo InstituteInfo);
        string DeleteInstitute(int id);


        IEnumerable<ComonModel.ClsMaritalStatusInfo> GetAllMaritalStatus();
        ComonModel.ClsMaritalStatusInfo GetMaritalStatusId(int id);
        string CreateMaritalStatus(ComonModel.ClsMaritalStatusInfo MaritalStatusInfo);
        string UpdateMaritalStatus(ComonModel.ClsMaritalStatusInfo MaritalStatusInfo);
        string DeleteMaritalStatus(int id);


        IEnumerable<ComonModel.ClsOccupationInfo> GetAllOccupation();
        ComonModel.ClsOccupationInfo GetOccupatioId(int id);
        string CreateOccupation(ComonModel.ClsOccupationInfo OccupationInfo);
        string UpdateOccupation(ComonModel.ClsOccupationInfo OccupationInfo);
        string DeleteOccupation(int id);


        IEnumerable<ComonModel.ClsPolicestationMaster> GetAllPolicestation();
        ComonModel.ClsPolicestationMaster GetPolicestationId(int id);
        string CreatePolicestation(ComonModel.ClsPolicestationMaster PolicestationMaster);
        string UpdatePolicestation(ComonModel.ClsPolicestationMaster PolicestationMaster);
        string DeletePolicestation(int id);


        IEnumerable<ComonModel.ClsPostofficeMaster> GetAllPostoffice();
        ComonModel.ClsPostofficeMaster GetPostofficeId(int id);
        string CreatePostoffice(ComonModel.ClsPostofficeMaster PostofficeMaster);
        string UpdatePostoffice(ComonModel.ClsPostofficeMaster PostofficeMaster);
        string DeletePostoffice(int id);


        IEnumerable<ComonModel.ClsReligionInfo> GetAllReligion();
        ComonModel.ClsReligionInfo GetReligionId(int id);
        string CreateReligion(ComonModel.ClsReligionInfo ReligionInfo);
        string UpdateReligion(ComonModel.ClsReligionInfo ReligionInfo);
        string DeleteReligion(int id);


        IEnumerable<ComonModel.ClsResultInfo> GetAllResult();
        ComonModel.ClsResultInfo GetResultId(int id);
        string CreateResult(ComonModel.ClsResultInfo ResultInfo);
        string UpdateResult(ComonModel.ClsResultInfo ResultInfo);
        string DeleteResult(int id);


        IEnumerable<ComonModel.ClsSalutationInfo> GetAllSalutation();
        ComonModel.ClsSalutationInfo GetSalutationId(int id);
        string CreateSalutation(ComonModel.ClsSalutationInfo SalutationInfo);
        string UpdateSalutation(ComonModel.ClsSalutationInfo SalutationInfo);
        string DeleteSalutation(int id);
    }
}
