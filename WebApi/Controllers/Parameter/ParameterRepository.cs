﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;
namespace WebApi.Controllers.Parameter
{
    public class ParameterRepository : IParameterRepository
    {

        #region Get All Drop Down Data
        public ComonModel.ClsAlldropdownData GetAllDropdownData()
        {

            ComonModel.ClsAlldropdownData AlldropdownData = new ComonModel.ClsAlldropdownData();

            using (var db = new EMPLOYEE_DBEntities())
            {

                AlldropdownData.BankinfoList = db.T_SYS_BANK_INFO.Select(s => new ComonModel.ClsBankInfo()
                {
                    BankId = s.BANK_ID,
                    BankName = s.BANK_NAME,
                    AddedBy = null,
                    Ts = null,
                    Active = true
                }).ToList();


                AlldropdownData.BloodGroupList = db.T_SYS_BLOOD_GROUP_INFO.Select(s => new ComonModel.ClsBloodGroupInfo()
                {
                    BloodGroupId = s.BLOOD_GROUP_ID,
                    GroupName = s.GROUP_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.BranchOfficeList = db.T_SYS_BRANCH_OFFICE_INFO.Select(s => new ComonModel.ClsBranchOfficeInfo()
                {
                    BranchId = s.BRANCH_ID,
                    Description = s.DESCRIPTION,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.CountryList = db.T_SYS_COUNTRY_MASTER.Select(s => new ComonModel.ClsCountryMaster()
                {
                    CountryId = s.COUNTRY_ID,
                    CountryName = s.COUNTRY_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS,
                    Nationality = s.NATIONALITY
                }).ToList();

                AlldropdownData.CourseList = db.T_SYS_COURSE_INFO.Select(s => new ComonModel.ClsCourseInfo()
                {
                    CourseId = s.COURSE_ID,
                    CourseName = s.COURSE_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.DegreeList = db.T_SYS_DEGREE_INFO.Select(s => new ComonModel.ClsDegreeInfo()
                {
                    DegreeId = s.DEGREE_ID,
                    DegreeName = s.DEGREE_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS,

                }).ToList();

                AlldropdownData.DepartmentList = db.T_SYS_DEPARTMET_INFO.Select(s => new ComonModel.ClsDepartmentInfo()
                {
                    DeptId = s.DEPT_ID,
                    Description = s.DESCRIPTION,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.DesignationList = db.T_SYS_DESIGNATION_MASTER.Select(s => new ComonModel.ClsDesignationMaster()
                {
                    DesignationId = s.DESIGNATION_ID,
                    Description = s.DESCRIPTION,
                    Ts = s.TS,
                    AddedBy = s.ADDEDBY,
                    Rate = s.RATE
                }).ToList();

                AlldropdownData.DistrictList = db.T_SYS_DISTRICT_MASTER.Select(s => new ComonModel.ClsDistrictMaster()
                {
                    DistrictId = s.DISTRICT_ID,
                    Description = s.DESCRIPTION,
                    Ts = s.TS,
                    AddedBy = s.ADDEDBY
                }).ToList();

                AlldropdownData.EmployeeTypeList = db.T_SYS_EMPLOYEE_TYPE_INFO.Select(s => new ComonModel.ClsEmployeeTypeInfo()
                {
                    EmpTypeId = s.EMP_TYPE_ID,
                    Description = s.DESCRIPTION,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.GenderList = db.T_SYS_GENDER_INFO.Select(s => new ComonModel.ClsGenderInfo()
                {
                    GenderId = s.GENDER_ID,
                    GenderName = s.GENDER_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.InstitueList = db.T_SYS_INSTITUTE_INFO.Select(s => new ComonModel.ClsInstituteInfo()
                {
                    InstitueId = s.INSTITUTE_ID,
                    InstituteName = s.INSTITUTE_NAME,
                    InstituteType = s.INSTITUTE_TYPE,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS,

                }).ToList();

                AlldropdownData.MaritalStatusList = db.T_SYS_MARITAL_STATUS_INFO.Select(s => new ComonModel.ClsMaritalStatusInfo()
                {
                    MaritalId = s.MARITAL_ID,
                    MaritalName = s.MARITAL_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.OccupationList = db.T_SYS_OCCUPATION_INO.Select(s => new ComonModel.ClsOccupationInfo()
                {
                    OccupationId = s.OCCUPATION_ID,
                    OccupationName = s.OCCUPATION_NAME,
                    Description = s.DESCRIPTION,
                    AddedBy = s.ADDEDBY
                }).ToList();

                AlldropdownData.ReligionList = db.T_SYS_RELIGION_INFO.Select(s => new ComonModel.ClsReligionInfo()
                {
                    ReligionId = s.RELIGION_ID,
                    ReligionName = s.RELIGION_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.ResultTypeList = db.T_SYS_RESULT_INFO.Select(s => new ComonModel.ClsResultInfo()
                {
                    ResultTypeId = s.RESULT_TYPE_ID,
                    ResultTypeName = s.RESULT_TYPE_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();

                AlldropdownData.SalutationList = db.T_SYS_SALUTATION_INFO.Select(s => new ComonModel.ClsSalutationInfo()
                {
                    salutationId = s.SALUTATION_ID,
                    SalutationName = s.SALUTATION_NAME,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();


            }
            return AlldropdownData;


        }

        public IEnumerable<ComonModel.ClsPolicestationMaster> GetPoliceStationByDistrictId(int id)
        {
            List<ComonModel.ClsPolicestationMaster> PoliceStationList = new List<ComonModel.ClsPolicestationMaster>();

            using (var db = new EMPLOYEE_DBEntities())
            {
                PoliceStationList = db.T_SYS_POLICESTATION_MASTER.Where(s=>s.DISTRICT_ID== id).Select(p =>  new ComonModel.ClsPolicestationMaster()
                {
                    PolicestationId = p.POLICESTATION_ID,
                    Description = p.DESCRIPTION                   
                }).ToList();
            }
            return PoliceStationList;
        }

        public IEnumerable<ComonModel.ClsPostofficeMaster> GetOfficeByDistrictId(int id)
        {
            List<ComonModel.ClsPostofficeMaster>PostOfficeist = new List<ComonModel.ClsPostofficeMaster>();

            using (var db = new EMPLOYEE_DBEntities())
            {
                PostOfficeist = db.T_SYS_POSTOFFICE_MASTER.Where(s=>s.DISTRICT_ID==id).Select(p => new ComonModel.ClsPostofficeMaster()
                {
                    PostofficeId = p.POSTOFFICE_ID,
                    Description = p.DESCRIPTION
                }).ToList();
            }
            return PostOfficeist;
        }
        #endregion

        #region Employee Type Setup
        public IEnumerable<ComonModel.ClsEmployeeTypeInfo> GetAllEmployeeType()
        {        
            IList<ComonModel.ClsEmployeeTypeInfo> AllEmployeeTypes = null;

            using (var db = new EMPLOYEE_DBEntities())
            {
                AllEmployeeTypes = db.T_SYS_EMPLOYEE_TYPE_INFO.Select(s => new ComonModel.ClsEmployeeTypeInfo()
                {
                    EmpTypeId = s.EMP_TYPE_ID,
                    Description = s.DESCRIPTION,
                    Active = s.ACTIVE==false?false:true,
                    AddedBy = s.ADDEDBY,
                    Ts=s.TS
                }).ToList();
            }

            return AllEmployeeTypes;
        }

        public ComonModel.ClsEmployeeTypeInfo GetEmployeeTypeId(int id)
        {

            ComonModel.ClsEmployeeTypeInfo EmployeeTypeInfo = new ComonModel.ClsEmployeeTypeInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldEmployeeType = db.T_SYS_EMPLOYEE_TYPE_INFO.Where(s => s.EMP_TYPE_ID == id).FirstOrDefault();

                if (OldEmployeeType != null)
                {
                    EmployeeTypeInfo.EmpTypeId = OldEmployeeType.EMP_TYPE_ID;
                    EmployeeTypeInfo.Description = OldEmployeeType.DESCRIPTION;
                    EmployeeTypeInfo.Active = OldEmployeeType.ACTIVE == false ? false : true; 
                    EmployeeTypeInfo.Ts = OldEmployeeType.TS;
                    EmployeeTypeInfo.AddedBy = OldEmployeeType.ADDEDBY;
                }

            }
            return EmployeeTypeInfo;
        }

        public string CreateEmployeeType(ComonModel.ClsEmployeeTypeInfo EmployeeTypeInfo)
        {            
            using (var db = new EMPLOYEE_DBEntities())
            {
                
                db.T_SYS_EMPLOYEE_TYPE_INFO.Add(new T_SYS_EMPLOYEE_TYPE_INFO()
                {
                    DESCRIPTION = EmployeeTypeInfo.Description,
                    TS = DateTime.Now,
                    ACTIVE = EmployeeTypeInfo.Active,
                    ADDEDBY=EmployeeTypeInfo.AddedBy
                });

                db.SaveChanges();
            }

            return "Saved Successfully";
        }

        public string UpdateEmployeeType(ComonModel.ClsEmployeeTypeInfo EmployeeTypeInfo)
        {

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldEmployeeType = db.T_SYS_EMPLOYEE_TYPE_INFO.Where(s => s.EMP_TYPE_ID == EmployeeTypeInfo.EmpTypeId).FirstOrDefault();

                if (OldEmployeeType != null)
                {
                    OldEmployeeType.EMP_TYPE_ID = EmployeeTypeInfo.EmpTypeId;
                    OldEmployeeType.DESCRIPTION = EmployeeTypeInfo.Description;
                    OldEmployeeType.ACTIVE = EmployeeTypeInfo.Active;
                    OldEmployeeType.TS = DateTime.Now;
                    OldEmployeeType.ADDEDBY = EmployeeTypeInfo.AddedBy;
                    db.SaveChanges();

                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }
        public string DeleteEmployeeType(int id)
        {

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldEmployeeType = db.T_SYS_EMPLOYEE_TYPE_INFO.Where(s => s.EMP_TYPE_ID == id).FirstOrDefault();

                if (OldEmployeeType != null)
                {
                    db.T_SYS_EMPLOYEE_TYPE_INFO.Remove(OldEmployeeType);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }
        #endregion
        #region CRUED Bank 

        public IEnumerable<ComonModel.ClsBankInfo> GetAllBank()
        {
            IList<ComonModel.ClsBankInfo> AllBank = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllBank = db.T_SYS_BANK_INFO.Select(s => new ComonModel.ClsBankInfo()
                {
                    BankId = s.BANK_ID,
                    BankName = s.BANK_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllBank;
        }

        public ComonModel.ClsBankInfo GetBankId(int id)
        {
            ComonModel.ClsBankInfo BankInfo = new ComonModel.ClsBankInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBank = db.T_SYS_BANK_INFO.Where(s => s.BANK_ID == id).FirstOrDefault();

                if (OldBank != null)
                {
                    BankInfo.BankId = OldBank.BANK_ID;
                    BankInfo.BankName = OldBank.BANK_NAME;
                    BankInfo.Active = OldBank.ACTIVE == false ? false : true;
                    BankInfo.Ts = OldBank.TS;
                    BankInfo.AddedBy = OldBank.ADDEDBY;

                }
            }
            return BankInfo;
        }

        public string CreateBank(ComonModel.ClsBankInfo BankInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_BANK_INFO.Add(new T_SYS_BANK_INFO()
                {
                    BANK_NAME = BankInfo.BankName,
                    TS = DateTime.Now,
                    ACTIVE = BankInfo.Active,
                    ADDEDBY = BankInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Save Successfully";
        }

        public string UpdateBank(ComonModel.ClsBankInfo BankInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBank = db.T_SYS_BANK_INFO.Where(s => s.BANK_ID == BankInfo.BankId).FirstOrDefault();

                if (OldBank != null)
                {
                    OldBank.BANK_ID = BankInfo.BankId;
                    OldBank.BANK_NAME = BankInfo.BankName;
                    OldBank.ACTIVE = BankInfo.Active;
                    OldBank.TS = DateTime.Now;
                    OldBank.ADDEDBY = BankInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Update Successfully";
        }

        public string DeleteBank(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBank = db.T_SYS_BANK_INFO.Where(s => s.BANK_ID == id).FirstOrDefault();

                if (OldBank != null)
                {
                    db.T_SYS_BANK_INFO.Remove(OldBank);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUED Blood Group

        public IEnumerable<ComonModel.ClsBloodGroupInfo> GetAllBloodGroup()
        {
            IList<ComonModel.ClsBloodGroupInfo> AllBloodGroup = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllBloodGroup = db.T_SYS_BLOOD_GROUP_INFO.Select(s => new ComonModel.ClsBloodGroupInfo()
                {
                    BloodGroupId = s.BLOOD_GROUP_ID,
                    GroupName = s.GROUP_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllBloodGroup;
        }

        public ComonModel.ClsBloodGroupInfo GetBloodGroupId(int id)
        {
            ComonModel.ClsBloodGroupInfo BloodGroupInfo = new ComonModel.ClsBloodGroupInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBloodGroup = db.T_SYS_BLOOD_GROUP_INFO.Where(s => s.BLOOD_GROUP_ID == id).FirstOrDefault();

                if (OldBloodGroup != null)
                {
                    BloodGroupInfo.BloodGroupId = OldBloodGroup.BLOOD_GROUP_ID;
                    BloodGroupInfo.GroupName = OldBloodGroup.GROUP_NAME;
                    BloodGroupInfo.Active = OldBloodGroup.ACTIVE == false ? false : true;
                    BloodGroupInfo.Ts = OldBloodGroup.TS;
                    BloodGroupInfo.AddedBy = OldBloodGroup.ADDEDBY;

                }
            }
            return BloodGroupInfo;
        }

        public string CreateBloodGroup(ComonModel.ClsBloodGroupInfo BloodGroupInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_BLOOD_GROUP_INFO.Add(new T_SYS_BLOOD_GROUP_INFO()
                {
                    
                    GROUP_NAME = BloodGroupInfo.GroupName,
                    TS = DateTime.Now,
                    ACTIVE = BloodGroupInfo.Active,
                    ADDEDBY = BloodGroupInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateBloodGroup(ComonModel.ClsBloodGroupInfo BloodGroupInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBloodGroup = db.T_SYS_BLOOD_GROUP_INFO.Where(s => s.BLOOD_GROUP_ID == BloodGroupInfo.BloodGroupId).FirstOrDefault();

                if (OldBloodGroup != null)
                {
                    OldBloodGroup.BLOOD_GROUP_ID = BloodGroupInfo.BloodGroupId;
                    OldBloodGroup.GROUP_NAME = BloodGroupInfo.GroupName;
                    OldBloodGroup.ACTIVE = BloodGroupInfo.Active;
                    OldBloodGroup.TS = DateTime.Now;
                    OldBloodGroup.ADDEDBY = BloodGroupInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteBloodGroup(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBloodGroup = db.T_SYS_BLOOD_GROUP_INFO.Where(s => s.BLOOD_GROUP_ID == id).FirstOrDefault();

                if (OldBloodGroup != null)
                {
                    db.T_SYS_BLOOD_GROUP_INFO.Remove(OldBloodGroup);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUED Branch Office

        public IEnumerable<ComonModel.ClsBranchOfficeInfo> GetAllBranchOffice()
        {
            IList<ComonModel.ClsBranchOfficeInfo> AllBranchOffice = null;

            using (var db = new EMPLOYEE_DBEntities())
            {
                AllBranchOffice = db.T_SYS_BRANCH_OFFICE_INFO.Select(s => new ComonModel.ClsBranchOfficeInfo()
                {
                    BranchId = s.BRANCH_ID,
                    Description = s.DESCRIPTION,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllBranchOffice;
        }

        public ComonModel.ClsBranchOfficeInfo GetBranchOfficeId(int id)
        {
            ComonModel.ClsBranchOfficeInfo BranchOfficeInfo = new ComonModel.ClsBranchOfficeInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBranchOffice = db.T_SYS_BRANCH_OFFICE_INFO.Where(s => s.BRANCH_ID == id).FirstOrDefault();
                if (OldBranchOffice != null)
                {
                    BranchOfficeInfo.BranchId = OldBranchOffice.BRANCH_ID;
                    BranchOfficeInfo.Description = OldBranchOffice.DESCRIPTION;
                    BranchOfficeInfo.Active = OldBranchOffice.ACTIVE == false ? false : true;
                    BranchOfficeInfo.Ts = OldBranchOffice.TS;
                    BranchOfficeInfo.AddedBy = OldBranchOffice.ADDEDBY;
                }

            }
            return BranchOfficeInfo;
        }

        public string CreateBranchOffice(ComonModel.ClsBranchOfficeInfo BranchOfficeInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_BRANCH_OFFICE_INFO.Add(new T_SYS_BRANCH_OFFICE_INFO()
                {

                    DESCRIPTION = BranchOfficeInfo.Description,
                    ACTIVE = BranchOfficeInfo.Active,
                    TS = DateTime.Now,
                    ADDEDBY = BranchOfficeInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateBranchOffice(ComonModel.ClsBranchOfficeInfo BranchOfficeInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBranchOffice = db.T_SYS_BRANCH_OFFICE_INFO.Where(s => s.BRANCH_ID == BranchOfficeInfo.BranchId).FirstOrDefault();
                if (OldBranchOffice != null)
                {
                    OldBranchOffice.BRANCH_ID = BranchOfficeInfo.BranchId;
                    OldBranchOffice.DESCRIPTION = BranchOfficeInfo.Description;
                    OldBranchOffice.ACTIVE = BranchOfficeInfo.Active;
                    OldBranchOffice.TS = DateTime.Now;
                    OldBranchOffice.ADDEDBY = BranchOfficeInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteBranchOffice(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldBranchOffice = db.T_SYS_BRANCH_OFFICE_INFO.Where(s => s.BRANCH_ID == id).FirstOrDefault();
                if (OldBranchOffice != null)
                {
                    db.T_SYS_BRANCH_OFFICE_INFO.Remove(OldBranchOffice);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }
        #endregion

        #region CRUD Country

        public IEnumerable<ComonModel.ClsCountryMaster> GetAllCountry()
        {
            IList<ComonModel.ClsCountryMaster> AllCountry = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllCountry = db.T_SYS_COUNTRY_MASTER.Select(s => new ComonModel.ClsCountryMaster()
                {
                    CountryId = s.COUNTRY_ID,
                    CountryName = s.COUNTRY_NAME,
                    Nationality = s.NATIONALITY,
                    Active = s.ACTIVE,
                    AddedBy = s.ADDEDBY,
                    EditedBy=s.EDITEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllCountry;
        }

        public ComonModel.ClsCountryMaster GetCountryId(int id)
        {
            ComonModel.ClsCountryMaster CountryMaster = new ComonModel.ClsCountryMaster();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldCountry = db.T_SYS_COUNTRY_MASTER.Where(s => s.COUNTRY_ID == id).FirstOrDefault();
                if (OldCountry != null)
                {
                    CountryMaster.CountryId = OldCountry.COUNTRY_ID;
                    CountryMaster.CountryName = OldCountry.COUNTRY_NAME;
                    CountryMaster.Nationality = OldCountry.NATIONALITY;
                    CountryMaster.Active = OldCountry.ACTIVE;
                    CountryMaster.Ts = OldCountry.TS;
                    CountryMaster.AddedBy = OldCountry.ADDEDBY;
                    CountryMaster.EditedBy = OldCountry.EDITEDBY;
                }
            }
            return CountryMaster;
        }

        public string CreateCountry(ComonModel.ClsCountryMaster CountryMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_COUNTRY_MASTER.Add(new T_SYS_COUNTRY_MASTER()
                {
                    COUNTRY_NAME = CountryMaster.CountryName,
                    NATIONALITY=CountryMaster.Nationality,
                    TS = DateTime.Now,
                    ACTIVE = CountryMaster.Active,
                    ADDEDBY = CountryMaster.AddedBy,
                    EDITEDBY=CountryMaster.EditedBy
                    
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateCountry(ComonModel.ClsCountryMaster CountryMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldCountry = db.T_SYS_COUNTRY_MASTER.Where(s => s.COUNTRY_ID == CountryMaster.CountryId).FirstOrDefault();
                if (OldCountry != null)
                {
                    OldCountry.COUNTRY_ID = CountryMaster.CountryId;
                    OldCountry.COUNTRY_NAME = CountryMaster.CountryName;
                    OldCountry.NATIONALITY = CountryMaster.Nationality;
                    OldCountry.ACTIVE = CountryMaster.Active;
                    OldCountry.ADDEDBY = CountryMaster.AddedBy;
                    OldCountry.EDITEDBY = CountryMaster.EditedBy;
                    OldCountry.TS = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteCountry(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldCountry = db.T_SYS_COUNTRY_MASTER.Where(s => s.COUNTRY_ID == id).FirstOrDefault();
                if (OldCountry != null)
                {
                    db.T_SYS_COUNTRY_MASTER.Remove(OldCountry);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Course

        public IEnumerable<ComonModel.ClsCourseInfo> GetAllCourse()
        {
            IList<ComonModel.ClsCourseInfo> AllCourse = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllCourse = db.T_SYS_COURSE_INFO.Select(s => new ComonModel.ClsCourseInfo()
                {
                    CourseId = s.COURSE_ID,
                    CourseName = s.COURSE_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllCourse;
        }

        public ComonModel.ClsCourseInfo GetCourseId(int id)
        {
            ComonModel.ClsCourseInfo CourseInfo = new ComonModel.ClsCourseInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldCourse = db.T_SYS_COURSE_INFO.Where(s => s.COURSE_ID == id).FirstOrDefault();

                if (OldCourse != null)
                {
                    CourseInfo.CourseId = OldCourse.COURSE_ID;
                    CourseInfo.CourseName = OldCourse.COURSE_NAME;
                    CourseInfo.Active = OldCourse.ACTIVE == false ? false : true;
                    CourseInfo.AddedBy = OldCourse.ADDEDBY;
                    CourseInfo.Ts = OldCourse.TS;
                }
            }
            return CourseInfo;
        }

        public string CreateCourse(ComonModel.ClsCourseInfo CourseInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_COURSE_INFO.Add(new T_SYS_COURSE_INFO()
                {
                    COURSE_NAME = CourseInfo.CourseName,
                    TS = DateTime.Now,
                    ACTIVE = CourseInfo.Active,
                    ADDEDBY = CourseInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateCourse(ComonModel.ClsCourseInfo CourseInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldCourse = db.T_SYS_COURSE_INFO.Where(s => s.COURSE_ID == CourseInfo.CourseId).FirstOrDefault();
                if (OldCourse != null)
                {
                    OldCourse.COURSE_ID = CourseInfo.CourseId;
                    OldCourse.COURSE_NAME = CourseInfo.CourseName;
                    OldCourse.ACTIVE = CourseInfo.Active;
                    OldCourse.TS = DateTime.Now;
                    OldCourse.ADDEDBY = CourseInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteCourse(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldCourse = db.T_SYS_COURSE_INFO.Where(s => s.COURSE_ID == id).FirstOrDefault();
                if (OldCourse != null)
                {
                    db.T_SYS_COURSE_INFO.Remove(OldCourse);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Delete Successfully";
        }


        #endregion

        #region CRUD Degree

        public IEnumerable<ComonModel.ClsDegreeInfo> GetAllDegree()
        {
            IList<ComonModel.ClsDegreeInfo> AllDegree = null;

            using (var db = new EMPLOYEE_DBEntities())
            {
                AllDegree = db.T_SYS_DEGREE_INFO.Select(s => new ComonModel.ClsDegreeInfo()
                {
                    DegreeId = s.DEGREE_ID,
                    DegreeName = s.DEGREE_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllDegree;
        }

        public ComonModel.ClsDegreeInfo GetDegreeId(int id)
        {
            ComonModel.ClsDegreeInfo DegreeInfo = new ComonModel.ClsDegreeInfo();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDegree = db.T_SYS_DEGREE_INFO.Where(s => s.DEGREE_ID == id).FirstOrDefault();

                if (OldDegree != null)
                {
                    DegreeInfo.DegreeId = OldDegree.DEGREE_ID;
                    DegreeInfo.DegreeName = OldDegree.DEGREE_NAME;
                    DegreeInfo.Active = OldDegree.ACTIVE == false ? false : true;
                    DegreeInfo.Ts = OldDegree.TS;
                    DegreeInfo.AddedBy = OldDegree.ADDEDBY;
                }
            }
            return DegreeInfo;
        }

        public string CreateDegree(ComonModel.ClsDegreeInfo DegreeInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_DEGREE_INFO.Add(new T_SYS_DEGREE_INFO()
                {
                    DEGREE_NAME = DegreeInfo.DegreeName,
                    TS = DateTime.Now,
                    ACTIVE = DegreeInfo.Active,
                    ADDEDBY = DegreeInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateDegree(ComonModel.ClsDegreeInfo DegreeInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDegree = db.T_SYS_DEGREE_INFO.Where(s => s.DEGREE_ID == DegreeInfo.DegreeId).FirstOrDefault();
                if (OldDegree != null)
                {
                    OldDegree.DEGREE_ID = DegreeInfo.DegreeId;
                    OldDegree.DEGREE_NAME = DegreeInfo.DegreeName;
                    OldDegree.ACTIVE = DegreeInfo.Active;
                    OldDegree.TS = DateTime.Now;
                    OldDegree.ADDEDBY = DegreeInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteDegree(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDegree = db.T_SYS_DEGREE_INFO.Where(s => s.DEGREE_ID == id).FirstOrDefault();
                if (OldDegree != null)
                {
                    db.T_SYS_DEGREE_INFO.Remove(OldDegree);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Department

        public IEnumerable<ComonModel.ClsDepartmentInfo> GetAllDepartment()
        {
            IList<ComonModel.ClsDepartmentInfo> AllDepartment = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllDepartment = db.T_SYS_DEPARTMET_INFO.Select(s => new ComonModel.ClsDepartmentInfo()
                {
                    DeptId = s.DEPT_ID,
                    Description = s.DESCRIPTION,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    EditedBy=s.EDITEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllDepartment;
        }

        public ComonModel.ClsDepartmentInfo GetDepartmentId(int id)
        {
            ComonModel.ClsDepartmentInfo DepartmentInfo = new ComonModel.ClsDepartmentInfo();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDepartment = db.T_SYS_DEPARTMET_INFO.Where(s => s.DEPT_ID == id).FirstOrDefault();
                if (OldDepartment != null)
                {
                    DepartmentInfo.DeptId = OldDepartment.DEPT_ID;
                    DepartmentInfo.Description = OldDepartment.DESCRIPTION;
                    DepartmentInfo.Active = OldDepartment.ACTIVE;
                    DepartmentInfo.Ts = OldDepartment.TS;
                    DepartmentInfo.AddedBy = OldDepartment.ADDEDBY;
                    DepartmentInfo.EditedBy = OldDepartment.EDITEDBY;
                }
            }
            return DepartmentInfo;
        }

        public string CreateDepartment(ComonModel.ClsDepartmentInfo DepartmentInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_DEPARTMET_INFO.Add(new T_SYS_DEPARTMET_INFO()
                {
                    DESCRIPTION = DepartmentInfo.Description,
                    TS = DateTime.Now,
                    ACTIVE = DepartmentInfo.Active,
                    ADDEDBY = DepartmentInfo.AddedBy,
                    EDITEDBY=DepartmentInfo.EditedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateDepartment(ComonModel.ClsDepartmentInfo DepartmentInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDepartment = db.T_SYS_DEPARTMET_INFO.Where(s => s.DEPT_ID == DepartmentInfo.DeptId).FirstOrDefault();
                if (OldDepartment != null)
                {
                    OldDepartment.DEPT_ID = DepartmentInfo.DeptId;
                    OldDepartment.DESCRIPTION = DepartmentInfo.Description;
                    OldDepartment.ACTIVE = DepartmentInfo.Active;
                    OldDepartment.TS = DateTime.Now;
                    OldDepartment.ADDEDBY = DepartmentInfo.AddedBy;
                    OldDepartment.EDITEDBY = DepartmentInfo.EditedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Update Successfully";
        }

        public string DeleteDepartment(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDepartment = db.T_SYS_DEPARTMET_INFO.Where(s => s.DEPT_ID == id).FirstOrDefault();
                if (OldDepartment != null)
                {
                    db.T_SYS_DEPARTMET_INFO.Remove(OldDepartment);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Designation

        public IEnumerable<ComonModel.ClsDesignationMaster> GetAllDesignation()
        {
            IList<ComonModel.ClsDesignationMaster> AllDesignation = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllDesignation = db.T_SYS_DESIGNATION_MASTER.Select(s => new ComonModel.ClsDesignationMaster()
                {
                    DesignationId = s.DESIGNATION_ID,
                    Description = s.DESCRIPTION,
                    Active = s.ACTIVE,
                    AddedBy = s.ADDEDBY,
                    EditedBy=s.EDITEDBY,
                    Rate=s.RATE,
                    Ts = s.TS
                }).ToList();
            }
            return AllDesignation;
        }

        public ComonModel.ClsDesignationMaster GetDesignationId(int id)
        {
            ComonModel.ClsDesignationMaster DesignationMaster = new ComonModel.ClsDesignationMaster();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDesignation = db.T_SYS_DESIGNATION_MASTER.Where(s => s.DESIGNATION_ID == id).FirstOrDefault();
                if (OldDesignation != null)
                {
                    DesignationMaster.DesignationId = OldDesignation.DESIGNATION_ID;
                    DesignationMaster.Description = OldDesignation.DESCRIPTION;
                    DesignationMaster.Active = OldDesignation.ACTIVE;
                    DesignationMaster.Ts = OldDesignation.TS;
                    DesignationMaster.AddedBy = OldDesignation.ADDEDBY;
                    DesignationMaster.EditedBy = OldDesignation.EDITEDBY;
                    DesignationMaster.Rate = OldDesignation.RATE;
                }
            }
            return DesignationMaster;
        }

        public string CreateDesignation(ComonModel.ClsDesignationMaster DesignationMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_DESIGNATION_MASTER.Add(new T_SYS_DESIGNATION_MASTER()
                {
                    DESCRIPTION = DesignationMaster.Description,
                    TS = DateTime.Now,
                    ACTIVE = DesignationMaster.Active,
                    ADDEDBY = DesignationMaster.AddedBy,
                    EDITEDBY=DesignationMaster.EditedBy,
                    RATE=DesignationMaster.Rate
                    
                });
                db.SaveChanges();
            }
            return "Save Successfully";
        }

        public string UpdateDesignation(ComonModel.ClsDesignationMaster DesignationMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDesignation = db.T_SYS_DESIGNATION_MASTER.Where(s => s.DESIGNATION_ID == DesignationMaster.DesignationId).FirstOrDefault();
                if (OldDesignation != null)
                {
                    OldDesignation.DESIGNATION_ID = DesignationMaster.DesignationId;
                    OldDesignation.DESCRIPTION = DesignationMaster.Description;
                    OldDesignation.ACTIVE = DesignationMaster.Active;
                    OldDesignation.TS = DateTime.Now;
                    OldDesignation.ADDEDBY = DesignationMaster.AddedBy;
                    OldDesignation.EDITEDBY = DesignationMaster.EditedBy;
                    OldDesignation.RATE = DesignationMaster.Rate;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteDesignation(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDesignation = db.T_SYS_DESIGNATION_MASTER.Where(s => s.DESIGNATION_ID == id).FirstOrDefault();
                if (OldDesignation != null)
                {
                    db.T_SYS_DESIGNATION_MASTER.Remove(OldDesignation);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD District

        public IEnumerable<ComonModel.ClsDistrictMaster> GetAllDistrict()
        {
            IList<ComonModel.ClsDistrictMaster> AllDistrict = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllDistrict = db.T_SYS_DISTRICT_MASTER.Select(s => new ComonModel.ClsDistrictMaster()
                {
                    DistrictId = s.DISTRICT_ID,
                    Description = s.DESCRIPTION,
                    Active = s.ACTIVE,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllDistrict;
        }

        public ComonModel.ClsDistrictMaster GetDistrictId(int id)
        {
            ComonModel.ClsDistrictMaster DistrictMaster = new ComonModel.ClsDistrictMaster();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDistrict = db.T_SYS_DISTRICT_MASTER.Where(s => s.DISTRICT_ID == id).FirstOrDefault();
                if (OldDistrict != null)
                {
                    DistrictMaster.DistrictId = OldDistrict.DISTRICT_ID;
                    DistrictMaster.Description = OldDistrict.DESCRIPTION;
                    DistrictMaster.Active = OldDistrict.ACTIVE;
                    DistrictMaster.Ts = OldDistrict.TS;
                    DistrictMaster.AddedBy = OldDistrict.ADDEDBY;
                }
            }
            return DistrictMaster;
        }

        public string CreateDistrict(ComonModel.ClsDistrictMaster DistrictMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_DISTRICT_MASTER.Add(new T_SYS_DISTRICT_MASTER()
                {
                    DESCRIPTION = DistrictMaster.Description,
                    TS = DateTime.Now,
                    ACTIVE = DistrictMaster.Active,
                    ADDEDBY = DistrictMaster.AddedBy
                });
                db.SaveChanges();
            }

            return "Saved Successfully";
        }

        public string UpdateDistrict(ComonModel.ClsDistrictMaster DistrictMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDistrict = db.T_SYS_DISTRICT_MASTER.Where(s => s.DISTRICT_ID == DistrictMaster.DistrictId).FirstOrDefault();
                if (OldDistrict != null)
                {
                    OldDistrict.DISTRICT_ID = DistrictMaster.DistrictId;
                    OldDistrict.DESCRIPTION = DistrictMaster.Description;
                    OldDistrict.ACTIVE = DistrictMaster.Active;
                    OldDistrict.TS = DateTime.Now;
                    OldDistrict.ADDEDBY = DistrictMaster.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteDistrict(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldDistrict = db.T_SYS_DISTRICT_MASTER.Where(s => s.DISTRICT_ID == id).FirstOrDefault();
                if (OldDistrict != null)
                {
                    db.T_SYS_DISTRICT_MASTER.Remove(OldDistrict);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Gender

        public IEnumerable<ComonModel.ClsGenderInfo> GetAllGender()
        {
            IList<ComonModel.ClsGenderInfo> AllGender = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllGender = db.T_SYS_GENDER_INFO.Select(s => new ComonModel.ClsGenderInfo()
                {
                    GenderId = s.GENDER_ID,
                    GenderName = s.GENDER_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllGender;
        }
        public ComonModel.ClsGenderInfo GetGenderId(int id)
        {
            ComonModel.ClsGenderInfo GenderInfo = new ComonModel.ClsGenderInfo();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldGender = db.T_SYS_GENDER_INFO.Where(s => s.GENDER_ID == id).FirstOrDefault();
                if (OldGender != null)
                {
                    GenderInfo.GenderId = OldGender.GENDER_ID;
                    GenderInfo.GenderName = OldGender.GENDER_NAME;
                    GenderInfo.Active = OldGender.ACTIVE == false ? false : true;
                    GenderInfo.Ts = OldGender.TS;
                    GenderInfo.AddedBy = OldGender.ADDEDBY;
                }
            }
            return GenderInfo;
        }

        public string CreateGender(ComonModel.ClsGenderInfo GenderInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_GENDER_INFO.Add(new T_SYS_GENDER_INFO()
                {
                    GENDER_NAME = GenderInfo.GenderName,
                    TS = DateTime.Now,
                    ACTIVE = GenderInfo.Active,
                    ADDEDBY = GenderInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateGender(ComonModel.ClsGenderInfo GenderInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldGender = db.T_SYS_GENDER_INFO.Where(s => s.GENDER_ID == GenderInfo.GenderId).FirstOrDefault();
                if (OldGender != null)
                {
                    OldGender.GENDER_ID = GenderInfo.GenderId;
                    OldGender.GENDER_NAME = GenderInfo.GenderName;
                    OldGender.ACTIVE = GenderInfo.Active;
                    OldGender.TS = DateTime.Now;
                    OldGender.ADDEDBY = GenderInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Update Successfully";
        }

        public string DeleteGender(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldGender = db.T_SYS_GENDER_INFO.Where(s => s.GENDER_ID == id).FirstOrDefault();
                if (OldGender != null)
                {
                    db.T_SYS_GENDER_INFO.Remove(OldGender);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Institute

        public IEnumerable<ComonModel.ClsInstituteInfo> GetAllInstitute()
        {
            IList<ComonModel.ClsInstituteInfo> AllInstitute = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllInstitute = db.T_SYS_INSTITUTE_INFO.Select(s => new ComonModel.ClsInstituteInfo()
                {
                    InstitueId = s.INSTITUTE_ID,
                    InstituteName = s.INSTITUTE_NAME,
                    InstituteType=s.INSTITUTE_TYPE,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllInstitute;
        }
        public ComonModel.ClsInstituteInfo GetInstituteId(int id)
        {
            ComonModel.ClsInstituteInfo InstituteInfo = new ComonModel.ClsInstituteInfo();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldInstitute = db.T_SYS_INSTITUTE_INFO.Where(s => s.INSTITUTE_ID == id).FirstOrDefault();
                if (OldInstitute != null)
                {
                    InstituteInfo.InstitueId = OldInstitute.INSTITUTE_ID;
                    InstituteInfo.InstituteName = OldInstitute.INSTITUTE_NAME;
                    InstituteInfo.InstituteType = OldInstitute.INSTITUTE_TYPE;
                    InstituteInfo.Active = OldInstitute.ACTIVE == false ? false : true;
                    InstituteInfo.Ts = OldInstitute.TS;
                    InstituteInfo.AddedBy = OldInstitute.ADDEDBY;
                }
            }
            return InstituteInfo;
        }
        public string CreateInstitute(ComonModel.ClsInstituteInfo InstituteInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_INSTITUTE_INFO.Add(new T_SYS_INSTITUTE_INFO()
                {
                    INSTITUTE_NAME = InstituteInfo.InstituteName,
                    INSTITUTE_TYPE=InstituteInfo.InstituteType,
                    TS = DateTime.Now,
                    ACTIVE = InstituteInfo.Active,
                    ADDEDBY = InstituteInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }
        public string UpdateInstitute(ComonModel.ClsInstituteInfo InstituteInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldInstitute = db.T_SYS_INSTITUTE_INFO.Where(s => s.INSTITUTE_ID == InstituteInfo.InstitueId).FirstOrDefault();
                if (OldInstitute != null)
                {
                    OldInstitute.INSTITUTE_ID = InstituteInfo.InstitueId;
                    OldInstitute.INSTITUTE_NAME = InstituteInfo.InstituteName;
                    OldInstitute.INSTITUTE_TYPE = InstituteInfo.InstituteType;
                    OldInstitute.ACTIVE = InstituteInfo.Active;
                    OldInstitute.TS = DateTime.Now;
                    OldInstitute.ADDEDBY = InstituteInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Update Successfully";
        }
        public string DeleteInstitute(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldInstitute = db.T_SYS_INSTITUTE_INFO.Where(s => s.INSTITUTE_ID == id).FirstOrDefault();
                if (OldInstitute != null)
                {
                    db.T_SYS_INSTITUTE_INFO.Remove(OldInstitute);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }


        #endregion

        #region CRUD Marital Status


        public IEnumerable<ComonModel.ClsMaritalStatusInfo> GetAllMaritalStatus()
        {
            IList<ComonModel.ClsMaritalStatusInfo> AllMaritalStatus = null;

            using (var db = new EMPLOYEE_DBEntities())
            {
                AllMaritalStatus = db.T_SYS_MARITAL_STATUS_INFO.Select(s => new ComonModel.ClsMaritalStatusInfo()
                {
                    MaritalId = s.MARITAL_ID,
                    MaritalName = s.MARITAL_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllMaritalStatus;
        }

        public ComonModel.ClsMaritalStatusInfo GetMaritalStatusId(int id)
        {
            ComonModel.ClsMaritalStatusInfo MaritalStatusInfo = new ComonModel.ClsMaritalStatusInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldMaritalStatus = db.T_SYS_MARITAL_STATUS_INFO.Where(s => s.MARITAL_ID == id).FirstOrDefault();
                if (OldMaritalStatus != null)
                {
                    MaritalStatusInfo.MaritalId = OldMaritalStatus.MARITAL_ID;
                    MaritalStatusInfo.MaritalName = OldMaritalStatus.MARITAL_NAME;
                    MaritalStatusInfo.Active = OldMaritalStatus.ACTIVE == false ? false : true;
                    MaritalStatusInfo.Ts = OldMaritalStatus.TS;
                    MaritalStatusInfo.AddedBy = OldMaritalStatus.ADDEDBY;
                }
            }
            return MaritalStatusInfo;
        }

        public string CreateMaritalStatus(ComonModel.ClsMaritalStatusInfo MaritalStatusInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_MARITAL_STATUS_INFO.Add(new T_SYS_MARITAL_STATUS_INFO()
                {
                    
                    MARITAL_NAME = MaritalStatusInfo.MaritalName,
                    ACTIVE = MaritalStatusInfo.Active,
                    TS=DateTime.Now,
                    ADDEDBY = MaritalStatusInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateMaritalStatus(ComonModel.ClsMaritalStatusInfo MaritalStatusInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldMaritalStatus = db.T_SYS_MARITAL_STATUS_INFO.Where(s => s.MARITAL_ID == MaritalStatusInfo.MaritalId).FirstOrDefault();
                if (OldMaritalStatus != null)
                {
                    OldMaritalStatus.MARITAL_ID = MaritalStatusInfo.MaritalId;
                    OldMaritalStatus.MARITAL_NAME = MaritalStatusInfo.MaritalName;
                    OldMaritalStatus.ACTIVE = MaritalStatusInfo.Active;
                    OldMaritalStatus.TS = DateTime.Now;
                    OldMaritalStatus.ADDEDBY = MaritalStatusInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteMaritalStatus(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldMaritalStatus = db.T_SYS_MARITAL_STATUS_INFO.Where(s => s.MARITAL_ID == id).FirstOrDefault();
                if (OldMaritalStatus != null)
                {
                    db.T_SYS_MARITAL_STATUS_INFO.Remove(OldMaritalStatus);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Occupation

        public IEnumerable<ComonModel.ClsOccupationInfo> GetAllOccupation()
        {
            IList<ComonModel.ClsOccupationInfo> AllOccupation = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllOccupation = db.T_SYS_OCCUPATION_INO.Select(s => new ComonModel.ClsOccupationInfo()
                {
                    OccupationId = s.OCCUPATION_ID,
                    OccupationName = s.OCCUPATION_NAME,
                    Description=s.DESCRIPTION,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY
                }).ToList();
            }
            return AllOccupation;
        }
        public ComonModel.ClsOccupationInfo GetOccupatioId(int id)
        {
            ComonModel.ClsOccupationInfo OccupationInfo = new ComonModel.ClsOccupationInfo();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldOccupation = db.T_SYS_OCCUPATION_INO.Where(s => s.OCCUPATION_ID == id).FirstOrDefault();
                if (OldOccupation != null)
                {
                    OccupationInfo.OccupationId = OldOccupation.OCCUPATION_ID;
                    OccupationInfo.OccupationName = OldOccupation.OCCUPATION_NAME;
                    OldOccupation.DESCRIPTION = OldOccupation.DESCRIPTION;
                    OccupationInfo.Active = OldOccupation.ACTIVE == false ? false : true;
                    OccupationInfo.AddedBy = OldOccupation.ADDEDBY;
                }
            }
            return OccupationInfo;
        }

        public string CreateOccupation(ComonModel.ClsOccupationInfo OccupationInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_OCCUPATION_INO.Add(new T_SYS_OCCUPATION_INO()
                {
                    OCCUPATION_NAME = OccupationInfo.OccupationName,
                    DESCRIPTION=OccupationInfo.Description,
                    ACTIVE = OccupationInfo.Active,
                    ADDEDBY = OccupationInfo.AddedBy
                });
                db.SaveChanges();

            }
            return "Saved Successfully";
        }

        public string UpdateOccupation(ComonModel.ClsOccupationInfo OccupationInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldOccupation = db.T_SYS_OCCUPATION_INO.Where(s => s.OCCUPATION_ID == OccupationInfo.OccupationId).FirstOrDefault();
                if (OldOccupation != null)
                {
                    OldOccupation.OCCUPATION_ID = OccupationInfo.OccupationId;
                    OldOccupation.OCCUPATION_NAME = OccupationInfo.OccupationName;
                    OldOccupation.DESCRIPTION = OccupationInfo.Description;
                    OldOccupation.ACTIVE = OccupationInfo.Active;
                    OldOccupation.ADDEDBY = OccupationInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Update Successfully";
        }

        public string DeleteOccupation(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldOccupation = db.T_SYS_OCCUPATION_INO.Where(s => s.OCCUPATION_ID == id).FirstOrDefault();
                if (OldOccupation != null)
                {
                    db.T_SYS_OCCUPATION_INO.Remove(OldOccupation);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Policestation

        public IEnumerable<ComonModel.ClsPolicestationMaster> GetAllPolicestation()
        {
            IList<ComonModel.ClsPolicestationMaster> AllPolicestation = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllPolicestation = db.T_SYS_POLICESTATION_MASTER.Select(s => new ComonModel.ClsPolicestationMaster()
                {
                    PolicestationId = s.POLICESTATION_ID,
                    Description = s.DESCRIPTION,
                    DistrictId=s.DISTRICT_ID,
                    Active = s.ACTIVE,
                    AddedBy = s.ADDEDBY,
                    EditedBy=s.EDITEDBY,
                    EditedTime=s.EDITED_TIME,
                    AddedTime = s.ADDED_TIME
                }).ToList();
            }
            return AllPolicestation;
        }

        public ComonModel.ClsPolicestationMaster GetPolicestationId(int id)
        {
            ComonModel.ClsPolicestationMaster PolicestationMaster = new ComonModel.ClsPolicestationMaster();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldPolicestation = db.T_SYS_POLICESTATION_MASTER.Where(s => s.POLICESTATION_ID == id).FirstOrDefault();
                if (OldPolicestation != null)
                {
                    PolicestationMaster.PolicestationId = OldPolicestation.POLICESTATION_ID;
                    PolicestationMaster.Description = OldPolicestation.DESCRIPTION;
                    PolicestationMaster.DistrictId = OldPolicestation.DISTRICT_ID;
                    PolicestationMaster.Active = OldPolicestation.ACTIVE;
                    PolicestationMaster.AddedTime = OldPolicestation.ADDED_TIME;
                    PolicestationMaster.EditedTime = OldPolicestation.EDITED_TIME;
                    PolicestationMaster.EditedBy = OldPolicestation.EDITEDBY;
                    PolicestationMaster.AddedBy = OldPolicestation.ADDEDBY;
                }
            }
            return PolicestationMaster;
        }

        public string CreatePolicestation(ComonModel.ClsPolicestationMaster PolicestationMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_POLICESTATION_MASTER.Add(new T_SYS_POLICESTATION_MASTER()
                {
                    DESCRIPTION = PolicestationMaster.Description,
                    DISTRICT_ID=PolicestationMaster.DistrictId,
                    ADDED_TIME = DateTime.Now,
                    //EDITED_TIME= EDITED_TIME,
                    ACTIVE = PolicestationMaster.Active,
                    ADDEDBY = PolicestationMaster.AddedBy,
                    EDITEDBY=PolicestationMaster.EditedBy
                });
                db.SaveChanges();
            }

            return "Saved Successfully";
        }

        public string UpdatePolicestation(ComonModel.ClsPolicestationMaster PolicestationMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldPolicestation = db.T_SYS_POLICESTATION_MASTER.Where(s => s.POLICESTATION_ID == PolicestationMaster.PolicestationId).FirstOrDefault();
                if (OldPolicestation != null)
                {
                    OldPolicestation.POLICESTATION_ID = PolicestationMaster.PolicestationId;
                    OldPolicestation.DESCRIPTION = PolicestationMaster.Description;
                    OldPolicestation.DISTRICT_ID = PolicestationMaster.DistrictId;
                    OldPolicestation.ACTIVE = PolicestationMaster.Active;
                    //OldPolicestation.ADDED_TIME = PolicestationMaster.AddedTime;
                    OldPolicestation.EDITED_TIME = DateTime.Now;
                    OldPolicestation.ADDEDBY = PolicestationMaster.AddedBy;
                    OldPolicestation.EDITEDBY = PolicestationMaster.EditedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeletePolicestation(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldPolicestation = db.T_SYS_POLICESTATION_MASTER.Where(s => s.POLICESTATION_ID == id).FirstOrDefault();
                if (OldPolicestation != null)
                {
                    db.T_SYS_POLICESTATION_MASTER.Remove(OldPolicestation);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Post Office

        public IEnumerable<ComonModel.ClsPostofficeMaster> GetAllPostoffice()
        {
            IList<ComonModel.ClsPostofficeMaster> AllPostoffice = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllPostoffice = db.T_SYS_POSTOFFICE_MASTER.Select(s => new ComonModel.ClsPostofficeMaster()
                {
                    PostofficeId = s.POSTOFFICE_ID,
                    Description = s.DESCRIPTION,
                    DistrictId=s.DISTRICT_ID,
                    Active = s.ACTIVE,
                    AddedBy = s.ADDEDBY,
                    EditedBy=s.EDITEDBY,
                    EditedTime=s.EDITED_TIME,
                    AddedTime = s.ADDED_TIME
                }).ToList();
            }
            return AllPostoffice;
        }

        public ComonModel.ClsPostofficeMaster GetPostofficeId(int id)
        {
            ComonModel.ClsPostofficeMaster PostofficeMaster = new ComonModel.ClsPostofficeMaster();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldPostoffice = db.T_SYS_POSTOFFICE_MASTER.Where(s => s.POSTOFFICE_ID == id).FirstOrDefault();
                if (OldPostoffice != null)
                {
                    PostofficeMaster.PostofficeId = OldPostoffice.POSTOFFICE_ID;
                    PostofficeMaster.Description = OldPostoffice.DESCRIPTION;
                    PostofficeMaster.DistrictId = OldPostoffice.DISTRICT_ID;
                    PostofficeMaster.Active = OldPostoffice.ACTIVE;
                    PostofficeMaster.AddedTime = OldPostoffice.ADDED_TIME;
                    PostofficeMaster.EditedTime = OldPostoffice.EDITED_TIME;
                    PostofficeMaster.AddedBy = OldPostoffice.ADDEDBY;
                    PostofficeMaster.EditedBy = OldPostoffice.EDITEDBY;
                }
            }
            return PostofficeMaster;
        }

        public string CreatePostoffice(ComonModel.ClsPostofficeMaster PostofficeMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_POSTOFFICE_MASTER.Add(new T_SYS_POSTOFFICE_MASTER()
                {
                    DESCRIPTION = PostofficeMaster.Description,
                    DISTRICT_ID=PostofficeMaster.DistrictId,
                    ADDED_TIME = DateTime.Now,
                    //EDITED_TIME=DateTime.Now,
                    ACTIVE = PostofficeMaster.Active,
                    EDITEDBY=PostofficeMaster.EditedBy,
                    ADDEDBY = PostofficeMaster.AddedBy
                });
                db.SaveChanges();
            }
            return "Save Successfully";
        }

        public string UpdatePostoffice(ComonModel.ClsPostofficeMaster PostofficeMaster)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldPostoffice = db.T_SYS_POSTOFFICE_MASTER.Where(s => s.POSTOFFICE_ID == PostofficeMaster.PostofficeId).FirstOrDefault();
                if (OldPostoffice != null)
                {
                    OldPostoffice.POSTOFFICE_ID = PostofficeMaster.PostofficeId;
                    OldPostoffice.DESCRIPTION = PostofficeMaster.Description;
                    OldPostoffice.DISTRICT_ID = PostofficeMaster.DistrictId;
                    OldPostoffice.ACTIVE = PostofficeMaster.Active;
                   // OldPostoffice.ADDED_TIME = PostofficeMaster.AddedTime;
                    OldPostoffice.EDITED_TIME = DateTime.Now;
                    OldPostoffice.ADDEDBY = PostofficeMaster.AddedBy;
                    OldPostoffice.EDITEDBY = PostofficeMaster.EditedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeletePostoffice(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldPostoffice = db.T_SYS_POSTOFFICE_MASTER.Where(s => s.POSTOFFICE_ID == id).FirstOrDefault();
                if (OldPostoffice != null)
                {
                    db.T_SYS_POSTOFFICE_MASTER.Remove(OldPostoffice);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Religion

        public IEnumerable<ComonModel.ClsReligionInfo> GetAllReligion()
        {
            IList<ComonModel.ClsReligionInfo> AllReligion = null;

            using (var db = new EMPLOYEE_DBEntities())
            {
                AllReligion = db.T_SYS_RELIGION_INFO.Select(s => new ComonModel.ClsReligionInfo()
                {
                    ReligionId = s.RELIGION_ID,
                    ReligionName = s.RELIGION_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllReligion;
        }

        public ComonModel.ClsReligionInfo GetReligionId(int id)
        {
            ComonModel.ClsReligionInfo ReligionInfo = new ComonModel.ClsReligionInfo();
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldReligion = db.T_SYS_RELIGION_INFO.Where(s => s.RELIGION_ID == id).FirstOrDefault();

                if (OldReligion != null)
                {
                    ReligionInfo.ReligionId = OldReligion.RELIGION_ID;
                    ReligionInfo.ReligionName = OldReligion.RELIGION_NAME;
                    ReligionInfo.Active = OldReligion.ACTIVE == false ? false : true;
                    ReligionInfo.Ts = OldReligion.TS;
                    ReligionInfo.AddedBy = OldReligion.ADDEDBY;
                }
            }
            return ReligionInfo;
        }

        public string CreateReligion(ComonModel.ClsReligionInfo ReligionInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_RELIGION_INFO.Add(new T_SYS_RELIGION_INFO()
                {
                    RELIGION_NAME = ReligionInfo.ReligionName,
                    TS = DateTime.Now,
                    ACTIVE = ReligionInfo.Active,
                    ADDEDBY = ReligionInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Saved Successfully";
        }

        public string UpdateReligion(ComonModel.ClsReligionInfo ReligionInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldReligion = db.T_SYS_RELIGION_INFO.Where(s => s.RELIGION_ID == ReligionInfo.ReligionId).FirstOrDefault();
                if (OldReligion != null)
                {
                    OldReligion.RELIGION_ID = ReligionInfo.ReligionId;
                    OldReligion.RELIGION_NAME = ReligionInfo.ReligionName;
                    OldReligion.ACTIVE = ReligionInfo.Active;
                    OldReligion.TS = DateTime.Now;
                    OldReligion.ADDEDBY = ReligionInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Updated Successfully";
        }

        public string DeleteReligion(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldReligion = db.T_SYS_RELIGION_INFO.Where(s => s.RELIGION_ID == id).FirstOrDefault();
                if (OldReligion != null)
                {
                    db.T_SYS_RELIGION_INFO.Remove(OldReligion);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Result

        public IEnumerable<ComonModel.ClsResultInfo> GetAllResult()
        {
            IList<ComonModel.ClsResultInfo> AllResult = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllResult = db.T_SYS_RESULT_INFO.Select(s => new ComonModel.ClsResultInfo()
                {
                    ResultTypeId = s.RESULT_TYPE_ID,
                    ResultTypeName = s.RESULT_TYPE_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllResult;
        }

        public ComonModel.ClsResultInfo GetResultId(int id)
        {
            ComonModel.ClsResultInfo ResultInfo = new ComonModel.ClsResultInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldResult = db.T_SYS_RESULT_INFO.Where(s => s.RESULT_TYPE_ID == id).FirstOrDefault();

                if (OldResult != null)
                {
                    ResultInfo.ResultTypeId = OldResult.RESULT_TYPE_ID;
                    ResultInfo.ResultTypeName = OldResult.RESULT_TYPE_NAME;
                    ResultInfo.Active = OldResult.ACTIVE == false ? false : true;
                    ResultInfo.Ts = OldResult.TS;
                    ResultInfo.AddedBy = OldResult.ADDEDBY;

                }
            }
            return ResultInfo;
        }

        public string CreateResult(ComonModel.ClsResultInfo ResultInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_RESULT_INFO.Add(new T_SYS_RESULT_INFO()
                {
                    RESULT_TYPE_NAME = ResultInfo.ResultTypeName,
                    TS = DateTime.Now,
                    ACTIVE = ResultInfo.Active,
                    ADDEDBY = ResultInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Save Successfully";
        }

        public string UpdateResult(ComonModel.ClsResultInfo ResultInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldResult = db.T_SYS_RESULT_INFO.Where(s => s.RESULT_TYPE_ID == ResultInfo.ResultTypeId).FirstOrDefault();

                if (OldResult != null)
                {
                    OldResult.RESULT_TYPE_ID = ResultInfo.ResultTypeId;
                    OldResult.RESULT_TYPE_NAME = ResultInfo.ResultTypeName;
                    OldResult.ACTIVE = ResultInfo.Active;
                    OldResult.TS = DateTime.Now;
                    OldResult.ADDEDBY = ResultInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Update Successfully";
        }

        public string DeleteResult(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldResult = db.T_SYS_RESULT_INFO.Where(s => s.RESULT_TYPE_ID == id).FirstOrDefault();

                if (OldResult != null)
                {
                    db.T_SYS_RESULT_INFO.Remove(OldResult);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion

        #region CRUD Salutation

        public IEnumerable<ComonModel.ClsSalutationInfo> GetAllSalutation()
        {
            IList<ComonModel.ClsSalutationInfo> AllSalutation = null;
            using (var db = new EMPLOYEE_DBEntities())
            {
                AllSalutation = db.T_SYS_SALUTATION_INFO.Select(s => new ComonModel.ClsSalutationInfo()
                {
                    salutationId = s.SALUTATION_ID,
                    SalutationName = s.SALUTATION_NAME,
                    Active = s.ACTIVE == false ? false : true,
                    AddedBy = s.ADDEDBY,
                    Ts = s.TS
                }).ToList();
            }
            return AllSalutation;
        }

        public ComonModel.ClsSalutationInfo GetSalutationId(int id)
        {
            ComonModel.ClsSalutationInfo SalutationInfo = new ComonModel.ClsSalutationInfo();

            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldSalutation = db.T_SYS_SALUTATION_INFO.Where(s => s.SALUTATION_ID == id).FirstOrDefault();

                if (OldSalutation != null)
                {
                    SalutationInfo.salutationId = OldSalutation.SALUTATION_ID;
                    SalutationInfo.SalutationName = OldSalutation.SALUTATION_NAME;
                    SalutationInfo.Active = OldSalutation.ACTIVE == false ? false : true;
                    SalutationInfo.Ts = OldSalutation.TS;
                    SalutationInfo.AddedBy = OldSalutation.ADDEDBY;

                }
            }
            return SalutationInfo;
        }

        public string CreateSalutation(ComonModel.ClsSalutationInfo SalutationInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                db.T_SYS_SALUTATION_INFO.Add(new T_SYS_SALUTATION_INFO()
                {
                    SALUTATION_NAME = SalutationInfo.SalutationName,
                    TS = DateTime.Now,
                    ACTIVE = SalutationInfo.Active,
                    ADDEDBY = SalutationInfo.AddedBy
                });
                db.SaveChanges();
            }
            return "Save Successfully";
        }

        public string UpdateSalutation(ComonModel.ClsSalutationInfo SalutationInfo)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldSalutation = db.T_SYS_SALUTATION_INFO.Where(s => s.SALUTATION_ID == SalutationInfo.salutationId).FirstOrDefault();

                if (OldSalutation != null)
                {
                    OldSalutation.SALUTATION_ID = SalutationInfo.salutationId;
                    OldSalutation.SALUTATION_NAME = SalutationInfo.SalutationName;
                    OldSalutation.ACTIVE = SalutationInfo.Active;
                    OldSalutation.TS = DateTime.Now;
                    OldSalutation.ADDEDBY = SalutationInfo.AddedBy;
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Update Successfully";
        }

        public string DeleteSalutation(int id)
        {
            using (var db = new EMPLOYEE_DBEntities())
            {
                var OldSalutation = db.T_SYS_SALUTATION_INFO.Where(s => s.SALUTATION_ID == id).FirstOrDefault();

                if (OldSalutation != null)
                {
                    db.T_SYS_SALUTATION_INFO.Remove(OldSalutation);
                    db.SaveChanges();
                }
                else
                {
                    return "Not Found";
                }
            }
            return "Deleted Successfully";
        }

        #endregion
    }

}