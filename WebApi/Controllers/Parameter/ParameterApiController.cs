﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers.Parameter
{
    public class ParameterApiController : ApiController
    {

        IParameterRepository iparameterRepository;
        public ParameterApiController()
        {
            iparameterRepository = new ParameterRepository();
        }
        #region Drop down data
        //[Authorize]
        public IHttpActionResult GetAllDropdownData()
        {
            return Ok(iparameterRepository.GetAllDropdownData());
        }

       // [Authorize]
        public IHttpActionResult GetPoliceStationByDistrictId(int id)
        {
            return Ok(iparameterRepository.GetPoliceStationByDistrictId(id));
        }

      //  [Authorize]
        public IHttpActionResult GetOfficeByDistrictId(int id)
        {
            return Ok(iparameterRepository.GetOfficeByDistrictId(id));
        }
        #endregion

        #region Employee Type Setup
        //[Authorize]
        public IHttpActionResult GetAllEmployeeType()
        {
            return Ok(iparameterRepository.GetAllEmployeeType());
        }

        // [Authorize]
        public IHttpActionResult GetEmployeeTypeId(int id)
        {
            return Ok(iparameterRepository.GetEmployeeTypeId(id));
        }

        //  [Authorize]
        public IHttpActionResult CreateEmployeeType(ComonModel.ClsEmployeeTypeInfo EmployeeTypeInfo)
        {
            return Ok(iparameterRepository.CreateEmployeeType(EmployeeTypeInfo));
        }

        public IHttpActionResult UpdateEmployeeType(ComonModel.ClsEmployeeTypeInfo EmployeeTypeInfo)
        {
            return Ok(iparameterRepository.UpdateEmployeeType(EmployeeTypeInfo));
        }

        public IHttpActionResult DeleteEmployeeType(int id)
        {
            return Ok(iparameterRepository.DeleteEmployeeType(id));
        }
        #endregion
        #region Bank Info Setup

        // [Authorize]
        public IHttpActionResult GetAllBank()
        {
            return Ok(iparameterRepository.GetAllBank());
        }

        // [Authorize]
        public IHttpActionResult GetBankId(int id)
        {
            return Ok(iparameterRepository.GetBankId(id));
        }

        // [Authorize]

        public IHttpActionResult CreateBank(ComonModel.ClsBankInfo BankInfo)
        {
            return Ok(iparameterRepository.CreateBank(BankInfo));
        }

        public IHttpActionResult UpdateBank(ComonModel.ClsBankInfo BankInfo)
        {
            return Ok(iparameterRepository.UpdateBank(BankInfo));
        }
        public IHttpActionResult DeleteBank(int id)
        {
            return Ok(iparameterRepository.DeleteBank(id));
        }
        #endregion

        #region  Blood Group
        //[Authorize]
        public IHttpActionResult GetAllBloodGroup()
        {
            return Ok(iparameterRepository.GetAllBloodGroup());
        }

        // [Authorize]
        public IHttpActionResult GetBloodGroupId(int id)
        {
            return Ok(iparameterRepository.GetBloodGroupId(id));
        }

        public IHttpActionResult CreateBloodGroup(ComonModel.ClsBloodGroupInfo BloodGroupInfo)
        {
            return Ok(iparameterRepository.CreateBloodGroup(BloodGroupInfo));
        }

        public IHttpActionResult UpdateBloodGroup(ComonModel.ClsBloodGroupInfo BloodGroupInfo)
        {
            return Ok(iparameterRepository.UpdateBloodGroup(BloodGroupInfo));
        }

        public IHttpActionResult DeleteBloodGroup(int id)
        {
            return Ok(iparameterRepository.DeleteBloodGroup(id));
        }

        #endregion

        #region  Branch Office

        // [Authorize]

        public IHttpActionResult GetAllBranchOffice()
        {
            return Ok(iparameterRepository.GetAllBranchOffice());
        }

        //  [Authorize]
        public IHttpActionResult GetBranchOfficeId(int id)
        {
            return Ok(iparameterRepository.GetBranchOfficeId(id));
        }
        public IHttpActionResult CreateBranchOffice(ComonModel.ClsBranchOfficeInfo BranchOfficeInfo)
        {
            return Ok(iparameterRepository.CreateBranchOffice(BranchOfficeInfo));
        }

        public IHttpActionResult UpdateBranchOffice(ComonModel.ClsBranchOfficeInfo BranchOfficeInfo)
        {
            return Ok(iparameterRepository.UpdateBranchOffice(BranchOfficeInfo));
        }

        public IHttpActionResult DeleteBranchOffice(int id)
        {
            return Ok(iparameterRepository.DeleteBranchOffice(id));
        }

        #endregion

        #region  Country


        // [Authorize]

        public IHttpActionResult GetAllCountry()
        {
            return Ok(iparameterRepository.GetAllCountry());
        }

        //  [Authorize]
        public IHttpActionResult GetCountryId(int id)
        {
            return Ok(iparameterRepository.GetCountryId(id));
        }
        public IHttpActionResult CreateCountry(ComonModel.ClsCountryMaster CountryMaster)
        {
            return Ok(iparameterRepository.CreateCountry(CountryMaster));
        }

        public IHttpActionResult UpdateCountry(ComonModel.ClsCountryMaster CountryMaster)
        {
            return Ok(iparameterRepository.UpdateCountry(CountryMaster));
        }

        public IHttpActionResult DeleteCountry(int id)
        {
            return Ok(iparameterRepository.DeleteCountry(id));
        }


        #endregion

        #region  Course

        public IHttpActionResult GetAllCourse()
        {
            return Ok(iparameterRepository.GetAllCourse());
        }
        public IHttpActionResult GetCourseId(int id)
        {
            return Ok(iparameterRepository.GetCountryId(id));
        }
        public IHttpActionResult CreateCourse(ComonModel.ClsCourseInfo CourseInfo)
        {
            return Ok(iparameterRepository.CreateCourse(CourseInfo));
        }
        public IHttpActionResult UpdateCourse(ComonModel.ClsCourseInfo CourseInfo)
        {
            return Ok(iparameterRepository.UpdateCourse(CourseInfo));
        }
        public IHttpActionResult DeleteCourse(int id)
        {
            return Ok(iparameterRepository.DeleteCourse(id));
        }


        #endregion

        #region  Degree

        public IHttpActionResult GetAllDegree()
        {
            return Ok(iparameterRepository.GetAllDegree());
        }

        public IHttpActionResult GetDegreeId(int id)
        {
            return Ok(iparameterRepository.GetDegreeId(id));
        }

        public IHttpActionResult CreateDegree(ComonModel.ClsDegreeInfo DegreeInfo)
        {
            return Ok(iparameterRepository.CreateDegree(DegreeInfo));
        }

        public IHttpActionResult UpdateDegree(ComonModel.ClsDegreeInfo DegreeInfo)
        {
            return Ok(iparameterRepository.UpdateDegree(DegreeInfo));
        }

        public IHttpActionResult DeleteDegree(int id)
        {
            return Ok(iparameterRepository.DeleteDegree(id));
        }

        #endregion

        #region  Department

        public IHttpActionResult GetAllDepartment()
        {
            return Ok(iparameterRepository.GetAllDepartment());
        }

        public IHttpActionResult GetDepartmentId(int id)
        {
            return Ok(iparameterRepository.GetDepartmentId(id));
        }

        public IHttpActionResult CreateDepartment(ComonModel.ClsDepartmentInfo DepartmentInfo)
        {
            return Ok(iparameterRepository.CreateDepartment(DepartmentInfo));
        }

        public IHttpActionResult UpdateDepartment(ComonModel.ClsDepartmentInfo DepartmentInfo)
        {
            return Ok(iparameterRepository.UpdateDepartment(DepartmentInfo));
        }

        public IHttpActionResult DeleteDepartment(int id)
        {
            return Ok(iparameterRepository.DeleteDepartment(id));
        }

        #endregion

        #region  Designation

        public IHttpActionResult GetAllDesignation()
        {
            return Ok(iparameterRepository.GetAllDesignation());
        }

        public IHttpActionResult GetDesignationId(int id)
        {
            return Ok(iparameterRepository.GetDesignationId(id));
        }

        public IHttpActionResult CreateDesignation(ComonModel.ClsDesignationMaster DesignationMaster)
        {
            return Ok(iparameterRepository.CreateDesignation(DesignationMaster));
        }

        public IHttpActionResult UpdateDesignation(ComonModel.ClsDesignationMaster DesignationMaster)
        {
            return Ok(iparameterRepository.UpdateDesignation(DesignationMaster));
        }

        public IHttpActionResult DeleteDesignation(int id)
        {
            return Ok(iparameterRepository.DeleteDesignation(id));
        }


        #endregion

        #region  District


        public IHttpActionResult GetAllDistrict()
        {
            return Ok(iparameterRepository.GetAllDistrict());
        }

        public IHttpActionResult GetDistrictId(int id)
        {
            return Ok(iparameterRepository.GetDistrictId(id));
        }

        public IHttpActionResult CreateDistrict(ComonModel.ClsDistrictMaster DistrictMaster)
        {
            return Ok(iparameterRepository.CreateDistrict(DistrictMaster));
        }

        public IHttpActionResult UpdateDistrict(ComonModel.ClsDistrictMaster DistrictMaster)
        {
            return Ok(iparameterRepository.UpdateDistrict(DistrictMaster));
        }

        public IHttpActionResult DeleteDistrict(int id)
        {
            return Ok(iparameterRepository.DeleteDistrict(id));
        }


        #endregion

        #region  Gender

        public IHttpActionResult GetAllGender()
        {
            return Ok(iparameterRepository.GetAllGender());
        }

        public IHttpActionResult GetGenderId(int id)
        {
            return Ok(iparameterRepository.GetGenderId(id));
        }

        public IHttpActionResult CreateGender(ComonModel.ClsGenderInfo GenderInfo)
        {
            return Ok(iparameterRepository.CreateGender(GenderInfo));
        }

        public IHttpActionResult UpdateGender(ComonModel.ClsGenderInfo GenderInfo)
        {
            return Ok(iparameterRepository.UpdateGender(GenderInfo));
        }

        public IHttpActionResult DeleteGender(int id)
        {
            return Ok(iparameterRepository.DeleteGender(id));
        }


        #endregion

        #region  Institute

        public IHttpActionResult GetAllInstitute()
        {
            return Ok(iparameterRepository.GetAllInstitute());
        }

        public IHttpActionResult GetInstituteId(int id)
        {
            return Ok(iparameterRepository.GetInstituteId(id));
        }

        public IHttpActionResult CreateInstitute(ComonModel.ClsInstituteInfo InstituteInfo)
        {
            return Ok(iparameterRepository.CreateInstitute(InstituteInfo));
        }

        public IHttpActionResult UpdateInstitute(ComonModel.ClsInstituteInfo InstituteInfo)
        {
            return Ok(iparameterRepository.UpdateInstitute(InstituteInfo));
        }

        public IHttpActionResult DeleteInstitute(int id)
        {
            return Ok(iparameterRepository.DeleteInstitute(id));
        }


        #endregion

        #region  Marital Status


        //[Authorize]

        public IHttpActionResult GetAllMaritalStatus()
        {
            return Ok(iparameterRepository.GetAllMaritalStatus());
        }

        //  [Authorize]
        public IHttpActionResult GetMaritalStatusId(int id)
        {
            return Ok(iparameterRepository.GetMaritalStatusId(id));
        }
        public IHttpActionResult CreateMaritalStatus(ComonModel.ClsMaritalStatusInfo MaritalStatusInfo)
        {
            return Ok(iparameterRepository.CreateMaritalStatus(MaritalStatusInfo));
        }

        public IHttpActionResult UpdateMaritalStatus(ComonModel.ClsMaritalStatusInfo MaritalStatusInfo)
        {
            return Ok(iparameterRepository.UpdateMaritalStatus(MaritalStatusInfo));
        }

        public IHttpActionResult DeleteMaritalStatus(int id)
        {
            return Ok(iparameterRepository.DeleteMaritalStatus(id));
        }


        #endregion

        #region  Occupation

        public IHttpActionResult GetAllOccupation()
        {
            return Ok(iparameterRepository.GetAllOccupation());
        }

        public IHttpActionResult GetOccupationId(int id)
        {
            return Ok(iparameterRepository.GetOccupatioId(id));
        }

        public IHttpActionResult CreateOccupation(ComonModel.ClsOccupationInfo OccupationInfo)
        {
            return Ok(iparameterRepository.CreateOccupation(OccupationInfo));
        }

        public IHttpActionResult UpdateOccupation(ComonModel.ClsOccupationInfo OccupationInfo)
        {
            return Ok(iparameterRepository.UpdateOccupation(OccupationInfo));
        }

        public IHttpActionResult DeleteOccupation(int id)
        {
            return Ok(iparameterRepository.DeleteOccupation(id));
        }

        #endregion

        #region  Police Station

        public IHttpActionResult GetAllPolicestation()
        {
            return Ok(iparameterRepository.GetAllPolicestation());
        }

        public IHttpActionResult GetPolicestationId(int id)
        {
            return Ok(iparameterRepository.GetPolicestationId(id));
        }

        public IHttpActionResult CreatePolicestation(ComonModel.ClsPolicestationMaster PolicestationMaster)
        {
            return Ok(iparameterRepository.CreatePolicestation(PolicestationMaster));
        }

        public IHttpActionResult UpdatePolicestation(ComonModel.ClsPolicestationMaster PolicestationMaster)
        {
            return Ok(iparameterRepository.UpdatePolicestation(PolicestationMaster));
        }

        public IHttpActionResult DeletePolicestation(int id)
        {
            return Ok(iparameterRepository.DeletePolicestation(id));
        }

        #endregion

        #region  Post Office

        public IHttpActionResult GetAllPostoffice()
        {
            return Ok(iparameterRepository.GetAllPostoffice());
        }

        public IHttpActionResult GetPostofficeId(int id)
        {
            return Ok(iparameterRepository.GetPostofficeId(id));
        }

        public IHttpActionResult CreatePostoffice(ComonModel.ClsPostofficeMaster PostofficeMaster)
        {
            return Ok(iparameterRepository.CreatePostoffice(PostofficeMaster));
        }

        public IHttpActionResult UpdatePostoffice(ComonModel.ClsPostofficeMaster PostofficeMaster)
        {
            return Ok(iparameterRepository.UpdatePostoffice(PostofficeMaster));
        }

        public IHttpActionResult DeletePostoffice(int id)
        {
            return Ok(iparameterRepository.DeletePostoffice(id));
        }


        #endregion

        #region  Religion

        public IHttpActionResult GetAllReligion()
        {
            return Ok(iparameterRepository.GetAllReligion());
        }

        public IHttpActionResult GetReligionId(int id)
        {
            return Ok(iparameterRepository.GetReligionId(id));
        }

        public IHttpActionResult CreateReligion(ComonModel.ClsReligionInfo ReligionInfo)
        {
            return Ok(iparameterRepository.CreateReligion(ReligionInfo));
        }

        public IHttpActionResult UpdateReligion(ComonModel.ClsReligionInfo ReligionInfo)
        {
            return Ok(iparameterRepository.UpdateReligion(ReligionInfo));
        }

        public IHttpActionResult DeleteReligion(int id)
        {
            return Ok(iparameterRepository.DeleteReligion(id));
        }

        #endregion

        #region  Result

        // [Authorize]
        public IHttpActionResult GetAllResult()
        {
            return Ok(iparameterRepository.GetAllResult());
        }

        // [Authorize]
        public IHttpActionResult GetResultId(int id)
        {
            return Ok(iparameterRepository.GetResultId(id));
        }

        // [Authorize]

        public IHttpActionResult CreateResult(ComonModel.ClsResultInfo ResultInfo)
        {
            return Ok(iparameterRepository.CreateResult(ResultInfo));
        }

        public IHttpActionResult UpdateResult(ComonModel.ClsResultInfo ResultInfo)
        {
            return Ok(iparameterRepository.UpdateResult(ResultInfo));
        }

        public IHttpActionResult DeleteResult(int id)
        {
            return Ok(iparameterRepository.DeleteResult(id));
        }

        #endregion

        #region  Salutation

        // [Authorize]
        public IHttpActionResult GetAllSalutation()
        {
            return Ok(iparameterRepository.GetAllSalutation());
        }

        // [Authorize]
        public IHttpActionResult GetSalutationId(int id)
        {
            return Ok(iparameterRepository.GetSalutationId(id));
        }

        // [Authorize]

        public IHttpActionResult CreateSalutation(ComonModel.ClsSalutationInfo SalutationInfo)
        {
            return Ok(iparameterRepository.CreateSalutation(SalutationInfo));
        }

        public IHttpActionResult UpdateSalutation(ComonModel.ClsSalutationInfo SalutationInfo)
        {
            return Ok(iparameterRepository.UpdateSalutation(SalutationInfo));
        }

        public IHttpActionResult DeleteSalutation(int id)
        {
            return Ok(iparameterRepository.DeleteSalutation(id));
        }


        #endregion

    }
}
