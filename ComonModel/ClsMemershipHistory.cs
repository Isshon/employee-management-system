﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsMemershipHistory
    {
        public int id { get; set; }
        //[Display(Name = "Member Id")]
       // [MaxLength(50, ErrorMessage = "Maximum  Hundred Character Acceptable")]
        public string MemberId { get; set; }
       // [Display(Name = "Society Name")]
      //  [MaxLength(50, ErrorMessage = "Maximum  Hundred Character Acceptable")]
        public string SocietyName { get; set; }
        
      //  [MaxLength(100, ErrorMessage = "Maximum  Hundred Character Acceptable")]
        public string Address { get; set; }
        public int EmpId { get; set; }
        public System.DateTime Ts { get; set; }
        public int AddedBy { get; set; }
      
    }
}
