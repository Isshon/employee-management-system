﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
  public class ClsDepartmentInfo
    {
        public int DeptId { get; set; }
        [MaxLength(100, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string Description { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
