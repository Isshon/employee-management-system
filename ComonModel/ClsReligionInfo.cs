﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
    public class ClsReligionInfo
    {
        public int ReligionId { get; set; }

        [MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string ReligionName { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
