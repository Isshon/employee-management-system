﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsAddressInfo
    {
        public int Id { get; set; }
        public string PermanentDistrictName { set; get; }
        public string PermanentPolliceStationName { set; get; }
        public string PermanentPostOfficeName { set; get; }
        public string PresentDistrictName { set; get; }
        public string PresentPolliceStationName { set; get; }
        public string PresentPostOfficeName { set; get; }
        public string PresentVillage { get; set; }
        public string PresentRoad { get; set; }
        public Nullable<int> PresentDistrictId { get; set; }
        public Nullable<int> PresentPoliceId { get; set; }
        public Nullable<int> PresentPostId { get; set; }
        public Nullable<bool> SameAsPresentAddr { get; set; }
        public string PermanentVillage { get; set; }
        public string PermanentRoad { get; set; }
        public Nullable<int> PermanentDistrictId { get; set; }
        public Nullable<int> PermanentPoliceId { get; set; }
        public Nullable<int> PermanentPostId { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<int> EmpId { get; set; }
        public string AddedBy { get; set; }
    }
}
