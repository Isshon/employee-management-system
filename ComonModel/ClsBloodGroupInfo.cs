﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
  public class ClsBloodGroupInfo
    {
        public int BloodGroupId { get; set; }
        [MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        [Display(Name ="Blood Group")]
        public string GroupName { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<bool> Active { get; set; }

    }
}
