﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
   public class ClsInstituteInfo
    {
        public int InstitueId { get; set; }
        [Display(Name = "Institution Name")]
        [MaxLength(50, ErrorMessage = "Maximum Lenth Fifty Character")]
        public string InstituteName { get; set; }
        public Nullable<int> InstituteType { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
