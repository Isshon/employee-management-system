﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
    public class ClsSiblingInfo
    {
        public int Id { get; set; }      
        public string FullName { get; set; }
        public Nullable<System.DateTime> Dob { get; set; }       
        public string MobileNo { get; set; }
        public Nullable<int> OccupationId { get; set; }       
        public int NoOfSiblings { get; set; }
        public Nullable<int> SiblingsGender { get; set; }        
        public int EmpId { get; set; }
        public System.DateTime Ts { get; set; }
        public int AddedBy { get; set; }
        public string LoginId { get; set; }
    }
}
