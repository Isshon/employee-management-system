﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsEducationInfo
    {
        public int Id { get; set; }
        public int DegreeId { get; set; }
        public string DegreeName { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        [Display(Name =" Institution Name")]
        [MaxLength(100, ErrorMessage ="Maximum Hundred Character acceptable")]
        public string InstitutionName { get; set; }
        public Nullable<int> BoardUniversityId { get; set; }
        public string BoardUniversityName { set; get; }
        [Display(Name ="Passing Year")]
        [Required(ErrorMessage ="Passing Year Required")]
        public int PassingYear { get; set; }
        public int ResultTypeId { get; set; }
        
        public Nullable<decimal> Cgpa { get; set; }
        public Nullable<decimal> OutOf { get; set; }
        public string ClassDivision { get; set; }
        public string Session { get; set; }
        public Nullable<int> EmpId { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
    }
}
