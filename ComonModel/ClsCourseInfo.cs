﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
  public class ClsCourseInfo
    {
        public int CourseId { get; set; }
        [MaxLength(100, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string CourseName { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
