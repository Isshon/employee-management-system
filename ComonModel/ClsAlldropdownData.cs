﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
   public class ClsAlldropdownData
    {
        // Load for all drop list data
        public List<ClsBankInfo> BankinfoList { get; set; }       
        public List<ClsBloodGroupInfo> BloodGroupList { get; set; }
        public List<ClsBranchOfficeInfo> BranchOfficeList { get; set; }
        public List<ClsCountryMaster> CountryList { get; set; }
        public List<ClsCourseInfo> CourseList { get; set; }
        public List<ClsDegreeInfo>DegreeList { get; set; }
        public List<ClsDepartmentInfo> DepartmentList { get; set; }      
        public List<ClsDesignationMaster> DesignationList { get; set; }
        public List<ClsDistrictMaster> DistrictList { get; set; }
        public List<ClsEmployeeTypeInfo> EmployeeTypeList { get; set; }
        public List<ClsGenderInfo> GenderList { get; set; }
        public List<ClsInstituteInfo> InstitueList { get; set; }
        public List<ClsMaritalStatusInfo> MaritalStatusList { get; set; }
        public List<ClsOccupationInfo> OccupationList { get; set; }
        // Police station office should by dynmically popolated
        // post office should by dynmically popolated
        public List<ClsReligionInfo> ReligionList { get; set; }
        public List<ClsResultInfo> ResultTypeList { get; set; }
        public List<ClsSalutationInfo> SalutationList { get; set; }
    }
}
