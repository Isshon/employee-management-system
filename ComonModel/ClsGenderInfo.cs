﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
   public class ClsGenderInfo
    {
        public int GenderId { get; set; }
        [Display(Name ="Gender")]
        [MaxLength(50, ErrorMessage ="Maximum Lenth Fifty Character")]
        public string GenderName { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
