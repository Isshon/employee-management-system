﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
    public class ClsPersonalInfo
    {

        public int EmpId { get; set; }

        [Display(Name = "Personal id")]
        [Required(ErrorMessage = "Personal Id required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string PersonalId { get; set; }

        [Display(Name = "Domain id")]
        [Required(ErrorMessage = "Domain id required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string DomenId { get; set; }

        [Display(Name = "Salutation id")]
        public Nullable<int> SalutationId { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string LastName { get; set; }

        [Display(Name = "Father's Name")]
        [Required(ErrorMessage = "Father's Name required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string FatherName { get; set; }

        [Display(Name = "Father's occupation id")]
        [Required(ErrorMessage = "Father's occupation Id required")]
        public int FatherOccupationId { get; set; }

        [Display(Name = "Mother's name")]
        [Required(ErrorMessage = "Mother's Name required")]
        public string MotherName { get; set; }

        [Display(Name = "Mother's occupation id")]
        [Required(ErrorMessage = "Mother's occupation Id required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public int MotherOccupationId { get; set; }

        [Display(Name = "Date Of Birth")]
        [Required(ErrorMessage = "Date Of Birth  required")]
        public System.DateTime DateOfBirth { get; set; }

        [Display(Name = "Religion Id")]
        [Required(ErrorMessage = "Religion Id required")]
        public int ReligionId { get; set; }

        [Display(Name = "Blood Group Id")]
        public Nullable<int> BloodGroupId { get; set; }

        [Display(Name = "Gender Id")]
        [Required(ErrorMessage = "Gender Id required")]
        public int GenderId { get; set; }

        [Display(Name = "Martial Status Id")]
        public Nullable<int> MartialStatusId { get; set; }

        [Display(Name = "Spouse's Name")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string SpouseName { get; set; }

        [Display(Name = "Spouse's occupation Id")]
        public Nullable<int> SpouseOccupationId { get; set; }

        [Display(Name = "Nationality Id")]
        public Nullable<int> NationalityId { get; set; }

        [Display(Name = "National Id")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string NationalId { get; set; }

        [Display(Name = "Birth Id")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string BirthId { get; set; }

        [Display(Name = "Passport No.")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string PassportNo { get; set; }

        [Display(Name = "Passport Expiry Date")]
        public Nullable<System.DateTime> PassportExpiry { get; set; }

        [Display(Name = "Designation Id")]
        public Nullable<int> DesignationId { get; set; }

        [Display(Name = "Department Id")]
        public Nullable<int> DepartmentId { get; set; }

        [Display(Name = "Branch Id")]
        public Nullable<int> BranchId { get; set; }

        [Display(Name = "Employee Type Id")]
        public Nullable<int> EmpTypeId { get; set; }

        [Display(Name = "Joining Date")]
        [Required(ErrorMessage = "Joining Date required")]
        public System.DateTime JoiningDate { get; set; }

        [Display(Name = "Confirmation Date")]
        public Nullable<System.DateTime> ConfirmationDate { get; set; }

        [Display(Name = "Bank Account")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string BankAccount { get; set; }

        [Display(Name = "Bank Id")]
        public Nullable<int> BankId { get; set; }

        [Display(Name = "Office Mobile")]
        [MaxLength(20, ErrorMessage = "Maximum Twenty Character Acceptable")]
        public string OfficeMobile { get; set; }

        [Display(Name = "Office Extension")]
        public Nullable<int> OfficeExt { get; set; }

        [Display(Name = "Personal Mobile")]
        [Required(ErrorMessage = "Personal Mobile required")]
        [MaxLength(20, ErrorMessage = "Maximum Twenty Character Acceptable")]
        public string PersonalMobile { get; set; }

        [Display(Name = "Office Email")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string OfficeEmail { get; set; }

        [Display(Name = "Personal Email")]
        [Required(ErrorMessage = "Personal Email required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string PersonalEmail { get; set; }

        [Display(Name = "Emergency person name")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string EmerPerson { get; set; }

        [Display(Name = "Emergency Person Mobile")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string EmerPersonMobile { get; set; }

        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string Relation { get; set; }

        [Display(Name = "Emergency Person Address")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string EmerPersonAddr { get; set; }
        //public int AddedBy { get; set; }
        //public Nullable<DateTime> Ts { get; set; }
        //public List<ClsChildInfo> ChildInfoList { get; set; }
        //public List<ClsSiblingInfo> SiblingsInfoList { get; set; }
    }

    public class Personal
    {
        public int EmpId { get; set; }
        public string PersonalId { get; set; }
        public string DomenId { get; set; }
        public Nullable<int> SalutationId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public int FatherOccupationId { get; set; }
        public string MotherName { get; set; }
        public int MotherOccupationId { get; set; }
        public System.DateTime DateOfBirth { get; set; }
        public int ReligionId { get; set; }
        public Nullable<int> BloodGroupId { get; set; }
        public int GenderId { get; set; }
        public Nullable<int> MartialStatusId { get; set; }
        public string SpouseName { get; set; }
        public Nullable<int> SpouseOccupationId { get; set; }
        public Nullable<int> NationalityId { get; set; }
        public string NationalId { get; set; }
        public string BirthId { get; set; }
        public string PassportNo { get; set; }
        public Nullable<System.DateTime> PassportExpiry { get; set; }
        public Nullable<int> DesignationId { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> EmpTypeId { get; set; }
        public System.DateTime JoiningDate { get; set; }
        public Nullable<System.DateTime> ConfirmationDate { get; set; }
        public string BankAccount { get; set; }
        public Nullable<int> BankId { get; set; }
        public string OfficeMobile { get; set; }
        public Nullable<int> OfficeExt { get; set; }
        public string PersonalMobile { get; set; }
        public string OfficeEmail { get; set; }
        public string PersonalEmail { get; set; }
        public string EmerPerson { get; set; }
        public string EmerPersonMobile { get; set; }
        public string Relation { get; set; }
        public string EmerPersonAddr { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public string LoginId { get; set; }
        public string PermanentDistrictName { set; get; }
        public string PermanentPolliceStationName { set; get; }
        public string PermanentPostOfficeName { set; get; }
        public string PresentDistrictName { set; get; }
        public string PresentPolliceStationName { set; get; }
        public string PresentPostOfficeName { set; get; }
        public string PresentVillage { get; set; }
        public string PresentRoad { get; set; }
        public Nullable<int> PresentDistrictId { get; set; }
        public Nullable<int> PresentPoliceId { get; set; }
        public Nullable<int> PresentPostId { get; set; }
        public Nullable<bool> SameAsPresentAddr { get; set; }
        public string PermanentVillage { get; set; }
        public string PermanentRoad { get; set; }
        public Nullable<int> PermanentDistrictId { get; set; }
        public Nullable<int> PermanentPoliceId { get; set; }
        public Nullable<int> PermanentPostId { get; set; }
        public int ExtraId { get; set; }
        public string ExtraActivities { get; set; }
        public string Publication { get; set; }
        public byte[] Signature { get; set; }
        public byte[] Photo { get; set; }
        public List<ClsChildInfo> ChildInfoList { get; set; }
        public List<ClsSiblingInfo> SiblingsInfoList { get; set; }
        public List<ClsEducationInfo> EducationInfoList { get; set; }
        public List<ClsTrainingInfo> TrainingInfoList { get; set; }
        public List<ClsCertificationInfo> CertificationInfoList { get; set; }
        public List<ClsEmploymentInfo> EmploymentInfoList { get; set; }
        public List<ClsProjectExperienceInfo> ProjectExperienceInfoList { get; set; }
        public List<ClsMemershipHistory> MembershipInfoList { get; set; }

    }
}
