﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsChildInfo
    {
        public int ChildId { get; set; }      
        public string FullName { get; set; }
        public Nullable<System.DateTime> Dob { get; set; }        
        public string MobileNo { get; set; }      
        public string Occupation { get; set; }      
        public int NoOfChild { get; set; }
        public Nullable<int> ChildGender { get; set; }
        public Nullable<int> EmpId { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public string LoginId { get; set; }
    }
}
