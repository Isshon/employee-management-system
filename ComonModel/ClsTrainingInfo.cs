﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsTrainingInfo
    {
        public int Id { get; set; }

        //[Display(Name ="Training type")]
        //[Required(ErrorMessage ="Tarining type required")]
        public int TrainingType { get; set; }

        //[Display(Name = "Training title")]
        //[Required(ErrorMessage = "Tarining title required")]
        //[MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string TrainingTitle { get; set; }

        //[Display(Name = "Training details")]
        //[Required(ErrorMessage = "Tarining details required")]
        //[MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string TrainingDetails { get; set; }

        //[Display(Name = "Institution Name")]
        //[Required(ErrorMessage = "Institution Name required")]
        //[MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string InstitutionName { get; set; }

        //[Display(Name = "Country Id")]
        public Nullable<int> CountryId { get; set; }
        public string CountryName { set; get; }

        //[Display(Name = "Training Start Date")]
        public Nullable<System.DateTime> TrainingStartDate { get; set; }

        //[Display(Name = "Training End Date")]
        public Nullable<System.DateTime> TrainingEndDate { get; set; }

        //[Required(ErrorMessage = "Duration required")]
        //[MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string Duration { get; set; }

        public string Location { get; set; }
        public Nullable<bool> IsLegalBond { get; set; }
        public string BondFor { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }

        //[Display(Name = "Employee Id")]
        //[Required(ErrorMessage = "Employee Id required")]
        public int EmpId { get; set; }

        //[Required(ErrorMessage = "Time required")]
        public System.DateTime Ts { get; set; }

        //[Display(Name = "Added By")]
        //[Required(ErrorMessage = "Added By required")]
        public int AddedBy { get; set; }

        
    }
}
