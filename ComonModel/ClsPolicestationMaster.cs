﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
    public class ClsPolicestationMaster
    {
        public int PolicestationId  { get; set; }

        [Required(ErrorMessage = "Description required")]
        [MaxLength(100, ErrorMessage = "Maximum Hundred character Acceptable")]
        public string Description { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<int> EditedBy { get; set; }
        public Nullable<int> Active { get; set; }
        public Nullable<System.DateTime> AddedTime { get; set; }
        public Nullable<System.DateTime> EditedTime { get; set; }
    }
}
