﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
  public class ClsDistrictMaster
    {
        public int DistrictId { get; set; }
        [Required(ErrorMessage = "Description must be put")]
        [MaxLength(100, ErrorMessage = "Maximum Hundred Charachter Acceptable")]
        public string Description { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<int> EditedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<int> Active { get; set; }
    }
}
