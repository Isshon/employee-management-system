﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
    public class ClsProjectExperienceInfo
    {
        public int Id { get; set; }

        //[Required(ErrorMessage = "Project Name required")]
        //[MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string ProjectName { get; set; }

        //[Required(ErrorMessage = "Company Name required")]
        //[MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string CompanyName { get; set; }

        //[Required(ErrorMessage = "Customer Name required")]
        //[MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string CustomerName { get; set; }

        //[Required(ErrorMessage = "Roles responsibility required")]
        //[MaxLength(200, ErrorMessage = "Maximum Two hundred Charachter Acceptable")]
        public string RolesResponsibility { get; set; }

        //[Required(ErrorMessage = "Start Date required")]
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }

        //[Required(ErrorMessage = "Technology Used required")]
        //[MaxLength(100, ErrorMessage = "Maximum hundred Charachter Acceptable")]
        public string TechnologyUsed { get; set; }

        //[Required(ErrorMessage = "Employee Id required")]
        public int EmpId { get; set; }

        //[Required(ErrorMessage = "Timestamp required")]
        public System.DateTime ts { get; set; }

        //[Required(ErrorMessage = "Added By required")]
        public int addedBy { get; set; }
    }
}
