﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsCertificationInfo
    {
        public int Id { get; set; }
        //[Display(Name ="Certificate Name")]
       // [Required(ErrorMessage ="Certificate Name Required")]
       // [MaxLength(100,ErrorMessage ="Maximum Hundred Charachter Acceptable")]
        public string CertName { get; set; }
       // [Display(Name = "Exam Details")]
       // [Required(ErrorMessage = "Exam Details Required")]
      //  [MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string ExamDetails { get; set; }
       // [Display(Name = "Certificate Authority")]
       // [Required(ErrorMessage = "Certificate Authority Required")]
       // [MaxLength(100, ErrorMessage = "Maximum Hundred Charachter Acceptable")]
        public string CertAuthority { get; set; }
        public Nullable<int> CertType { get; set; }

        public string Location { get; set; }
        //[MaxLength(100, ErrorMessage = "Maximum Hundred Charachter Acceptable")]
        public System.DateTime ObtainedDate { get; set; }
       // [Required(ErrorMessage ="Obtained Date Required ")]
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public Nullable<int> EmpId { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
    }
}
