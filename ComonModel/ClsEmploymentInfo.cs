﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsEmploymentInfo
    {
        public int Id { get; set; }
        [Display(Name ="Company Name")]
        [Required(ErrorMessage ="Company Name Required")]
        [MaxLength(50, ErrorMessage ="Maximum Fifty Character Acceptable")]
        public string CompanyName { get; set; }
        [Display(Name = "Company Location")]
        [Required(ErrorMessage = "Company Location Required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string CompanyLocation { get; set; }
        [Required(ErrorMessage = "Designation Required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string Designation { get; set; }
        [Display(Name = "Department Name")]
        [Required(ErrorMessage = "Department Name Required")]
        [MaxLength(50, ErrorMessage = "Maximum Fifty Character Acceptable")]
        public string DepartmentName { get; set; }
        [Display(Name = "Start Date")]
        [Required(ErrorMessage = "Start Date Required")]
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        [Display(Name = "Roles And Responsibility")]
        [Required(ErrorMessage = "Roles And Responsibility Required")]
        [MaxLength(200, ErrorMessage = "Maximum Two Hundred Character Acceptable")]
        public string RolesResponsibility { get; set; }
        [Display(Name = "Aria Of Experience")]
        [MaxLength(200, ErrorMessage = "Maximum Two Hundred Character Acceptable")]
        public string AreaOfExperience { get; set; }
        public int EmpId { get; set; }
        public System.DateTime Ts { get; set; }
        public int AddedBy { get; set; }
    }
}
