﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
    public class ClsExitInterview
    {
        public int Id { get; set; }
        //[Display(Name ="Resignation Date")]
        //[Required(ErrorMessage ="Resignation Date required")]
        public System.DateTime ResignationDate { get; set; }
        //[Display(Name = "Release Date")]
        //[Required(ErrorMessage = "Release Date required")]
        public System.DateTime ReleaseDate { get; set; }
       
        public Nullable<bool> RequestForExp { get; set; }
        public Nullable<bool> RequestForRelease { get; set; }
       // [MaxLength(260, ErrorMessage ="Maximum 260 Character Acceptable")]
        public string QuestionNo1 { get; set; }
     //   [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public Nullable<bool> QuestionNo2 { get; set; }
      //  [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo21 { get; set; }
       // [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo22 { get; set; }
      //  [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo23 { get; set; }
      //  [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo24 { get; set; }
       // [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo3 { get; set; }
      //  [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo4 { get; set; }
      //  [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo5 { get; set; }
       // [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo6 { get; set; }
       // [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo7 { get; set; }
        [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo8 { get; set; }
      //  [MaxLength(260, ErrorMessage = "Maximum 260 Character Acceptable")]
        public string QuestionNo9 { get; set; }
        public Nullable<int> EmpId { get; set; }
        public System.DateTime Ts { get; set; }
        public int AddedBy { get; set; }
    }
}
