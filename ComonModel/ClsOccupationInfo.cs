﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
  public class ClsOccupationInfo
    {
        [Required(ErrorMessage ="OccupationId required")]
        public int OccupationId { get; set; }
        [MaxLength(50, ErrorMessage = "Maximum Fifty character Acceptable")]
        public string OccupationName { get; set; }
        [MaxLength(50, ErrorMessage = "Maximum Fifty character Acceptable")]
        public string Description { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<bool> Active { get; set; }
        
    }
}
