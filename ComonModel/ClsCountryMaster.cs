﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
   public class ClsCountryMaster
    {
        public int CountryId { get; set; }
        [Display(Name = "Country Name")]
        [Required(ErrorMessage = "Country Name Required")]
        [MaxLength(100, ErrorMessage = "Maximum Hundred Charachter Acceptable")]
        public string CountryName { get; set; }
        [MaxLength(100, ErrorMessage = "Maximum Hundred Charachter Acceptable")]
        public string Nationality { get; set; }
        public Nullable<int> AddedBy { get; set; }
        public Nullable<int> EditedBy { get; set; }
        public Nullable<System.DateTime> Ts { get; set; }
        public Nullable<int> Active { get; set; }
    }
}
