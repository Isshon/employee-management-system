﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
    public class ClsPhotoAndOthers
    {
        public int ExtraId { get; set; }

       // [MaxLength(500, ErrorMessage = "Maximum Five hundred character Acceptable")]
        public string ExtraActivities { get; set; }

      //  [MaxLength(500, ErrorMessage = "Maximum Five hundred character Acceptable")]
        public string Publication { get; set; }
        public byte[] Signature { get; set; }
        public byte[] Photo { get; set; }

      //  [Required(ErrorMessage = "Employee Id required")]
        public int EmpId { get; set; }

       // [Required(ErrorMessage = "TimeStamp required")]
        public System.DateTime Ts { get; set; }

       // [Required(ErrorMessage = "Added By required")]
        public int AddedBy { get; set; }
        public List<ClsMemershipHistory> MemberShipHistoryList { set; get; }
    }
}
