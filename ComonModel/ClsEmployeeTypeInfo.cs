﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComonModel
{
  public class ClsEmployeeTypeInfo
    {
        public int EmpTypeId { get; set; }
        [MaxLength(50, ErrorMessage ="Maximum Fifty Character Acceptable")]
        [Display(Name = "Employee Type")]
        [Required]
        public string Description { get; set; }        
        public Nullable<int> AddedBy { get; set; }            
        public Nullable<System.DateTime> Ts { get; set; }
        public bool Active { get; set; } = true;
    }
}
