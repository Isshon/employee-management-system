﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ComonModel
{
    public class ClsProducts
    {
        public int Id { get; set; }

        [MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string Name { get; set; }

        [MaxLength(50, ErrorMessage = "Maximum Fifty Charachter Acceptable")]
        public string Category { get; set; }
        public Nullable<decimal> price { get; set; } // Should be capital letter 

        public List<ClsBankInfo> listBankinfo { get; set; }
        public List<ClsCountryMaster> listCountry { get; set; }
    }
}
