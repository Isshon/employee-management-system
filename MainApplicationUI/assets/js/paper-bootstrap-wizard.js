/*!

 =========================================================
 * Paper Bootstrap Wizard - v1.0.2
 =========================================================
 
 * Product Page: https://www.creative-tim.com/product/paper-bootstrap-wizard
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/paper-bootstrap-wizard/blob/master/LICENSE.md)
 
 =========================================================
 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 */

// Paper Bootstrap Wizard Functions

searchVisible = 0;
transparent = true;

$(document).ready(function () {
    
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    // Code for the Validator
    var $validator = $('.wizard-card form').validate({
        rules: {

            //.....................................sobiul start here...

            txtCgpa: {
                required: true
            },
            txtOutof: {
                required: true
            },
            //txtActualPassingYear: {
            //    required: true
            //},
            txtEmpId: {
                required: true
            },
            txtFirstName: {
                required: true
            },
            txtLastName: {
                required: true
            },
            txtFatherName: {
                required: true
            },

            txtMotherName: {
                required: true
            },
            txtJoinDate: {
                required: true
            },
            txtDateOfBirth: {
                required: true
            },           
            txtPersonalMobileNo: {
                required: true
            },          
            txtOfficeEmail: {
                required: true
            },
            txtPersonalEmail: {
                required: true
            },
            txtNationalIdNo: {
                required: true
            },
            txtVillageOrHouse: {
                required: true
            },
            txtRoad: {
                required: true
            },
            
            txtDateOfBirth: {
                required: true
            },
            
            txtDateOfBirth: {
                required: true
            },
            txtTrainingTitle: {
                required: true
            },
            txtTrainingDetails: {
                required: true
            },           
            txtDuration: {
                required: true
            },
            txtBondFor: {
                required: true
            },
            txtEffectiveDate: {
                required: true
            },
            txtEndDateOfBond: {
                required: true
            },
            txtCertificationName: {
                required: true
            },
            txtExamDetails: {
                required: true
            },
            txtCertificationAuthority: {
                required: true
            },
            txtObtainedDate: {
                required: true
            },
            txtExpiryDate: {
                required: true
            },
            txtProjectName: {
                required: true
            },
            txtCompanyName: {
                required: true
            },
            txtCustomerName: {
                required: true
            },
            txtRolesAndResponsibility: {
                required: true
            },
            txtStartDate: {
                required: true
            },
            txtEndDate: {
                required: true
            },
            txtTechnologyUsed: {
                required: true
            },
            txtResignationDate: {
                required: true
            },
            txtReleaseDate: {
                required: true
            },
            txtVillageOrHousePermanent: {
                required: true
            },
            txtRaodPermanent: {
                required: true
            },
            txtDateOfBirthForSiblings: {
                required: true
            },
            txtDateOfBirthForChild: {
                required: true
            },

            txtCompanyLocation: {
                required: true
            },
            txtDepartmentName: {
                required: true
            },
            txtDesignation: {
                required: true
            },

        },
    });

    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function (tab, navigation, index) {
            var $valid = $('.wizard-card form').valid();
            //var $valid = true;
            if (!$valid) {
                $validator.focusInvalid();
                return false;
            }
            else {
                 alert(index);
                if (index == 1) {
                    //Save Personal Info 
                    var Id = SavePersonalInfo();
                    alert(Id);
                    return true;
                }
                else if (index == 2) {
                    //Save Address Info 
                    alert("Address Info");
                    return true;
                }
                else if (index == 3) {
                    //Save Education Info 
                    var Id = EducationInfo();
                    //alert("Education Info");
                    return true;
                }
                else if (index == 4) {
                    //Save Training Info 
                    var Id = TrainingInfo();
                    alert("Training Info");
                    return true;
                }
                else if (index == 5) {
                    //Save Certication Info 
                    alert("Certication Info");
                    return true;
                }
                else if (index == 6) {
                    //Save Employment Info 
                    var Id = EmploymentInfo();
                    alert("Employment Info");
                    return true;
                }
                else if (index == 7) {
                    //Save Project Experience Info 
                  var Id= ProjectExperienceInfo();
                    alert("Project Info");
                    return true;
                }
                else if (index == 8) {
                    //Photo & others Info 
                    alert("Photo & others Info");
                    var Id = PhotoAndOthers();
                    return true;
                }
                else if (index == 9) {
                    //Preview
                    var Id = Preview();
                    alert("Preview");
                    return true;
                }
            }
        },

        onInit: function (tab, navigation, index) {

            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            $width = 100 / $total;

            navigation.find('li').css('width', $width + '%');

        },

        onTabClick: function (tab, navigation, index) {

            var $valid = $('.wizard-card form').valid();

            if (!$valid) {
                return false;
            } else {
                return true;
            }

        },

        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;

            var $wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
            } else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
            }

            //update progress
            var move_distance = 100 / $total;
            move_distance = move_distance * (index) + move_distance / 2;

            $wizard.find($('.progress-bar')).css({ width: move_distance + '%' });
            //e.relatedTarget // previous tab

            $wizard.find($('.wizard-card .nav-pills li.active a .icon-circle')).addClass('checked');

        }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked', 'true');
        }
    });

    $('.set-full-height').css('height', 'auto');

});



//Function to show image before upload

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function SavePersonalInfo() {   
   
    var ChildInfoList = new Array();
    var SiblingsInfoList = new Array();
    $("#tblChildInfo > tbody > tr").each(function () {
        var tds = $(this).find("td");       
        //var SStudent = { PAGE_NAME: $(tds[0]).html(), INDEX: $(this).find('td:eq(1) input').is(':checked'), CREATE: $(this).find('td:eq(2) input').is(':checked'), UPDATE: $(this).find('td:eq(3) input').is(':checked'), DELETE: $(this).find('td:eq(4) input').is(':checked'), AUTHORIZE: $(this).find('td:eq(5) input').is(':checked'), PAGE_ID: $(tds[6]).html() }
        var ChildInfo = {
            ChildId: $(tds[0]).html(),
            FullName: $(tds[1]).html(),
            Dob: $(tds[2]).html(),
            MobileNo: $(tds[3]).html(),
            Occupation: $(tds[4]).html(),
            ChildGender: $(tds[5]).html()
        }
         ChildInfoList.push(ChildInfo);
    });
    $("#tblSiblings > tbody > tr").each(function () {
        var tds = $(this).find("td");        
        //var SStudent = { PAGE_NAME: $(tds[0]).html(), INDEX: $(this).find('td:eq(1) input').is(':checked'), CREATE: $(this).find('td:eq(2) input').is(':checked'), UPDATE: $(this).find('td:eq(3) input').is(':checked'), DELETE: $(this).find('td:eq(4) input').is(':checked'), AUTHORIZE: $(this).find('td:eq(5) input').is(':checked'), PAGE_ID: $(tds[6]).html() }
        var Siblings = {
            Id: $(tds[0]).html(),
            FullName: $(tds[1]).html(),
            Dob: $(tds[2]).html(),
            MobileNo: $(tds[3]).html(),
            OccupationId: $(tds[4]).html(),
            SiblingsGender: $(tds[5]).html()
        }
        SiblingsInfoList.push(Siblings);
    });

    var PersonalInfo = {
        EmpId:0,
        PersonalId: $('#txtEmpId').val(),
        DomenId: 'tsultan',
        SalutationId: $('#ddlSalutation').val(),
        FirstName: $('#txtFirstName').val(),
        LastName: $('#txtLastName').val(),
        FatherName: $('#txtFatherName').val(),
        FatherOccupationId: $('#ddlFatherOccupation').val(),
        MotherName: $('#txtMotherName').val(),
        MotherOccupationId: $('#ddlMotherOccupation').val(),
        DateOfBirth: $('#txtDateOfBirth').val(),
        ReligionId: $('#ddlReligion').val(),
        BloodGroupId: $('#ddlBlood').val(),
        GenderId: $('#ddlGender').val(),
        MartialStatusId: $('#ddlMartialStatus').val(),
        SpouseName: $('#txtSpouseName').val(),
        SpouseOccupationId: $('#ddlSpouseOccupation').val(),
        NationalityId: $('#ddlNationality').val(),
        NationalId: $('#txtNationalIdNo').val(),
        BirthId: $('#txtBirthIdNo').val(),
        PassportNo: $('#txtPassportNo').val(),
        PassportExpiry: $('#txtPassportExpireDate').val(),
        DesignationId: $('#ddlDesignation').val(),
        DepartmentId: $('#ddlDepartment').val(),
        BranchId: $('#ddlBranch').val(),
        EmpTypeId: $('#ddlEmployeeType').val(),
        JoiningDate: $('#txtJoinDate').val(),
        ConfirmationDate: $('#txtConfirmationDate').val(),
        BankAccount: $('#txtBankAccount').val(),
        BankId: $('#ddlBankName').val(),
        OfficeMobile: $('#txtOfficeMobile').val(),
        OfficeExt: $('#txtOfficeExt').val(),
        OfficeEmail: $('#txtOfficeEmail').val(),
        PersonalMobile: $('#txtPersonalMobileNo').val(),
        PersonalEmail: $('#txtPersonalEmail').val(),
        EmerPerson: $('#txtEmergencyContactPerson').val(),
        EmerPersonMobile: $('#txtEmergencyPersonCell').val(),
        Relation: $('#txtRelation').val(),
        EmerPersonAddr: $('#txtEmergencyPersonAddress').val(),
        ChildInfoList: ChildInfoList,
        SiblingsInfoList: SiblingsInfoList
    };   


    var datas = JSON.stringify({
        'PersonalInfo': PersonalInfo
    });
   
    $.ajax({
        url: "/PersonalInfo/Create",
        dataType: "html",
        data: datas,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            alert('Success');
            return data.EmpId;
        }
        ,
        error: function (err) {
            alert(err.responseText);
            return 0;
        }
    });
   
}
//function EducationInfo () {
//    var educationInfo =
        
//     { 
//     //object.Id = $('#').val();
//   DegreeId : $('#ddlDegreeName').val(),
//   SubjectId : $('#ddlSubject').val(),
//   InstitutionName : $('#txtInstituteName').val(),
//   BoardUniversityId : $('#ddlBoardOrInstitute').val(),
//   PassingYear : $('#txtActualPassingYear').val(),
//   ResultTypeId : $('#ddlResultType').val(),
//   Cgpa : $('#txtCgpa').val(),
//   OutOf : $('#txtOutof').val(),
//   ClassDivision : $('#ddlClassOrDivision').val(),
//   Session : $('#txtSession').val(),
//    //object.EmpId=$('#').val();
//    //object.AddedBy=$('#').val();
//    //object.Ts = $('#').val();

//     };

//    var datas = JSON.stringify({
//        'educationInfo': educationInfo
//    });

//    $.ajax({
//        url: "/PersonalInfo/EducationInfoCreate",
//        dataType: "html",
//        data: datas,
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        success: function (data) {
//            alert('Success');
//            return data.EmpId;
//        }
//        ,
//        error: function (err) {
//            alert(err.responseText);
//            return 0;
//        }
//    });
  
   
//}
function CertificationInfo() {
    var CertificationInfo =
        {
            //object.Id=
            CertName: $('#txtCertificationName').val(),
            ExamDetails: $('#txtExamDetails').val(),
            CertAuthority: $('#txtCertificationAuthority').val(),
            CertType: $('#ddlCertificationType').val(),
            Location: $('#txtCountryLocation').val(),
            ObtainedDate: $('#txtObtainedDate').val(),
            ExpiryDate: $('#txtExpiryDate').val(),
            EmpId:16
            //object.AddedBy= 
            //object.Ts=
        };
    var datas = JSON.stringify({
        'CertificationInfo': CertificationInfo
    });
    $.ajax({
        url: "/PersonalInfo/CertificationInfoCreate",
        dataType: "html",
        data: datas,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            alert('Success');
            return data.EmpId;
        }
          ,
        error: function (err) {
            alert(err.responseText);
            return 0;
        }
    });
}
//function EmploymentInfo() {
//    var EmploymentInfo = {

//         object.Id=
//        CompanyName: $('#txtCompanyName').val(),
//        CompanyLocation: $('#txtCompanyLocation').val(),
//        Designation: $('#txtDesignation').val(),
//        DepartmentName: $('#txtDepartmentName').val(),
//        StartDate: $('#txtStartDate').val(),
//        EndDate: $('#txtEmployeeEndDate').val(),
//        RolesResponsibility: $('#txtRolesAndResponsibility').val(),
//        AreaOfExperience: $('#txtAreaOfExperience').val(),
//        EmpId:16
//        object. AddedBy=
//        object. Ts=
//    };
//    var datas = JSON.stringify({
//        'EmploymentInfo': EmploymentInfo
//    });
//    $.ajax({
//        url: "/PersonalInfo/EmploymentInfoCreate",
//        dataType: "html",
//        data: datas,
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        success: function (data) {
//            alert('Success');
//            return data.EmpId;
//        }
//          ,
//        error: function (err) {
//            alert(err.responseText);
//            return 0;
//        }
//    });
//}
function ProjectExperienceInfo (){
    var projectExperienceInfo =
            {  //object.Id=
                ProjectName: $('#txtProjectName').val(),
                CompanyName: $('#txtCompanyName').val(),
                CustomerName: $('#txtCustomerName').val(),
                RolesResponsibility: $('#txtRolesAndResponsibility').val(),
                StartDate: $('#txtStartDate').val(),
                EndDate: $('#txtEndDate').val(),
                TechnologyUsed: $('#txtTechnologyUsed').val(),

                EmpId:16
                //object. AddedBy=
                //object. Ts=
            };
    var datas = JSON.stringify({
        'projectExperienceInfo': projectExperienceInfo
    });
    $.ajax({
        url: "/PersonalInfo/ProjectExperienceInfoCreate",
        dataType: "html",
        data: datas,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            alert('Success');
            return data.EmpId;
        }
          ,
        error: function (err) {
            alert(err.responseText);
            return 0;
        }
    });
}
function TrainingInfo() {
    var object = {};
    //object.Id=
   // object. TrainingType= this is from radio button...............
    object.TrainingTitle = $('#txtTrainingTitle').val();
    object.TrainingDetails = $('#txtTrainingDetails').val();
    object.InstitutionName = $('#txtInstituteName').val();
    object.CountryId = $('#ddlCountry').val();
    object. TrainingStartDate= $('#txtTechnologyUsed').val();
    object.TrainingEndDate = $('#txtTrainingEndDate').val();
    object.Duration = $('#txtDuration').val();
    object.Location = $('#txtLocation').val();
    object.IsLegalBond = $('#chbBondingInformation');
    object.BondFor = $('#txtBondFor').val();
    object.EffectiveDate = $('#txtEffectiveDate').val();
    object.EndDate = $('#txtEndDateOfBond').val();
    //object. EmpId=
    //object. AddedBy=
    //object. Ts=
    return 1;
}
function PhotoAndOthers() {
    var object = {};
   // object.ExtraId = 
    object.ExtraActivities = $('#txtExtraCurriculumActivities').val();
    object.Publication = $('#txtPublication').val();
    object.Signature = $('#signature').val();
    object.Photo = $('#photo').val();
    //object.EmpId = 
    //object.Ts = 
    //object.AddedBy = 

}
function Preview() {
    var object = {};
    //object.Id
    object.ResignationDate = $('#txtResignationDate').val();
    object.ReleaseDate = $('#txtReleaseDate').val();
    object.RequestForExp = $('#photo').val();
   // object.RequestForRelease = $('#photo').val();
    object.QuestionNo1 = $('#txtQuestion1').val();
    object.QuestionNo2 = $('#chbOtherEmployment').val();    //checkbox value is here ...............
    object.QuestionNo21 = $('#txtAnswer2_1').val();
    object.QuestionNo22 = $('#txtAnswer2_2').val();
    object.QuestionNo23 = $('#txtBenifit').val();
    object.QuestionNo24 = $('#txtCompensation').val();
    object.QuestionNo3 = $('#txtAnswerThree').val();
    object.QuestionNo4 = $('#txtAnswerFour').val();
    object.QuestionNo5 = $('#txtAnswerFive').val();
    object.QuestionNo6 = $('#txtAnswerSix').val();
    object.QuestionNo7 = $('#txtAnswerSeven').val();
    object.QuestionNo8 = $('#txtAnswerEight').val();
    object.QuestionNo9 = $('#txtAnswerNine').val();
    //object.EmpId
    //object.Ts
    //object.AddedBy
}

   
