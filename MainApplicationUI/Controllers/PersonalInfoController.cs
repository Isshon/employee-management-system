﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using ComonModel;
using System.Net.Http.Headers;

namespace MainApplicationUI.Controllers
{
    public class PersonalInfoController : Controller
    {
        // GET: PersonalInfo

        string Baseurl = ConfigurationManager.AppSettings["BaseUrl"];

        public ActionResult Index()
        {
            //ClsAlldropdownData objAlldropdownData = null;
            //using (var client = new HttpClient())
            //{
               
            //    client.BaseAddress = new Uri(string.Concat(Baseurl));
            //   // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));

            //    //HTTP GET
            //    var responseTask = client.GetAsync("API/ParameterApi/GetAllDropdownData/");
            //    responseTask.Wait();

            //    var result = responseTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //       var readTask = result.Content.ReadAsAsync<ClsAlldropdownData>();
            //        readTask.Wait();
            //       objAlldropdownData = readTask.Result;
            //        ViewBag.CountryList = objAlldropdownData.CountryList;
            //    }
            //    else //web api sent error response 
            //    {
            //        //log response status here..

            //    }
              
            //}

            ////List<ClsSalutationInfo> sltList = objAlldropdownData.SalutationList.ToList();
            ////ViewBag.AllSalutationList = new SelectList(sltList, "CountryId", "CountryName");
            //return View(objAlldropdownData);
             
            return View();
            
        }

        [HttpPost]
        public ActionResult Create(Personal PersonalInfo)
        {
            try
            {
              PersonalInfo.LoginId = Convert.ToString(Session["UserId"]);
                PersonalInfo.Ts = DateTime.Now;
                if (ModelState.IsValid)
                {

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Baseurl);
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
                        var responseTask = client.PostAsJsonAsync<Personal>("API/PersonalApi/SavePersonalInfo", PersonalInfo);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else //web api sent error response 
                        {
                            //log response status here..                          

                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }
                    }
                    return View(); 
                }

                // TODO: Add insert logic here
                return View();

            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult EducationInfoCreate (ClsEducationInfo educationInfo)
        {
            try
            {
                educationInfo.Ts = DateTime.Now;
                if (ModelState.IsValid)
                {
                    using (var client= new HttpClient())
                    {
                        client.BaseAddress = new Uri(Baseurl);
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
                        var responseTask = client.PostAsJsonAsync<ClsEducationInfo>("API/PersonalApi/SaveEducationInfo", educationInfo);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else //web api sent error response 
                        {
                            //log response status here..                          

                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }
                    }
                    return RedirectToAction("Index");


                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult CertificationInfoCreate(ClsCertificationInfo CertificationInfo)
        {
            try
            {
                CertificationInfo.Ts = DateTime.Now;
                if (ModelState.IsValid)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Baseurl);
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
                        var responseTask = client.PostAsJsonAsync<ClsCertificationInfo>("API/PersonalApi/SaveCertificationInfo", CertificationInfo);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else //web api sent error response 
                        {
                            //log response status here..                          

                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }
                    }
                    return RedirectToAction("Index");


                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult EmploymentInfoCreate(ClsEmploymentInfo EmploymentInfo)
        {
            try
            {
                EmploymentInfo.Ts = DateTime.Now;
                if (ModelState.IsValid)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Baseurl);
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
                        var responseTask = client.PostAsJsonAsync<ClsEmploymentInfo>("API/PersonalApi/SaveEmploymentInfo", EmploymentInfo);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else //web api sent error response 
                        {
                            //log response status here..                          

                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }
                    }
                    return RedirectToAction("Index");


                }
                return View();
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult ProjectExperienceInfoCreate(ClsProjectExperienceInfo ProjectExperienceInfo)
        {
            try
            {
                ProjectExperienceInfo.ts = DateTime.Now;
                if (ModelState.IsValid)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Baseurl);
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
                        var responseTask = client.PostAsJsonAsync<ClsProjectExperienceInfo>("API/PersonalApi/SaveProjectExperienceInfo", ProjectExperienceInfo);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else //web api sent error response 
                        {
                            //log response status here..                          

                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }
                    }
                    return RedirectToAction("Index");


                }
                return View();
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult SavePhoto(ClsPhotoAndOthers photoAndOthersInfo)
        {
            try
            {

                photoAndOthersInfo.Ts = DateTime.Now;
                if (ModelState.IsValid)
                {

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Baseurl);
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
                        var responseTask = client.PostAsJsonAsync<ClsPhotoAndOthers>("API/PersonalApi/SavePhotoAndOtherInfo", photoAndOthersInfo);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else //web api sent error response 
                        {
                            //log response status here..                          

                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }
                    }
                    return View();
                }

                // TODO: Add insert logic here
                return View();

            }
            catch
            {
                return View();
            }
        }
        public class Test
        {
            //public string id { get; set; }
            //public string name { get; set; }  
              
            public int EmpId { get; set; }
            public string PersonalId { get; set; }
            public string DomenId { get; set; }
            public string FirstName { get; set; }
            public System.DateTime JoiningDate { get; set; }
        }
        //public class Personal
        //{
        //    public int EmpId { get; set; }          
        //    public string PersonalId { get; set; }          
        //    public string DomenId { get; set; }           
        //    public Nullable<int> SalutationId { get; set; }         
        //    public string FirstName { get; set; }           
        //    public string LastName { get; set; }         
        //    public string FatherName { get; set; }
        //    public int FatherOccupationId { get; set; }           
        //    public string MotherName { get; set; }          
        //    public int MotherOccupationId { get; set; }
        //    public System.DateTime DateOfBirth { get; set; }        
        //    public int ReligionId { get; set; }         
        //    public Nullable<int> BloodGroupId { get; set; }          
        //    public int GenderId { get; set; }
        //    public Nullable<int> MartialStatusId { get; set; }
        //    public string SpouseName { get; set; }
        //    public Nullable<int> SpouseOccupationId { get; set; }
        //    public Nullable<int> NationalityId { get; set; }
        //    public string NationalId { get; set; }
        //    public string BirthId { get; set; }
        //    public string PassportNo { get; set; }
        //    public Nullable<System.DateTime> PassportExpiry { get; set; }          
        //    public Nullable<int> DesignationId { get; set; }        
        //    public Nullable<int> DepartmentId { get; set; }           
        //    public Nullable<int> BranchId { get; set; }       
        //    public Nullable<int> EmpTypeId { get; set; }
        //    public System.DateTime JoiningDate { get; set; }         
        //    public Nullable<System.DateTime> ConfirmationDate { get; set; }
        //    public string BankAccount { get; set; }           
        //    public Nullable<int> BankId { get; set; }
        //    public string OfficeMobile { get; set; }
        //    public Nullable<int> OfficeExt { get; set; }
        //    public string PersonalMobile { get; set; }
        //    public string OfficeEmail { get; set; }
        //    public string PersonalEmail { get; set; }
        //    public string EmerPerson { get; set; }
        //    public string EmerPersonMobile { get; set; }
        //    public string Relation { get; set; }
        //    public string EmerPersonAddr { get; set; }                     
        //}
        [HttpPost]
        public ActionResult TestCreate(Personal test)
        {
            
            return View();
        }

    }
}