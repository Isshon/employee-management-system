﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Security;
using System.DirectoryServices;
using MainApplicationUI.Models;
using System.Net.Http;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace MainApplicationUI.Controllers
{
    public class AccountController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings["BaseUrl"];

        private class token
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }
            public string refresh_token { get; set; }

        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public  ActionResult Login(string loginId, string password)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}
            string serverName = ConfigurationManager.AppSettings["ADServer"];
            bool result = false;
            try
            {
                //Check user credentials

               // DirectoryEntry Entry = new DirectoryEntry("LDAP://"+serverName, loginId, password);
               // object nativeObject = Entry.NativeObject;
               
                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri(Baseurl);
                //     var pairs = new List<KeyValuePair<string, string>> {
                //        new KeyValuePair<string, string>("grant_type","password"),
                //         new KeyValuePair<string, string>("UserName",loginId),
                //         new KeyValuePair<string, string>("Password",password)
                //          };

                //    var content = new FormUrlEncodedContent(pairs);

                //    FormsAuthentication.SetAuthCookie(loginId, false);
                //}

              using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Baseurl);
                    //HTTP GET

                    var pairs = new List<KeyValuePair<string, string>> {
                        new KeyValuePair<string, string>("grant_type","password"),
                         new KeyValuePair<string, string>("UserName",loginId),
                         new KeyValuePair<string, string>("Password",password)
                          };

                    var content = new FormUrlEncodedContent(pairs);

                    //MediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter();

                    //HttpContent datas = new ObjectContent<dynamic>(new {
                    //    grant_type= "password",
                    //    UserName = loginId,
                    //    Password= password
                    //}, jsonFormatter);


                    var responseTask = client.PostAsync(Baseurl+"/token" , content);
                    responseTask.Wait();

                    var responseResult = responseTask.Result;
                    if (responseResult.IsSuccessStatusCode)
                    {
                        string readTask = Convert.ToString(responseResult.Content.ReadAsAsync<object>().Result);
                        //readTask.Wait();
                        FormsAuthentication.SetAuthCookie(loginId, false);
                        var products = JsonConvert.DeserializeObject<token>(readTask);
                        Session["token"] = products.access_token;
                        Session["UserId"] = loginId;


                    }
                    else //web api sent error response 
                    {
                        //log response status here..

                      

                        ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                    }
                }

                result = true;
               // return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)

            {
                // If we got this far, something failed, redisplay form

                ModelState.AddModelError("", "User name or password is incorrect.");

            }          
            if (result)
                return RedirectToAction("Index", "Home"); 
            else return View();
        }
        public ActionResult SignIn()
        {
            return View("SignIn");
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}
