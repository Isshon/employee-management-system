﻿using ComonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MainApplicationUI.Controllers
{
    public class HomeController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings["BaseUrl"];
        [Authorize]
        public ActionResult Index()
        {
            IEnumerable<ClsProducts> products = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                //HTTP GET
                //var pairs = new List<KeyValuePair<string, string>> {
                //        new KeyValuePair<string, string>("Authorization",Convert.ToString(Session["token"]))
                //          };

                //var content = new FormUrlEncodedContent(pairs);
             
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer",Convert.ToString(Session["token"]));


                var responseTask = client.GetAsync("API/ProductApi/GetAllProducts");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<ClsProducts>>();
                    readTask.Wait();

                    products = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    products = Enumerable.Empty<ClsProducts>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(products);
        }
      

        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult create(ClsProducts product)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);

                //HTTP POST
               // var postTask = client.PostAsJsonAsync<ClsProducts>("API/ProductApi/PostNewStudent", product);
               // postTask.Wait();

             //   var result = postTask.Result;
              //  if (result.IsSuccessStatusCode)
              //  {
                    return RedirectToAction("Index");
               // }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(product);
        }
        //public ActionResult Edit()
        //{
        //    return View();
        //}

        public ActionResult Edit(int id)
        {
            ClsProducts product = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(string.Concat(Baseurl));               //HTTP GET

               

                var responseTask = client.GetAsync("API/ProductApi/GetProduct/"+id.ToString() );
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    // string readTask = Convert.ToString(result.Content.ReadAsAsync<ClsProducts>().Result);
                    //readTask.Wait();
                    var readTask = result.Content.ReadAsAsync<ClsProducts>();
                    readTask.Wait();

                    product = readTask.Result;
                    // var products = JsonConvert.DeserializeObject<ClsProducts>(readTask);
                    var bankInfoList = product.listBankinfo;
                    var bankCountryInfo = product.listCountry;

                }
                else //web api sent error response 
                {
                    //log response status here..
                    
                }
            }
            return View();
        }


        [HttpPost]
        public ActionResult Edit(ClsProducts product)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);

                //HTTP POST
               // var postTask = client.PostAsJsonAsync<ClsProducts>("API/ProductApi/UpdateProduct", product);
               // postTask.Wait();

               // var result = postTask.Result;
                //if (result.IsSuccessStatusCode)
               // {
                    return RedirectToAction("Index");
               // }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(product);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}