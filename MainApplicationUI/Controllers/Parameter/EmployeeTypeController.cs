﻿using ComonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MainApplicationUI.Controllers.Parameter
{
    public class EmployeeTypeController : Controller
    {
        //string Baseurl = ConfigurationManager.AppSettings["BaseUrl"];

        // GET: EmployeeType
        public ActionResult Index()
        {
            return View();
        }
        //    public ActionResult Index()
        //    {
        //        IEnumerable<ClsEmployeeTypeInfo> EmployeeTypeInfoList = new List<ClsEmployeeTypeInfo>();

        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(Baseurl);

        //             client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));

        //            var responseTask = client.GetAsync("API/ParameterApi/GetAllEmployeeType");
        //            responseTask.Wait();

        //            var result = responseTask.Result;
        //            if (result.IsSuccessStatusCode)
        //            {
        //                var readTask = result.Content.ReadAsAsync<IList<ClsEmployeeTypeInfo>>();
        //                readTask.Wait();

        //                EmployeeTypeInfoList = readTask.Result;
        //            }
        //            else //web api sent error response 
        //            {
        //                //log response status here..

        //                EmployeeTypeInfoList = Enumerable.Empty<ClsEmployeeTypeInfo>();

        //                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //            }
        //        }

        //        return View(EmployeeTypeInfoList);
        //    }

        //    // GET: EmployeeType/Details/5
        //    public ActionResult Details(int id)
        //    {
        //        ClsEmployeeTypeInfo objEmployeeTypeInfo = null;

        //        try
        //        {
        //            using (var client = new HttpClient())
        //            {
        //                client.BaseAddress = new Uri(Baseurl);

        //                var responseTask = client.GetAsync("API/ParameterApi/GetEmployeeTypeId/" + id.ToString());
        //                responseTask.Wait();
        //                var result = responseTask.Result;

        //                if (result.IsSuccessStatusCode)
        //                {

        //                    var readTask = result.Content.ReadAsAsync<ClsEmployeeTypeInfo>();
        //                    readTask.Wait();
        //                    objEmployeeTypeInfo = readTask.Result;

        //                }
        //            }
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //        return View(objEmployeeTypeInfo);
        //    }

        //    // GET: EmployeeType/Create
        //    public ActionResult Create()
        //    {

        //        return View();
        //    }

        //    // POST: EmployeeType/Create
        //    [HttpPost]
        //    public ActionResult Create(ClsEmployeeTypeInfo EmployeeTypeInfo)
        //    {
        //        try
        //        {
        //            EmployeeTypeInfo.AddedBy = 0;
        //            EmployeeTypeInfo.Ts = DateTime.Now;
        //            if(ModelState.IsValid)
        //            {

        //                using (var client = new HttpClient())
        //                {
        //                    client.BaseAddress = new Uri(Baseurl);                    
        //                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
        //                    var responseTask = client.PostAsJsonAsync<ClsEmployeeTypeInfo>("API/ParameterApi/CreateEmployeeType", EmployeeTypeInfo);
        //                    responseTask.Wait();

        //                    var result = responseTask.Result;
        //                    if (result.IsSuccessStatusCode)
        //                    {
        //                        return RedirectToAction("Index");
        //                    }
        //                    else //web api sent error response 
        //                    {
        //                        //log response status here..                          

        //                        ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //                    }
        //                }
        //                return RedirectToAction("Index");
        //            }             

        //            // TODO: Add insert logic here
        //            return View();

        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }

        //    // GET: EmployeeType/Edit/5
        //    public ActionResult Edit(int id)
        //    {
        //        ClsEmployeeTypeInfo objEmployeeTypeInfo = null;

        //        try {
        //            using (var client = new HttpClient())
        //            {
        //                client.BaseAddress = new Uri(Baseurl);

        //                var responseTask = client.GetAsync("API/ParameterApi/GetEmployeeTypeId/" + id.ToString());
        //                responseTask.Wait();
        //                var result = responseTask.Result;

        //                if (result.IsSuccessStatusCode)
        //                {                       

        //                    var readTask = result.Content.ReadAsAsync<ClsEmployeeTypeInfo>();
        //                    readTask.Wait();
        //                    objEmployeeTypeInfo = readTask.Result;                        

        //                }
        //            }
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //        return View(objEmployeeTypeInfo);
        //    }

        //    // POST: EmployeeType/Edit/5
        //    [HttpPost]
        //    public ActionResult Edit(ClsEmployeeTypeInfo EmployeeTypeInfo)
        //    {
        //        try
        //        {
        //            EmployeeTypeInfo.AddedBy = 0;
        //            EmployeeTypeInfo.Ts = DateTime.Now;
        //            if (ModelState.IsValid)
        //            {

        //                using (var client = new HttpClient())
        //                {
        //                    client.BaseAddress = new Uri(Baseurl);
        //                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", Convert.ToString(Session["token"]));
        //                    var responseTask = client.PostAsJsonAsync<ClsEmployeeTypeInfo>("API/ParameterApi/UpdateEmployeeType", EmployeeTypeInfo);
        //                    responseTask.Wait();

        //                    var result = responseTask.Result;
        //                    if (result.IsSuccessStatusCode)
        //                    {
        //                        return RedirectToAction("Index");
        //                    }
        //                    else //web api sent error response 
        //                    {
        //                        //log response status here..                          

        //                        ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //                    }
        //                }
        //                return RedirectToAction("Index");
        //            }

        //            // TODO: Add insert logic here
        //            return View();

        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }

        //    // GET: EmployeeType/Delete/5
        //    public ActionResult Delete(int id)
        //    {

        //        try
        //        {
        //            using (var client = new HttpClient())
        //            {
        //                client.BaseAddress = new Uri(Baseurl);

        //                var responseTask = client.DeleteAsync("API/ParameterApi/DeleteEmployeeType/" + id.ToString());
        //                responseTask.Wait();
        //                var result = responseTask.Result;

        //                if (result.IsSuccessStatusCode)
        //                {

        //                    return RedirectToAction("Index");

        //                }
        //            }
        //        }
        //        catch
        //        {
        //            return RedirectToAction("Index");
        //        }
        //        return RedirectToAction("Index");
        //    }

        //    // POST: EmployeeType/Delete/5
        //    [HttpPost]
        //    public ActionResult Delete(int id, FormCollection collection)
        //    {
        //        try
        //        {
        //            // TODO: Add delete logic here

        //            return RedirectToAction("Index");
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }
        //}
    }
}
